Power:

Power:
| Connection (range) | setting                                             |
| ------------------ | --------------------------------------------------- |
| VSSA               | 1.2V                                                 |
| VGate              | 3.3V(this has a voltage divider for gate at 2.1V)    |
| VDDD               | 1.8V                                                 |
| VDDA               | 1.8V                                                 |