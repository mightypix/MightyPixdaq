### The system is made of several components:

The system consists of 3 cards:

FPGA board on MIO3 -> Adapter Board -> chip carrier board

Any PC with linux and USB (+ ethernet) works!

This is the old version now the adapter board will connect to the carrier with Display port cables!
<!-- ![not the best picture](1636122402846.jpg "MIO3 Setup with GPAC and Run2020") -->
<img src="IMG_20220110_132640.jpg" alt="MIO3 Setup with GPAC and Run2020" width="400"/>

The FPGA is a commercial one:

[enclustra KX1](https://www.enclustra.com/en/products/fpga-modules/mercury-kx1/)

You can also use KX2, but this does not give you any benefit, we (you) will just have to compile it with a different pinoutput

heatsink:

[This one fits](https://www.digikey.de/products/de?keywords=ATS-50270G-C1-R0)

The FPGA carrier borad:

MIO3 -> Bonn SILAB development -> ask Klaas for details if you need anyting

For the conncetor board:

~~GPAC -> Bonn SILAB development -> we will not use this!~~

[Adapter Card](https://gitlab.cern.ch/mightypix/mightypixadapterboard) This is what we will use!

For the chip carrier board:

[Run2020 PCB](https://gitlab.cern.ch/padeken/mightypixrun2020_pcb)


Costs:
| Part         | cost estimate | source |
| ------------ | ------------- |--------|
| kx1          | 630 (prices did get higher in the last month) +VAT      | [enclustra KX1](https://www.enclustra.com/en/products/fpga-modules/mercury-kx1/) |
| MIO3         | -       | Bonn   |
| [Adapter Card](https://gitlab.cern.ch/mightypix/mightypixadapterboard)         |  -       | Bonn   |
| chip carrier | < 20          | Bonn/You as you like |





