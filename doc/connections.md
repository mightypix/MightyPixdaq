The main connections in the repository will be named as in the chip "documentation"
But here is a short list how they are named from the GPAC side for the MIO/MIO3


| chip side    | fpga/GPAC side |
| ------------ | -------------- |
| SCK          | DIN6           |
| CSB          | DIN5           |
| MOSI         | DIN4           |
| MISO         | DOUT1          |
| RST_Ctl_RB   | DIN7           |
| Ctl_Clk_1    | DIN3           |
| Ctl_Clk_2    | DIN2           |
| Ctl_Load     | DIN1           |
| Ctl_SIN      | DIN0           |
| Ctl_SOut     | DOUT0          |
| INJECTION    | INJECTION      |
| DATA_LVDS    | LVDA_OUT0      |
| Clk_ext_LVDS | LVDA_IN0       |
| Clk_ref      | LVDA_IN1       |
| Sync_res     | LVDA_IN2       |


For the voltages look in the yaml file. But you basically have to remember the names as they are on the chip side.

More details can also be found here:
https://gitlab.cern.ch/padeken/mightypixrun2020_pcb


For power:

| Connection (range) | setting                                             |
| ------------------ | --------------------------------------------------- |
| VDDRAM 1.5-1.8V    | 1.6                                                 |
| VCasc2             | 0.8                                                 |
| Vminus 0.5-1.5V    | 1.                                                  |
| PWell -1 -0V       | 0V (connect to ground or use the poti to set to 0V) |
| Nwell              | VSSA                                                |
| VSS_TAC            | 0.3                                                 |



Jumper configurations on the FPGA Carrier board (MIO3PCI):

J200 -> 3.3

J400 -> set if you need external IO

J201 -> 1.8

Short R706


If you use USB to power, use a source that can supply >= 1A -> 5W.
This is not the case for the usual Laptops!!!


If you can not see an output signal:
 * Test the leds (maybe blick)
 * Use the speed test in the "Simple_Function_test". This should provide a 50MHz output on CLK40 (N/P) or 1 V DC measured with the Multimeter.



For Run2020V2:
connect the followong jumper:
TH2
VDDRAM
BL
VMimus
check that R13 and R14 are R=0 Ohm == Jumper




