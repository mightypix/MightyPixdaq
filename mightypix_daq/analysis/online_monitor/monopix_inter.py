import numpy as np

from online_monitor.converter.transceiver import Transceiver
from online_monitor.utils import utils
import time
from mightypix_daq.analysis.interpreter import RawDataInterpreter
import mightypix_daq.analysis.interpreter as interpreter
from mightypix_daq.analysis import analysis_utils as au

def gray2bin(gray):
        b6 = gray & 0x40
        b5 = (gray & 0x20) ^ (b6 >> 1)
        b4 = (gray & 0x10) ^ (b5 >> 1)
        b3 = (gray & 0x08) ^ (b4 >> 1)
        b2 = (gray & 0x04) ^ (b3 >> 1)
        b1 = (gray & 0x02) ^ (b2 >> 1)
        b0 = (gray & 0x01) ^ (b1 >> 1)
        return b6 + b5 + b4 + b3 + b2 + b1 + b0


class Mightypix(Transceiver):

    def setup_transceiver(self):
        ''' Called at the beginning

            We want to be able to change the histogrammmer settings
            thus bidirectional communication needed
        '''
        self.set_bidirectional_communication()
        print('bidirectional communication set')

    def setup_interpretation(self):
        ''' Objects defined here are available in interpretation process '''
        utils.setup_logging(self.loglevel)

        self.chunk_size = self.config.get('chunk_size', 1000000)
        self.analyze_tdc = self.config.get('analyze_tdc', False)
        # self.rx_id = int(self.config.get('rx', 'rx0')[2])
        # Mask pixels that have a higher occupancy than 3 * the median of all firing pixels
        self.noisy_threshold = self.config.get('noisy_threshold', 3)

        self.mask_noisy_pixel = False

        # Init result hists
        #self.interpreter = RawDataInterpreter()
        self.interpreter = interpreter.Interpreter()
        self.reset_hists()

        # Number of readouts to integrate
        self.int_readouts = 0

        # Variables for meta data time calculations
        self.ts_last_readout = 0.  # Time stamp last readout
        self.hits_last_readout = 0.  # Number of hits
        self.triggers_last_readout = 0.  # Number of trigger words
        self.fps = 0.  # Readouts per second
        self.hps = 0.  # Hits per second
        self.tps = 0.  # Triggers per second
        self.total_trigger_words = 0


        #when the online monitor is run for one parameter variation only, we need these to reset histograms
        self.current_dac = None
        self.current_dac_value = None
        #when the epoche changes while using online monitor, we need to reset histograms
        self.current_epoche = 0

    def deserialize_data(self, data):
        ''' Inverse of serialization '''
        return utils.simple_dec(data)

    def _add_to_meta_data(self, meta_data):
        ''' Meta data interpratation is deducing timings '''

        ts_now = float(meta_data['timestamp_stop'])

        # Calculate readout per second with smoothing
        if ts_now != self.ts_last_readout:
            recent_fps = 1.0 / (ts_now - self.ts_last_readout)
            self.fps = self.fps * 0.95 + recent_fps * 0.05

            # Calulate hits per second with smoothing
            recent_hps = self.hits_last_readout * recent_fps
            self.hps = self.hps * 0.95 + recent_hps * 0.05

            # Calculate trigger rate with smoothing
            recent_tps = self.triggers_last_readout * recent_fps
            self.tps = self.tps * 0.95 + recent_tps * 0.05

        self.ts_last_readout = ts_now

        # Add info to meta data
        meta_data.update(
            {'fps': self.fps,
             'hps': self.hps,
             'tps': self.tps,
             'total_hits': self.total_hits,
             'total_triggers': self.total_trigger_words})
        return meta_data

    def interpret_data(self, data):
        ''' Called for every chunk received '''
        
        #Here we want to implement a function that only takes one readout and displays the data received in that readout
        raw_data, meta_data = data[0][1]
        meta_data = self._add_to_meta_data(meta_data)

        # print('##################################')
        # print(f" Lenght of the raw data for this readout is {len(raw_data)}")
        # now = time.time()
        # print(f"time is : {now}")
        # print('\n\n##################################\n\n')
        self.interpreter.interpret_data_one_unit(raw_data)
        hits = self.interpreter.hit_data
        debug_out = self.interpreter.debug_out
        n_hits = len(hits)
        self.hits_last_readout = n_hits - self.total_hits
        self.total_hits = n_hits
        self.readout += 1
        last_triggers = self.total_trigger_words
        
        print('############### Interpreter Meta data #################')
        print(f"Run number {self.current_epoche} | Readout number {self.readout}")
        print(f"Current param: {self.current_dac}")
        print(f"Raw data size: {len(raw_data)} | Hit data size: {self.hits_last_readout}")
        print(f"In total there are {self.total_hits} hits")
        print(f"(Column, Row) tuple:")
        print(hits[["det_col","det_row"]])
        print('######################################################\n\n')
        
        self.hist_occ, _, _ = np.histogram2d(
            hits["det_row"],
            hits["det_col"],
            bins=(
                np.arange(120),    #TODO: specify this in the meta_data perhaps? find it in configure and put in the meta data perhaps and restore: np.arange(self.chip.COL_SIZE + 1)
                np.arange(28),
            ),
        )
        ts_tdc = gray2bin(hits["ts3"])
        ts_tot = gray2bin(hits["tot"])

        #self.hist_tot = np.ones((512, 512, 15, 128), dtype=np.int16)  # taking just dummy histograms to see if the receiver is actually working
        self.hist_tot, _ = np.histogram(ts_tot.astype(np.float64), bins=128)
        #self.hist_tot = np.ones((512, 512, self.n_scan_params, 128), dtype=numba.uint16)
        #self.hist_tdc = np.ones(4096, dtype=np.int16)
        self.hist_tdc, _ = np.histogram(ts_tdc.astype(np.float64), bins=500)

        
            

        #n_triggers = self.interpreter.get_n_triggers()
        #self.triggers_last_readout = n_triggers - last_triggers
        #self.total_trigger_words = n_triggers
        
        
        occupancy_hist = self.hist_occ
        # Mask noisy pixels
        if self.mask_noisy_pixel:
            sel = occupancy_hist > self.noisy_threshold * np.median(occupancy_hist[occupancy_hist > 0])
            occupancy_hist[sel] = 0
        
        # print('##################################')
        # print(int_data.shape)
        # print(np.nonzero(int_data))
        # print('\n\n##################################\n\n')
        
        interpreted_data = {
            'meta_data': meta_data,
            'occupancy': occupancy_hist,
            'tot_hist': self.hist_tot,
            #'tot_hist': self.hist_tot.sum(axis=(0, 1, 2)),
            'tdc_hist': self.hist_tdc,
        }

        if self.int_readouts != 0:  # = 0 for infinite integration
            if self.readout % self.int_readouts == 0:
                self.reset_hists()

        if "param_key" in meta_data.keys():
            if "param_val" in meta_data.keys():
                if (self.current_dac != meta_data["param_key"]) | (self.current_dac_value != meta_data["param_val"]):
                    self.reset_hists()
                    self.current_dac = meta_data["param_key"]
                    self.current_dac_value = meta_data["param_val"]

        if "epoche" in meta_data.keys():
            
            if (self.current_epoche != meta_data["epoche"]):
                self.reset_hists()
                self.current_epoche = meta_data["epoche"]


        return [interpreted_data]

    def serialize_data(self, data):
        ''' Serialize data from interpretation '''
        return utils.simple_enc(None, data)

    def handle_command(self, command):
        ''' Received commands from GUI receiver '''
        if command[0] == 'RESET':
            self.reset_hists()
            self.last_event = -1
            self.trigger_id = -1
        elif 'MASK' in command[0]:
            if '0' in command[0]:
                self.mask_noisy_pixel = False
            else:
                self.mask_noisy_pixel = True
        else:
            self.int_readouts = int(command[0])

    def reset_hists(self):
        ''' Reset the histograms '''
        self.total_hits = 0
        self.total_trigger_words = 0
        # Readout number
        self.readout = 0
        
        self.interpreter.reset_hist()
        
