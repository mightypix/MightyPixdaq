#! /usr/bin/env python
# load binary lib/pyeudaq.so
import pyeudaq
from pyeudaq import EUDAQ_INFO, EUDAQ_ERROR
from mightypix_daq.mightypix_adapter import MightyPix
import time
from datetime import datetime
import mightypix_daq.analysis.interpreter as interpreter


def exception_handler(method):
    def inner(*args, **kwargs):
        try:
            return method(*args, **kwargs)
        except Exception as e:
            EUDAQ_ERROR(str(e))
            raise e
    return inner


class MARSProducer(pyeudaq.Producer):
    def __init__(self, name, runctrl):
        EUDAQ_INFO(name)
        EUDAQ_INFO(runctrl)
        pyeudaq.Producer.__init__(self, name, runctrl)
        self.name = name
        self.is_running = False
        self.ini = False
        self.conf = False
        self.connected = False
        self.config_feedback = False
        self.chip = None
        self.run_num = 0
        self.readout = 0
        self.evn_num = 0
        self.chip = MightyPix(conf="../../mightypix_adapter.yaml")
        EUDAQ_INFO(self.name)
        EUDAQ_INFO('New instance of ExamplePyProducer')
        

    
    # def connect_to_eudaq(self):
    #     if self.connected:
    #         EUDAQ_ERROR("Trying to connect but already connected to MP Producer")
    #     else:
    #         ConnectionStatus = Connect()
    #     self.connected = True

    # def is_connected(self):
    #     self.ConnectionStatus(self.is_connected())

    # def change_connection(self, connect):
    #     if connect:
    #         self.connect_to_eudaq()
    #     else:
    #         EUDAQ_INFO("Disconnection triggered")
    #         Disconnect()

    # def log_message(self, msg, lvl):
    #     EUDAQ_USER(msg)

    @exception_handler
    def DoInitialise(self):        
        EUDAQ_INFO('DoInitialise')
        conf = self.GetInitConfiguration().as_dict()
        
        self.chip.init()
        self.ini = True
        EUDAQ_INFO("Initilization check")
     
    @exception_handler
    def DoConfigure(self):        
        EUDAQ_INFO('DoConfigure')
        self.chip.write_global_conf()
        conf = self.GetConfiguration().as_dict()
        self.runnum = 1
        # self.runnum = self.GetRunNumber()
        self.conf = True

    @exception_handler
    def DoStartRun(self):
        EUDAQ_INFO('DoStartRun just started!')
        self.evn = 0
        # self.evn = _eventnumber
        self.SetStatusTag("Filesize[MB]", str(self.evn))
        self.runnum = 1
        # self.runnum = self.GetRunNumber()
        self.readout = self.chip.set_read()
        self.is_running = True
        
    @exception_handler
    def DoStopRun(self):        
        EUDAQ_INFO ('DoStopRun')
        self.readout = self.chip.stop_read()
        self.is_running = False

    @exception_handler
    def DoTerminate(self):
        return

    @exception_handler  
    def DoReset(self):        
        EUDAQ_INFO('DoRese received')
        self.readout = self.chip.reset_read()
        self.is_running = False

    @exception_handler
    def DoStatus(self):
        pass
        # self.chip.power_status()
        # self.chip.get_rx_status()
        # self.SetStatusTag()

    @exception_handler
    def RunLoop(self):
        EUDAQ_INFO("Start of RunLoop in ExamplePyProducer")
        while(self.is_running):
            print('Eudaq data active')
   

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='MARS EUDAQ2 Producer', formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--run-control', '-r', default="tcp://localhost:44000")
    parser.add_argument('--name', '-n', default="MARS_Run2021_v2.X")
    args=parser.parse_args()

    myproducer = MARSProducer(args.name, args.run_control)
    myproducer.Connect()
    print (f"connecting to runcontrol in {args.run_control}", )
    time.sleep(2)
    while(myproducer.IsConnected()):
        time.sleep(1)