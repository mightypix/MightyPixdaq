/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps / 1ps 
`default_nettype none

module mightypix_data_rx #(
    parameter BASEADDR = 32'h0000,
    parameter HIGHADDR = 32'h0000,
    parameter DSIZE = 10,
    parameter DATA_IDENTIFIER = 4'h1,
    parameter ABUSWIDTH = 32,
    parameter USE_FIFO_CLK = 0
) (
    // clocks
    input  wire RX_CLK,
    input  wire CLK_RX_OUT,
    input  wire CLK_RX_OUTX2,


    // Fast receiver module
    output wire soft_reset_rx,
    input wire [3:0] rxdisperr,
    input wire [3:0] rxnotintable,
    input wire rxusrclk,
    input wire rxusrclk2,
    input wire [31:0] rxdata,
    input wire [3:0] rxcharisk,
    input wire resetdone,
    input wire track_data_out,
    input wire cplllocked,


    // chip
    output wire RX_READY,
    input wire RX_DATA,
    input wire [47:0] FPGA_TIMESTAMP,
    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    output wire RX_ENABLED,

    // Bus
    input wire                 BUS_CLK,
    input wire                 BUS_RST,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [          7:0] BUS_DATA,
    input wire                 BUS_RD,
    input wire                 BUS_WR
);


  wire IP_RD, IP_WR;
  wire [ABUSWIDTH-1:0] IP_ADD;
  wire [7:0] IP_DATA_IN;
  wire [7:0] IP_DATA_OUT;

  bus_to_ip #(
      .BASEADDR (BASEADDR),
      .HIGHADDR (HIGHADDR),
      .ABUSWIDTH(ABUSWIDTH)
  ) bus_to_ip (
      .BUS_RD  (BUS_RD),
      .BUS_WR  (BUS_WR),
      .BUS_ADD (BUS_ADD),
      .BUS_DATA(BUS_DATA),

      .IP_RD(IP_RD),
      .IP_WR(IP_WR),
      .IP_ADD(IP_ADD),
      .IP_DATA_IN(IP_DATA_IN),
      .IP_DATA_OUT(IP_DATA_OUT)
  );

  wire FIFO_CLK_INT;
  assign FIFO_CLK_INT = BUS_CLK;

  mightypix_data_rx_core_slow #(
      .DSIZE(DSIZE),
      .DATA_IDENTIFIER(DATA_IDENTIFIER),
      .ABUSWIDTH(ABUSWIDTH)
  ) core (
    // Bus
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(IP_ADD),
      .BUS_DATA_IN(IP_DATA_IN),
      .BUS_RD(IP_RD),
      .BUS_WR(IP_WR),
      .BUS_DATA_OUT(IP_DATA_OUT),

    // clocks
      .RX_CLKW(RX_CLK),
      .CLK_RX_OUT(CLK_RX_OUT),
      .CLK_RX_OUTX2(CLK_RX_OUTX2),

    // chip
      .RX_DATA(RX_DATA),
      .RX_READY(RX_READY), 
      .FPGA_TIMESTAMP(FPGA_TIMESTAMP),
      .FIFO_READ(FIFO_READ),
      .FIFO_EMPTY(FIFO_EMPTY),
      .FIFO_DATA(FIFO_DATA),
      .RX_ENABLED(RX_ENABLED),
     

    // Fast receiver module - not usable for run2020 chips
      .rxdisperr(rxdisperr),
      .rxnotintable(rxnotintable),
      .rxusrclk(rxusrclk),
      .rxusrclk2(rxusrclk2),
      .rxcharisk(rxcharisk),
      .TRACK_DATA_OUT(track_data_out)

  );

endmodule