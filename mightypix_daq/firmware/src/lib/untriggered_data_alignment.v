/*
 * ATLASPix3_SoftAndFirmware
 * Copyright (C) 2019  Rudolf Schimassek (rudolf.schimassek@kit.edu)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     KIT-ADL
// Engineer:    Rudolf Schimassek
// 
// Create Date: 05.09.2019
// Design Name: 
// Module Name: Data Alignment for untriggered Readout on ATLASPix3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module untriggered_data_aligner(
	input  wire 		clock,
	input  wire 		reset,
	
	input  wire 		block_update,
	
	input  wire [9:0] 	datain, 	//unaligned data input
	output wire [9:0] 	dataout, 	//aligned data output
	output reg        	alignment_found
);

reg [19:0] rec_data;
reg [3:0]  alignment;
reg [9:0]  aligned_data;

assign dataout = aligned_data;

// This is 0xBC,1 in 10b endoded!
parameter code_plus  = 10'b1010000011;
parameter code_minus = 10'b0101111100;

//take the data from the deserialiser on rising edge of the slow clock:
always @(posedge block_update) begin
    rec_data <= {datain[9:0],rec_data[19:10]};

end
always @(posedge clock) begin
    if(reset) begin
        rec_data        <= 20'b0;
		aligned_data    <= 10'b0;
        alignment_found <=  1'b0;
    end
    else begin
        //try to find data to align to:
        if((rec_data[9+0:0] == code_plus) || (rec_data[9+0:0] == code_minus)) begin
            alignment       <= 0;
            alignment_found <= 1;
        end else if((rec_data[9+1:1] == code_plus) || (rec_data[9+1:1] == code_minus)) begin
            alignment       <= 1;
            alignment_found <= 1;
        end else if((rec_data[9+2:2] == code_plus) || (rec_data[9+2:2] == code_minus)) begin
            alignment       <= 2;
            alignment_found <= 1;
        end else if((rec_data[9+3:3] == code_plus) || (rec_data[9+3:3] == code_minus)) begin
            alignment       <= 3;
            alignment_found <= 1;
        end else if((rec_data[9+4:4] == code_plus) || (rec_data[9+4:4] == code_minus)) begin
            alignment       <= 4;
            alignment_found <= 1;
        end else if((rec_data[9+5:5] == code_plus) || (rec_data[9+5:5] == code_minus)) begin
            alignment       <= 5;
            alignment_found <= 1;
        end else if((rec_data[9+6:6] == code_plus) || (rec_data[9+6:6] == code_minus)) begin
            alignment       <= 6;
            alignment_found <= 1;
        end else if((rec_data[9+7:7] == code_plus) || (rec_data[9+7:7] == code_minus)) begin
            alignment       <= 7;
            alignment_found <= 1;
        end else if((rec_data[9+8:8] == code_plus) || (rec_data[9+8:8] == code_minus)) begin
            alignment       <= 8;
            alignment_found <= 1;
        end else if((rec_data[9+9:9] == code_plus) || (rec_data[9+9:9] == code_minus)) begin
            alignment       <= 9;
            alignment_found <= 1;
        end else begin
            alignment_found <= 0;
        end
        
        case(alignment_found)
            0: aligned_data[9:0] <= rec_data[9+0:0];
            1: aligned_data[9:0] <= rec_data[9+1:1];
            2: aligned_data[9:0] <= rec_data[9+2:2];
            3: aligned_data[9:0] <= rec_data[9+3:3];
            4: aligned_data[9:0] <= rec_data[9+4:4];
            5: aligned_data[9:0] <= rec_data[9+5:5];
            6: aligned_data[9:0] <= rec_data[9+6:6];
            7: aligned_data[9:0] <= rec_data[9+7:7];
            8: aligned_data[9:0] <= rec_data[9+8:8];
            9: aligned_data[9:0] <= rec_data[9+9:9];        
        endcase
    end
end


endmodule