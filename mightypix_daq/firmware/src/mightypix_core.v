
`timescale 1ns / 1ps
`default_nettype none

module mightypix_core (

    //local bus
    input wire BUS_CLK,
    inout wire [7:0] BUS_DATA,
    input wire [15:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    input wire BUS_RST,

    //fifo
    input wire ARB_READY_OUT,
    output wire ARB_WRITE_OUT,
    output wire [31:0] ARB_DATA_OUT,
    input wire FIFO_FULL,
    input wire FIFO_NEAR_FULL,

    //TLU
    output wire TLU_CLK,    //TX0 and RJ45 
    output wire TLU_BUSY,   //TX1 and RJ45
    input wire TLU_RESET,   //RJ45
    input wire TLU_TRIGGER, //RJ45


    //clocks
    input wire conf_clk,
    input wire spi_clk,
    input wire CLK_RX_x0p1,
    input wire CLK_IN,
    input wire CLK_RX,
    input wire CLK_RX_OUT,
    input wire CLK_RX_OUTX2,
    // input wire CLK200,
    input wire CLK400,


    //RX INFO
    output wire soft_reset_rx,
    input wire [3:0] rxdisperr,
    input wire [3:0] rxnotintable,
    input wire rxusrclk,
    input wire rxusrclk2,
    input wire [31:0] rxdata,
    input wire cplllock,
    input wire [3:0] rxcharisk,
    input wire resetdone,
    input wire track_data_out,
    input wire cplllocked,


    // Chip
    inout wire SDA,
    inout wire SCL,
    input  wire DATA,
    output wire SCK,
    output wire CSB,
    output wire MOSI,
    input wire  MISO,
    output wire RST_Ctl_RB,
    output wire Ctl_Clk_1,
    output wire Ctl_Clk_2,
    output wire Ctl_Load,
    output wire Ctl_SIN,
    input wire  Ctl_SOut,
    output wire INJECTION,
    output wire Sync_res,
    output wire RX_READY,
    output wire CLK_ENABLE,

    //external TS
    input wire TS_IN

  );


  assign SCK = 0;
  assign CSB = 1;
  assign MOSI = 0;

  // -------  MODULE ADREESSES  ------- //
  // DO NOT assign 32'h2000-32'h3000 it is for GPAC in mio3!!

  localparam GPIO_BASEADDR = 16'h0010;
  localparam GPIO_HIGHADDR = 16'h0100-1;

  localparam PULSE_INJ_BASEADDR = 16'h4000;
  localparam PULSE_INJ_HIGHADDR = 16'h4100-1;

  localparam PULSE_GATE_TDC_BASEADDR = 16'h0400;
  localparam PULSE_GATE_TDC_HIGHADDR = 16'h0500-1;

  localparam DATA_RX_BASEADDR = 16'h0700;
  localparam DATA_RX_HIGHADDR = 16'h0800-1;

  localparam TLU_BASEADDR = 16'h0200;
  localparam TLU_HIGHADDR = 16'h0300-1;

  localparam TS_BASEADDR = 16'h0500;
  localparam TS_HIGHADDR = 16'h0600-1;

  localparam TS_TLU_BASEADDR = 16'h0800;
  localparam TS_TLU_HIGHADDR = 16'h0900-1;

  // external TS
  localparam TS_INJ_BASEADDR = 16'h01C0;  //addr-width = 12
  localparam TS_INJ_HIGHADDR = 16'h0200-1;


  localparam SPI_BASEADDR = 16'h5000;
  localparam SPI_HIGHADDR = 16'h7000-1;

  localparam I2C_BASEADDR = 32'h2000;
  localparam I2C_HIGHADDR = 32'h3000-1;


  // -------  FPGA version  ------- //
  localparam VERSION = 8'h01;

  reg RD_VERSION;
  always@(posedge BUS_CLK)
    if(BUS_ADD == 16'h0000 && BUS_RD)
      RD_VERSION <= 1;
    else
      RD_VERSION <= 0;
  assign BUS_DATA = (RD_VERSION) ? VERSION : 8'bz;



// reg CLK200;
// always @(posedge CLK400) begin
//     CLK200 <= ~CLK200;
// end
wire CLK200;

clock_divider #(
                  .DIVISOR(2)
                ) i_clock_divisor_two_phase_spi (
                  .CLK(CLK400),
                  .RESET(1'b0),
                  .CE(),
                  .CLOCK(CLK200)
                );


// ------- DATA interface ------- //
wire RX_FIFO_READ, RX_FIFO_EMPTY;
wire [31:0] RX_FIFO_DATA;

wire TS_INJ_FIFO_READ,TS_INJ_FIFO_EMPTY;
wire [31:0] TS_INJ_FIFO_DATA;

wire TLU_FIFO_READ,TLU_FIFO_EMPTY;
wire [31:0] TLU_FIFO_DATA;

wire TS_FIFO_READ,TS_FIFO_EMPTY;
wire [31:0] TS_FIFO_DATA;

wire TS_TLU_FIFO_READ,TS_TLU_FIFO_EMPTY;
wire [31:0] TS_TLU_FIFO_DATA;
assign TS_TLU_FIFO_EMPTY = 1;

wire TS_MON_FIFO_READ,TS_MON_FIFO_EMPTY;
wire [31:0] TS_MON_FIFO_DATA;
wire TS_MON_FIFO_READ_TRAILING,TS_MON_FIFO_EMPTY_TRAILING;
wire [31:0] TS_MON_FIFO_DATA_TRAILING;
// assign TS_MON_FIFO_EMPTY = 1;
// assign TS_MON_FIFO_EMPTY_TRAILING = 1;

wire [31:0] TS_INJ_FIFO_DATA_FALL;
wire [63:0] TIMESTAMP;
wire TS_INJ_FIFO_READ_FALL, TS_INJ_FIFO_EMPTY_FALL;

timestamp_div #(
    .BASEADDR(TS_INJ_BASEADDR),
    .HIGHADDR(TS_INJ_HIGHADDR),
    .IDENTIFIER(8),
    .CLKDV(2),    //CLKW x CLKDV = CLK  
    .DIV_WIDTH(3) // no of bits of log2(CLKDV) +2
) timestamp_div_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .CLK2x(CLK400), 
    .CLK(CLK200), 
    .CLKW(CLK_IN), 
    .DI(TS_IN), // input of timing layer
    .TIMESTAMP_OUT(TIMESTAMP), //timestamp based on CLKW
    .EXT_TIMESTAMP(64'b0),
    .EXT_ENABLE(1'b0),

    .FIFO_READ(TS_INJ_FIFO_READ),
    .FIFO_EMPTY(TS_INJ_FIFO_EMPTY),
    .FIFO_DATA(TS_INJ_FIFO_DATA),

    .FIFO_READ_FALL(TS_INJ_FIFO_READ_FALL),
    .FIFO_EMPTY_FALL(TS_INJ_FIFO_EMPTY_FALL),
    .FIFO_DATA_FALL(TS_INJ_FIFO_DATA_FALL)
);


rrp_arbiter 
#( 
    .WIDTH(5)
) rrp_arbiter
(
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    .WRITE_REQ({ 
                ~TS_INJ_FIFO_EMPTY,
                ~TS_INJ_FIFO_EMPTY_FALL,
                //  ~TS_MON_FIFO_EMPTY_TRAILING, 
                //  ~TS_FIFO_EMPTY,
                 ~RX_FIFO_EMPTY,
                 ~TS_TLU_FIFO_EMPTY,
                 ~TLU_FIFO_EMPTY}),
    .HOLD_REQ({3'b0}),
    .DATA_IN({
              TS_INJ_FIFO_DATA,
              TS_INJ_FIFO_DATA_FALL,
              // TS_MON_FIFO_DATA,
              // TS_MON_FIFO_DATA_TRAILING,
              // TS_FIFO_DATA,
              RX_FIFO_DATA,
              TS_TLU_FIFO_DATA,
              TLU_FIFO_DATA}),
    .READ_GRANT({
                TS_INJ_FIFO_READ,
                TS_INJ_FIFO_READ_FALL,
                //  TS_MON_FIFO_READ,
                //  TS_MON_FIFO_READ_TRAILING, 
                //  TS_FIFO_READ,
                 RX_FIFO_READ,
                 TS_TLU_FIFO_READ,
                 TLU_FIFO_READ}),
    .READY_OUT(ARB_READY_OUT),
    .WRITE_OUT(ARB_WRITE_OUT),
    .DATA_OUT(ARB_DATA_OUT)
    );
 

 // ---- USER MODULES _-------- //

//GPIO
  wire [15:0] GPIO_OUT;
  gpio
    #(
      .BASEADDR(GPIO_BASEADDR),
      .HIGHADDR(GPIO_HIGHADDR),
      .IO_WIDTH(16),
      .IO_DIRECTION(16'hffff)
    ) gpio
    (
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(BUS_ADD),
      .BUS_DATA(BUS_DATA),
      .BUS_RD(BUS_RD),
      .BUS_WR(BUS_WR),
      .IO(GPIO_OUT)
    );


  wire SW_SLOW_RX_CONF,EN_FAKE_Data,DISABLE_REF_CLK,DISABLE_CLK_EXT; 
  assign EN_FAKE_Data     = GPIO_OUT[1];
  assign Sync_res         = GPIO_OUT[2];
  assign CLK_ENABLE        = ~GPIO_OUT[3];
  assign DISABLE_REF_CLK        = GPIO_OUT[4];
  assign DISABLE_CLK_EXT        = GPIO_OUT[5];
  assign SW_SLOW_RX_CONF  = GPIO_OUT[9];
  // assign RST_Ctl_RB = GPIO_OUT[6];



  // ------- MODULES for GPAC - I2C module  ------- //
  wire I2C_CLK;

  clock_divider #(
                  .DIVISOR(1500)
                ) i_clock_divisor_spi (
                  .CLK(BUS_CLK),
                  .RESET(1'b0),
                  .CE(),
                  .CLOCK(I2C_CLK)
                );

  i2c
    #(
      .BASEADDR(I2C_BASEADDR),
      .HIGHADDR(I2C_HIGHADDR),
      .ABUSWIDTH(32),
      .MEM_BYTES(64)
    )  i_i2c
    (
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(BUS_ADD),
      .BUS_DATA(BUS_DATA),
      .BUS_RD(BUS_RD),
      .BUS_WR(BUS_WR),

      .I2C_CLK(I2C_CLK),
      .I2C_SDA(SDA),
      .I2C_SCL(SCL)
    );

  ////////////////////////////////////////////
  //TLU
  wire TRIGGER_ACKNOWLEDGE_FLAG,TRIGGER_ACCEPTED_FLAG;
  assign TRIGGER_ACKNOWLEDGE_FLAG = TRIGGER_ACCEPTED_FLAG;
  wire [63:0] TIMESTAMP_TLU;
  tlu_slave #(
      .BASEADDR(TLU_BASEADDR),
      .HIGHADDR(TLU_HIGHADDR),
      .DIVISOR(8)
  ) i_tlu_slave (
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(BUS_ADD),
      .BUS_DATA(BUS_DATA),
      .BUS_RD(BUS_RD),
      .BUS_WR(BUS_WR),
      
      .TRIGGER_CLK(CLK400),
      
      .FIFO_READ(TLU_FIFO_READ),
      .FIFO_EMPTY(TLU_FIFO_EMPTY),
      .FIFO_DATA(TLU_FIFO_DATA),
      .FIFO_PREEMPT_REQ(),
      
      .TRIGGER_ENABLED(),
      .TRIGGER_SELECTED(),
      .TLU_ENABLED(),
      //.TRIGGER({8'b0}),
      .TRIGGER({7'b0, TLU_TRIGGER}),
      .TRIGGER_VETO({7'b0,FIFO_FULL}),
      .TIMESTAMP_RESET(),
      //.EXT_TRIGGER_ENABLE(),     //.EXT_TRIGGER_ENABLE(TLU_EXT_TRIGGER_ENABLE)
      .EXT_TRIGGER_ENABLE(1'b0),
      .TRIGGER_ACKNOWLEDGE(TRIGGER_ACKNOWLEDGE_FLAG),
      .TRIGGER_ACCEPTED_FLAG(TRIGGER_ACCEPTED_FLAG),

      .TLU_TRIGGER(TLU_TRIGGER),
      .TLU_RESET(TLU_RESET),
      .TLU_BUSY(TLU_BUSY),
      .TLU_CLOCK(TLU_CLK),
      .EXT_TIMESTAMP(),
      .TIMESTAMP(TIMESTAMP_TLU)
  );



////////////////////////////////////////////
// SR config of DUT
  //INFO: Toko uses CLK20 as SPIclk, we try it a little bit slower
// wire RST_Ctl_RB1;
two_phase_spi #(
    .BASEADDR(SPI_BASEADDR),
    .HIGHADDR(SPI_HIGHADDR),
    .MEM_BYTES(1024)
) two_phase_spi_conf (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(spi_clk),

    .SCLK1(Ctl_Clk_1),
    .SCLK2(Ctl_Clk_2),
    .SDO(Ctl_SOut),
    .SDI(Ctl_SIN),
    .SRB(RST_Ctl_RB),
    .SLD(Ctl_Load),

    .EXT_START(1'b0),
    .SEN()
);


////////////////////////////////////////////
// Pulse injector
  wire GATE_TDC;

  pulse_gen
    #(
      .BASEADDR(PULSE_INJ_BASEADDR),
      .HIGHADDR(PULSE_INJ_HIGHADDR)
    )     pulse_gen_inj(
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(BUS_ADD),
      .BUS_DATA(BUS_DATA),
      .BUS_RD(BUS_RD),
      .BUS_WR(BUS_WR),

      .PULSE_CLK(CLK_RX_x0p1),
      .EXT_START(GATE_TDC),
      .PULSE(INJECTION)
    );


  wire FE_FIFO_READ, FE_FIFO_EMPTY;
  wire [31:0] FE_FIFO_DATA;

  wire SPI_FIFO_READ;
  wire SPI_FIFO_EMPTY;
  wire [31:0] SPI_FIFO_DATA;
  assign SPI_FIFO_EMPTY = 1;



////////////////////////////////////////////
//Data rx

  wire RX_CLK;
  mightypix_data_rx #(
                      .BASEADDR(DATA_RX_BASEADDR),
                      .HIGHADDR(DATA_RX_HIGHADDR)
                    ) mightypix_data_rx (
                    
                      // clocks
                      .RX_CLK(CLK_RX),
                      .CLK_RX_OUT(CLK_RX_OUT),
                      .CLK_RX_OUTX2(CLK_RX_OUTX2),

                      //Fast receiver 
                      .soft_reset_rx(soft_reset_rx),
                      .rxdisperr(rxdisperr),
                      .rxnotintable(rxnotintable),
                      .rxusrclk(rxusrclk),
                      .rxusrclk2(rxusrclk2),
                      .rxdata(rxdata),
                      .rxcharisk(rxcharisk),
                      .resetdone(resetdone),
                      .track_data_out(track_data_out),
                      .cplllocked(cplllocked),

                      // chips
                      .RX_READY(RX_READY),
                      .RX_DATA(DATA),
                      .FIFO_READ(RX_FIFO_READ),
                      .FIFO_EMPTY(RX_FIFO_EMPTY),
                      .FIFO_DATA(RX_FIFO_DATA),
                      .FPGA_TIMESTAMP(TIMESTAMP[47:0]),

                      // Bus
                      .BUS_CLK(BUS_CLK),
                      .BUS_RST(BUS_RST),
                      .BUS_ADD(BUS_ADD),
                      .BUS_DATA(BUS_DATA),
                      .BUS_RD(BUS_RD),
                      .BUS_WR(BUS_WR)
                    );


endmodule
