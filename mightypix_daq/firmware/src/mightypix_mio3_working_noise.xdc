# constrains based on MIO ucf file
# ------ Constraints
# FCLK (100 MHz)
set_property PACKAGE_PIN AA3 [get_ports FCLK_IN]
set_property IOSTANDARD LVCMOS15 [get_ports FCLK_IN]


create_clock -period 10.000 -name FCLK_IN -add [get_ports FCLK_IN]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]

# Derived Clocks
create_generated_clock -name clock_divider/conf_clk_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 80 [get_pins clock_divider/CLOCK_reg/Q]
create_generated_clock -name clock_divider_x5/conf_clk_x5_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 16 [get_pins clock_divider_x5/CLOCK_reg/Q]
create_generated_clock -name mightypix_core/i_clock_divisor_spi/I2C_CLK -source [get_pins PLLE2_BASE_inst_comm/CLKOUT0] -divide_by 1500 [get_pins mightypix_core/i_clock_divisor_spi/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]
create_clock -period 8.000 -name CLK_MGT_REF -add [get_ports MGT_REFCLK1_P]

set_false_path -from [get_clocks CLK_IN] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK_IN]

set_false_path -from [get_clocks CLK_RX_x0p1] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK_RX_x0p1]


set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks rgmii_rxc]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]

# set_false_path -from [get_clocks CLK_RX_x2] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK_RX_x2]

set_false_path -from [get_clocks CLK_RX] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK_RX]

set_false_path -from [get_clocks CLK_REF_IN] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK_REF_IN]

set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX]

set_false_path -from [get_clocks CLK_REF_IN] -to [get_clocks CLK_RX]
set_false_path -from [get_clocks CLK_RX] -to [get_clocks CLK_REF_IN]

set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks CLK125PLLTX]
set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks rgmii_rxc]

set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks clock_divider/conf_clk_div]
set_false_path -from [get_clocks clock_divider/conf_clk_div] -to [get_clocks BUS_CLK_PLL]

#  Exclude asynchronous clock domains from timing (handled by CDCs)
set_clock_groups -asynchronous -group {BUS_CLK_PLL clock_divider_x5/conf_clk_x5_div} -group {CLK_IN CLK_REF_IN CLK_RX CLK_RX_x0p1 CLK_RX_x0p5 CLK_RX_OUT} -group mightypix_core/i_clock_divisor_spi/I2C_CLK -group [get_clocks -include_generated_clocks rgmii_rxc] -group [get_clocks -include_generated_clocks CLK_MGT_REF]



set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]] -to [get_clocks fast_input_wraper_inst/fast_data_input_support_i/inst/fast_data_input_init_i/fast_data_input_i/gt0_fast_data_input_i/gtxe2_i/RXOUTCLK]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT4]] -to [get_clocks fast_input_wraper_inst/fast_data_input_support_i/inst/fast_data_input_init_i/fast_data_input_i/gt0_fast_data_input_i/gtxe2_i/RXOUTCLK]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT5]] -to [get_clocks fast_input_wraper_inst/fast_data_input_support_i/inst/fast_data_input_init_i/fast_data_input_i/gt0_fast_data_input_i/gtxe2_i/RXOUTCLK]
set_false_path -from [get_pins {mightypix_core/mightypix_data_rx/core/ireceiver_logic/userk_cdc_syncfifo_i/wptr_full_inst/wptr_reg[1]/C}] -to [get_pins {mightypix_core/mightypix_data_rx/core/ireceiver_logic/userk_cdc_syncfifo_i/sync_w2r_inst/cdc_sync_rq1_wptr_reg[1]/D}]
set_false_path -from [get_clocks fast_input_wraper_inst/fast_data_input_support_i/inst/fast_data_input_init_i/fast_data_input_i/gt0_fast_data_input_i/gtxe2_i/RXOUTCLK] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]]


#set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks mightypix_core/i_clock_divisor_spi/I2C_CLK]
#set_false_path -from [get_clocks mightypix_core/i_clock_divisor_spi/I2C_CLK] -to [get_clocks BUS_CLK_PLL]


# set_false_path -from [get_clocks CLK250PLL] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK250PLL]

# set_false_path -from [get_clocks CLK125PLLTX90] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX90]

# set_false_path -from [get_clocks CLK125PLLRX] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLRX]

# set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX]

# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks rgmii_rxc]
# set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]

# set_false_path -from [get_clocks I2C_CLK] -to [get_clocks BUS_CLK_PLL]
# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks I2C_CLK]

# ------ LED
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]

set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[0]}]

# CLK Mux
set_property PACKAGE_PIN K25 [get_ports MGT_REF_SEL]
set_property IOSTANDARD LVCMOS25 [get_ports MGT_REF_SEL]
set_property PULLUP true [get_ports MGT_REF_SEL]

set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

## PMOD
##  ____________
## |1 2 3 4  G +|  First PMOD channel (4 signal lines, ground and vcc)
## |7_8_9_10_G_+|  Second PMOD channel ("")
##
## PMOD connector Pin10-->PMOD0; Pin9-->PMOD1; Pin8-->PMOD2; Pin7-->PMOD3;
#set_property PACKAGE_PIN AC23 [get_ports {PMOD[0]}]
#set_property PACKAGE_PIN AC24 [get_ports {PMOD[1]}]
#set_property PACKAGE_PIN W25 [get_ports {PMOD[2]}]
#set_property PACKAGE_PIN W26 [get_ports {PMOD[3]}]
## PMOD connector Pin4-->PMOD4; Pin3-->PMOD5; Pin2-->PMOD6; Pin1-->PMOD7;
#set_property PACKAGE_PIN AA25 [get_ports {PMOD[4]}]
#set_property PACKAGE_PIN AB25 [get_ports {PMOD[5]}]
#set_property PACKAGE_PIN V24 [get_ports {PMOD[6]}]
#set_property PACKAGE_PIN V26 [get_ports {PMOD[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports PMOD*]
## pull down the PMOD pins which are used as inputs
#set_property PULLDOWN true [get_ports {PMOD[0]}]
#set_property PULLDOWN true [get_ports {PMOD[1]}]
#set_property PULLDOWN true [get_ports {PMOD[2]}]
#set_property PULLDOWN true [get_ports {PMOD[3]}]

## ------ Si570 / SMA input CLK
#set_property PACKAGE_PIN D6 [get_ports MGT_REFCLK0_P]
#set_property PACKAGE_PIN D5 [get_ports MGT_REFCLK0_N]
#set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
#set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

# ------ Button & Spare & more - omitted for now
set_property PACKAGE_PIN C18 [get_ports RESET_N]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_N]
set_property PULLUP true [get_ports RESET_N]

# ------ I2C control signals
set_property PACKAGE_PIN P24 [get_ports SDA]
set_property IOSTANDARD LVCMOS25 [get_ports SDA]
set_property PACKAGE_PIN N24 [get_ports SCL]
set_property IOSTANDARD LVCMOS25 [get_ports SCL]

# ------ Trigger IOs - partial (MIO3 has fewer lemo than MIO)
# TX[0]
set_property PACKAGE_PIN AB21 [get_ports TLU_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_CLK]
# TX[1]
set_property PACKAGE_PIN V23 [get_ports TLU_BUSY]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_BUSY]
# set_property PACKAGE_PIN U22 [get_ports {LEMO_RX[0]}]
# set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[0]}]
# set_property PACKAGE_PIN U26 [get_ports {LEMO_RX[1]}]
# set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[1]}]

# ------ Async SRAM - omitted for now
# SRAM faked with SiTCP

# ------ RGMII
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN N16 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN U16 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rxc]
set_property PACKAGE_PIN R21 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN P21 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN P16 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN N17 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN R16 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[3]}]
set_property PACKAGE_PIN R17 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_txc]
set_property PACKAGE_PIN R18 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN P18 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN N18 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN M19 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN U17 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN T17 [get_ports {rgmii_txd[3]}]

# ------ Debugging - empty in old ucf

# ------ GPAC IOs

#set_property PACKAGE_PIN G20 [get_ports {DOUT[19]}] #LVDS 0
#set_property PACKAGE_PIN H19 [get_ports {DOUT[18]}] #LVDS 1
#set_property PACKAGE_PIN J16 [get_ports {DOUT[17]}] #LVDS 2
#set_property PACKAGE_PIN J15 [get_ports {DOUT[16]}] #LVDS 3
#set_property PACKAGE_PIN F15 [get_ports {DOUT[15]}]
#set_property PACKAGE_PIN G15 [get_ports {DOUT[14]}]
#set_property PACKAGE_PIN N22 [get_ports {DOUT[13]}]
#set_property PACKAGE_PIN N21 [get_ports {DOUT[12]}]
#set_property PACKAGE_PIN Y21 [get_ports {DOUT[11]}]
#set_property PACKAGE_PIN W20 [get_ports {DOUT[10]}]
#set_property PACKAGE_PIN AC22 [get_ports {DOUT[9]}]
#set_property PACKAGE_PIN AB22 [get_ports {DOUT[8]}]
#set_property PACKAGE_PIN W24 [get_ports {DOUT[7]}]
#set_property PACKAGE_PIN W23 [get_ports {DOUT[6]}]
#set_property PACKAGE_PIN U25 [get_ports {DOUT[5]}]
#set_property PACKAGE_PIN U24 [get_ports {DOUT[4]}]
#set_property PACKAGE_PIN M26 [get_ports {DOUT[3]}]
#set_property PACKAGE_PIN N26 [get_ports {DOUT[2]}]
#set_property PACKAGE_PIN AE21 [get_ports {DOUT[1]}]
#set_property PACKAGE_PIN AD21 [get_ports {DOUT[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports "DOUT*"]

#set_property PACKAGE_PIN H17 [get_ports {DIN[11]}] #LVDS 3
#set_property PACKAGE_PIN E15 [get_ports {DIN[10]}] #LVDS 2
#set_property PACKAGE_PIN H18 [get_ports {DIN[9]}] #LVDS 1
#set_property PACKAGE_PIN E16 [get_ports {DIN[8]}] #LVDS 0
#set_property PACKAGE_PIN AF23 [get_ports {DIN[7]}]
#set_property PACKAGE_PIN AE23 [get_ports {DIN[6]}]
#set_property PACKAGE_PIN P25 [get_ports {DIN[5]}]
#set_property PACKAGE_PIN R25 [get_ports {DIN[4]}]
#set_property PACKAGE_PIN L24 [get_ports {DIN[3]}]
#set_property PACKAGE_PIN M24 [get_ports {DIN[2]}]
#set_property PACKAGE_PIN T25 [get_ports {DIN[1]}]
#set_property PACKAGE_PIN T24 [get_ports {DIN[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports "DIN*"]

#POWER ON Detection
set_property PACKAGE_PIN AE25 [get_ports POWER_ON]
set_property IOSTANDARD LVCMOS25 [get_ports POWER_ON]
set_property PULLDOWN true [get_ports POWER_ON]

# DOUT[0]
set_property PACKAGE_PIN A19 [get_ports Ctl_SIN]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SIN]
set_property PULLDOWN true [get_ports Ctl_SIN]
# set_property SLEW FAST [get_ports Ctl_SIN]
# set_property DRIVE 16 [get_ports Ctl_SIN]

# DOUT[1]
set_property PACKAGE_PIN A18 [get_ports Ctl_Load]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Load]
set_property PULLDOWN true [get_ports Ctl_Load]
# set_property SLEW FAST [get_ports Ctl_Load]
# set_property DRIVE 16 [get_ports Ctl_Load]

# DOUT[2]
set_property PACKAGE_PIN C16 [get_ports Ctl_Clk_2]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_2]
#set_property DRIVE 16 [get_ports Ctl_Clk_2]
# set_property SLEW FAST [get_ports Ctl_Clk_2]
set_property PULLDOWN true [get_ports Ctl_Clk_2]
# set_property DRIVE 16 [get_ports Ctl_Clk_2]

# DOUT[3]
set_property PACKAGE_PIN B16 [get_ports Ctl_Clk_1]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_1]
# set_property SLEW FAST [get_ports Ctl_Clk_1]
set_property PULLDOWN true [get_ports Ctl_Clk_1]
# set_property DRIVE 16 [get_ports Ctl_Clk_1]

# DOUT[4]
set_property PACKAGE_PIN D13 [get_ports MOSI]
set_property IOSTANDARD LVCMOS25 [get_ports MOSI]
set_property PULLDOWN true [get_ports MOSI]

# DOUT[5]
set_property PACKAGE_PIN U25 [get_ports CSB]
set_property IOSTANDARD LVCMOS25 [get_ports CSB]
set_property PULLDOWN true [get_ports CSB]

# DOUT[6]
set_property PACKAGE_PIN W23 [get_ports SCK]
set_property IOSTANDARD LVCMOS25 [get_ports SCK]
set_property PULLDOWN true [get_ports SCK]

# DOUT[7]
set_property PACKAGE_PIN E15 [get_ports RST_Ctl_RB]
set_property IOSTANDARD LVCMOS25 [get_ports RST_Ctl_RB]
set_property PULLDOWN true [get_ports RST_Ctl_RB]

# # DOUT[8]
# set_property PACKAGE_PIN AB22 [get_ports Def_Cnfg]
# set_property IOSTANDARD LVCMOS25 [get_ports Def_Cnfg]

#set_property PACKAGE_PIN G20 [get_ports {DOUT[19]}] #LVDS 0
#set_property PACKAGE_PIN H19 [get_ports {DOUT[18]}] #LVDS 1
#set_property PACKAGE_PIN J16 [get_ports {DOUT[17]}] #LVDS 2
#set_property PACKAGE_PIN J15 [get_ports {DOUT[16]}] #LVDS 3

# DOUT[19] LVDSOUT 0
# set_property PACKAGE_PIN G20 [get_ports Clk_ext_LVDS]
# set_property IOSTANDARD LVCMOS25 [get_ports Clk_ext_LVDS]
# set_property PULLDOWN true [get_ports Clk_ext_LVDS]
# set_property DRIVE 16 [get_ports Clk_ext_LVDS]
# set_property SLEW FAST [get_ports Clk_ext_LVDS]
set_property PACKAGE_PIN D19 [get_ports Clk_ext_LVDS_P]
set_property PACKAGE_PIN D20 [get_ports Clk_ext_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_N]

# DOUT[18] LVDSOUT 1
# set_property PACKAGE_PIN H19 [get_ports Clk_ref]
# set_property IOSTANDARD LVCMOS25 [get_ports Clk_ref]
# set_property PULLDOWN true [get_ports Clk_ref]
# set_property DRIVE 16 [get_ports Clk_ref]
# set_property SLEW FAST [get_ports Clk_ref]
set_property PACKAGE_PIN J18 [get_ports Clk_ref_P]
set_property PACKAGE_PIN J19 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_P]

# DOUT[17] LVDSOUT 2
# set_property PACKAGE_PIN J16 [get_ports Sync_res]
# set_property IOSTANDARD LVCMOS25 [get_ports Sync_res]
# set_property PULLDOWN true [get_ports Sync_res]
set_property PACKAGE_PIN K20 [get_ports Sync_res_P]
set_property PACKAGE_PIN J20 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_N]

# # DOUT[12] LVDSOUT 3
# set_property PACKAGE_PIN N21 [get_ports {DOUT[12]}]
# set_property IOSTANDARD LVCMOS25 [get_ports ClkSR]

# DIN[0]
set_property PACKAGE_PIN E12 [get_ports Ctl_SOut]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SOut]
set_property PULLDOWN true [get_ports Ctl_SOut]

#DIN[1]
set_property PACKAGE_PIN T25 [get_ports MISO]
set_property IOSTANDARD LVCMOS25 [get_ports MISO]
set_property PULLDOWN true [get_ports MISO]

## DIN[2]
#set_property PACKAGE_PIN M24 [get_ports HitOr]
#set_property IOSTANDARD LVCMOS25 [get_ports HitOr]


#set_property PACKAGE_PIN H17 [get_ports {DIN[11]}] #LVDS 3
#set_property PACKAGE_PIN E15 [get_ports {DIN[10]}] #LVDS 2
#set_property PACKAGE_PIN H18 [get_ports {DIN[9]}] #LVDS 1
#set_property PACKAGE_PIN E16 [get_ports {DIN[8]}] #LVDS 0

# DIN[8] (LVDS0)
set_property PACKAGE_PIN G3 [get_ports DATA_LVDS_N]
set_property PACKAGE_PIN G4 [get_ports DATA_LVDS_P]

# DIN[8] (LVDS0)
# set_property PACKAGE_PIN E16 [get_ports DATA_LVDS]
# set_property IOSTANDARD LVCMOS25 [get_ports DATA_LVDS]
# set_property PACKAGE_PIN C19 [get_ports DATA_LVDS_P]
# set_property PACKAGE_PIN B19 [get_ports DATA_LVDS_N]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_P]
#set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_N]
#set_property PULLDOWN true [get_ports DATA_LVDS_P]
#set_property PULLDOWN true [get_ports DATA_LVDS_N]
#set_property DIFF_TERM TRUE [get_ports DATA_LVDS_P]
#set_property DIFF_TERM TRUE [get_ports DATA_LVDS_N]


# INJECTION connected to INJ_STRB
set_property PACKAGE_PIN A15 [get_ports INJECTION]
set_property IOSTANDARD LVCMOS25 [get_ports INJECTION]
set_property DRIVE 16 [get_ports INJECTION]
set_property SLEW FAST [get_ports INJECTION]

set_property PACKAGE_PIN V21 [get_ports TLU_RESET]
set_property PACKAGE_PIN Y25 [get_ports TLU_TRIGGER]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_TRIGGER]

# set_property PACKAGE_PIN G12 [get_ports InjLoopOut]
# set_property IOSTANDARD LVCMOS25 [get_ports InjLoopOut]
# set_property SLEW FAST [get_ports InjLoopOut]
# set_property DRIVE 16 [get_ports InjLoopOut]

# set_property PACKAGE_PIN AD21 [get_ports DEBUG]
# set_property IOSTANDARD LVCMOS25 [get_ports DEBUG]
# set_property SLEW FAST [get_ports DEBUG]
# set_property DRIVE 16 [get_ports DEBUG]

# set_property PACKAGE_PIN F12 [get_ports InjLoopIn]
# set_property IOSTANDARD LVCMOS25 [get_ports InjLoopIn]
















# set_max_delay -from [get_pins fast_input_wraper_inst/fast_data_input_support_i/inst/fast_data_input_init_i/fast_data_input_i/gt0_fast_data_input_i/gtxe2_i/RXUSRCLK2] -to [get_pins mightypix_core/mightypix_data_rx/core/ireceiver_logic/cdc_fifo_write_input_reg/D] 0.500

# set_max_delay -from [get_pins sitcp/SiTCP/SiTCP_INT/SiTCP_RESET_OUT/C] -to [get_pins {i_data_fifo/fifo_i/wr_pointer_reg[10]_rep/R}] 0.500

# set_max_delay -from [get_clocks rgmii_rxc] -to [get_clocks rgmii_rxc] 0.500

# SiTCP
# set_max_delay -datapath_only -from [get_clocks CLK125PLLTX] -to [get_ports {rgmii_txd[*]}] 3.500
# set_max_delay -datapath_only -from [get_clocks CLK125PLLTX] -to [get_ports rgmii_tx_ctl] 3.500
# set_max_delay -datapath_only -from [get_clocks CLK125PLLTX90] -to [get_ports rgmii_txc] 3.000

# set_input_delay -clock [get_clocks rgmii_rxc] -clock_fall -max -add_delay 4.000 [get_ports {rgmii_rxd[*]}]
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 8.000 [get_ports {rgmii_rxd[*]}]
# set_input_delay -clock [get_clocks rgmii_rxc] -clock_fall -max -add_delay 4.000 [get_ports rgmii_rx_ctl]
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 8.000 [get_ports rgmii_rx_ctl]
