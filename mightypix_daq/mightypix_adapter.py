"""
This is the main class for the Mightypix daq that controlls the chip.
This is based on basil code from SiLab most of the credit goes to them and all the bugs are my fault!


Author: LHCb Bonn
contact: Klaas Padeken
"""
import sys
import time
import os
import bitarray
from bitarray.util import int2ba
from basil.dut import Dut
import tables as tb

import numpy as np
import logging

import collections
import pkg_resources

sys.path = [os.path.dirname(os.path.abspath(__file__))] + sys.path
OUTPUT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "output_data")

VERSION = pkg_resources.get_distribution("mightypix_daq").version

loglevel = logging.DEBUG

""" Set up main self.logger """
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(format="%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s")

logger = logging.getLogger("mightypix")
logger.setLevel(loglevel)


def mk_fname(ext="data.npy", dirname=None):
    """Generate a filename in in the output folder."""
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(OUTPUT_DIR, prefix)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)


class MightyPix(Dut):
    """
    This is the main class for the control of the MightyPix.

    The default __init__() will load all necessary software inits, but the HW will be initialized with the init() method.
    """

    # from Tomek typically bit[0] is the last bit that is send!
    default_yaml = os.path.dirname(os.path.abspath(__file__)) + os.sep + "mightypix_adapter.yaml"

    def __init__(self, conf=None, **kwargs):
        """Initialize the software components of the program."""
        self.isMIO3 = True
        if self.isMIO3:
            self.default_yaml = self.default_yaml.replace(".yaml", "_mio3.yaml")
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        if conf is not None:
            self.conf = conf
        else:
            self.conf = os.path.join(self.proj_dir, "mighty_daq", self.default_yaml)
        # this is just a convention
        # for the mio we will use "FIFO" for the mio3 we will use "fifo"
        self.which_fifo = "FIFO"
        if self.isMIO3:
            self.which_fifo = "fifo"
        # Initialize logger.
        self.logger = logging.getLogger()
        logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] [%(threadName)-10s] [%(filename)-15s] [%(funcName)-15s] %(message)s")
        fname = mk_fname(ext="log.log")
        fileHandler = logging.FileHandler(fname)
        fileHandler.setFormatter(logFormatter)

        self.logger.addHandler(fileHandler)
        for ilogger in self.logger.handlers:
            if isinstance(ilogger, logging.StreamHandler):
                ilogger.setFormatter(logFormatter)

        super(MightyPix, self).__init__(conf=self.conf)
        self.COL_SIZE = self._conf["ncols"]
        self.ROW_SIZE = self._conf["nrows"]
        self._initialize_pixel_config()
        self.mask = self._create_mask("none")
        self.injection = self._create_mask("none")
        self.trim = np.zeros([self.COL_SIZE, self.ROW_SIZE], np.uint8)
        self.isInitialized = False
        self.logger.info(f"We are using a MIO{'3' if self.isMIO3 else '1'}")
        self.logger.info(f"Used config file is {self.conf}")
        self.debug = 1
        self.inj_device = "GPAC_MightyPix"
        self.SET_VALUE = {}

    def init(self, dut=None, no_power_reset=True):
        """Initialize the HW part of the chip."""
        count = 0
        while 1:
            if count > 100:
                raise Exception("USB could not be initialized\n")
            try:
                super(MightyPix, self).init()
                break
            except (OSError):
                e = sys.exc_info()
                self.logger.error(str(e))
                count += 1
                time.sleep(0.1)

        # for now we just hardcode this here
        self.board_version = 1  # TODO Docu: maybe a library with different board versions?
        self.logger.info("MightyPix initialized at " + time.strftime("%Y-%m-%d_%H:%M:%S"))
        
        
        self.fw_version = self["intf"].read(0x0, 1)[0]  # FIXME this does not work??
        self.logger.info("Firmware version: %s" % (self.fw_version))

        for reg in self._conf["registers"]:

            # if reg["name"] in ["INJ_HIGH", "INJ_LOW"] and "init" in reg:
            if reg["name"] in ["INJ_HI", "INJ_LO"] and "init" in reg:
                self.logger.info("modify %s: %s" % (reg["name"], str(reg["init"])))
                self[reg["name"]]._ch_cal[reg["arg_add"]["channel"]].update(reg["init"])

        self.send_reset()
        self["inj"].reset()
        self["CONF_SR"].set_size(992)  # TODO Docu: What means SR? -> this is the shift register config
        self["CONF_SR"]._reverse_order_fields = True
        self["data_rx"].REORDER = True #this reorders the dataword 21 -> 12
        self["data_rx"].ENABLE = True #this reorders the dataword 21 -> 12

        self.power_up()
        time.sleep(0.01)
        self.write_global_conf()
        self.set_inj_all()
        self.logger.info("Just to check here should be a 0 : %s" % (self["intf"].read(0x0, 1)[0]))
        self.logger.info("RX status:\n%s" % (self.get_rx_status()))
        self.isInitialized = True

    # def set_mgt_ref(self, value):
    #     '''Controls the clock multiplexer chip, which is used for providing the Aurora RX reference clock'''
    #     if value == "int":
    #         logger.info('MGT: Switching to on-board (Si570) oscillator')
    #         self['CONF']['MGT_REF_SEL'] = 0
    #         self['CONF'].write()
    #     elif value == "ext":
    #         logger.info('MGT: Switching to external (SMA) clock source')
    #         self['CONF']['MGT_REF_SEL'] = 1
    #         self['CONF'].write()

    # def get_mgt_ref(self):
    #     value = self['CONF']['MGT_REF_SEL']
    #     if value == 0:
    #         return 'int'
    #     elif value == 1:
    #         return 'ext'


    def _create_mask(self, pix):
        """
        Create a mask of the same dimensions as the chip's dimensions.

        Parameters
        ----------
        pix: string OR list OR tuple OR np.ndarray
            - valid strings as input: "all" (all 1s) or "none" (all 0s)
            - list, tuple or np.ndarray:
                - Single pixel: e.g. pix=[20,60]
                - Matrix of the same dimensions as the chip
                - A list of pixels: e.g. pix=[[20,60],[20,61],[20,62]]
        """
        mask = np.empty([self.ROW_SIZE, self.COL_SIZE])
        mask.fill(np.NaN)
        # A string as input: "all" (all 1s) or "none" (all 0s)
        if isinstance(pix, str):
            if pix == "all":
                mask.fill(1)
            elif pix == "none":
                mask.fill(0)
            else:  # shouldnt here be an error raise saying that only "all" or "none" string is allowed
                pass
        # A list, tuple or np.ndarray as input
        elif isinstance(pix, (list, tuple, np.ndarray)):
            mask.fill(0)
            if len(pix) > 0:
                # Single pixel format: e.g. pix=[20,60]
                if isinstance(pix[0], int):
                    mask[pix[0], pix[1]] = 1
                # A matrix of the same dimensions as the chip
                elif len(pix) == self.COL_SIZE and len(pix[0]) == self.ROW_SIZE:
                    mask[:, :] = np.array(pix, np.bool)
                # A list of pixels: e.g. pix=[[20,60],[20,61],[20,62]]
                else:
                    for p in pix:
                        if len(p) == 2 and isinstance(p[0], int) and isinstance(p[1], int):
                            mask[p[0], p[1]] = 1
                        else:
                            self.logger.info("The listed item {0:s} does not correspond to a valid pixel format.".format(p))
                            mask.fill(np.NaN)
                            break
            else:
                self.logger.info("No pixels given as input for mask creation.")
        else:
            self.logger.info("You have not specified a valid input for mask creation. Please check the code documentation.")
        return mask

    def _pixel_bitorder(self, debug=False):
        """
        Correctly orders the PIXEL_CONF into the bit string that is needed for programming.

        Return
        ----------
        return_bits: bitarray.bitarray
            bits for the configuration
        """
        debug_string = ""
        return_bits = bitarray.bitarray(0)
        # loop over all columns
        # TODO fix the col loop order! At the moment it should be irrelevant if the columns are reversed or not
        # from the mupiox config one expects that col 28 is first!
        for col in range(self.COL_SIZE):
            # for col in range(self.COL_SIZE - 1, -1, -1):
            # loop over all entries of the pixel config
            colbit = bitarray.bitarray(0)
            # for this order we see noise
            for register in self.PIXEL_CONF:  # TODO Docu: per column 29 register including all info (hit buffer...)
                # but also for this order??
                # for register in reversed(self.PIXEL_CONF):
                # define a bit as the column of each register
                bit = self.PIXEL_CONF[register][col]
                # TODO  Docu: only even columns are ascending and all rows excluding RAM_Write_Row and Pixel_Write_ROW
                if col % 2 != 0 and (("ROW" in register) or ("PIX_WR" in register)):
                    return_bits.extend(bit[::-1])  # reverse bit order
                    colbit.extend(bit[::-1])
                else:
                    return_bits.extend(bit[::1])  # not reverse bit order
                    colbit.extend(bit[::1])
            # self.logger.debug(f"col: {col} bin: {colbit}")
            # self.logger.info(f"col: {col} bin: {colbit}")
            debug_string += f"col: {col} bin: {colbit}\n"
        # we can see noise for return_bits and for return_bits[::-1] ??
        # but for return_bits[::-1] the current consumption is lower
        if debug:
            return debug_string
        # the bit ordering needs to be inverted here!!!
        # return bitarray.bitarray(return_bits[::-1])
        return bitarray.bitarray(return_bits)

    def send_reset(self):
        """Send a reset to the FPGA (not the chip)."""
        self["CONF"]["RESET"] = 1
        self["CONF"].write()
        self["CONF"]["RESET"] = 0
        self["CONF"].write()

    def _initialize_pixel_config(self):
        """
        Create the initial data structure for the pixel configuration.

        no parameters and no returns
        """
        # for each of the columns there is a set of 5 bits that
        self.PIXEL_CONF = collections.OrderedDict()
        # //bit 0,1,2,3:              hit buffer cel 0-3
        # //bit 4:                    pixel ram for all 3 cells
        # //bit 5-16:                 5 bit hit buffer RAm write and 7 bit Wr RAM
        # //bit 5,6,7,8,9:            WR hit buffer row (col even: ascending order; col odd: descending order)!!!
        # //bit 10,11,12,13,14,15,16: pixel WR ram (col even: ascending order; col odd: descending order)!!!
        # //bit 17,18,19,20,21:       enInj row (col even: ascending order; col odd: descending order)!!!
        # //bit 22:                   HB_B column
        # //bit 23:                   Amp column
        # //bit 24:                   enInj col
        self.PIXEL_CONF["HIT_BUFFER_RAM"] = [bitarray.util.zeros(4) for i in range(self.COL_SIZE)]
        self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"] = [bitarray.bitarray("0") for i in range(self.COL_SIZE)]  # only one bit    #why is this one allocated as a string?
        self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_ROW"] = [bitarray.util.zeros(5) for i in range(self.COL_SIZE)]
        self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_PIX_WR"] = [bitarray.util.zeros(7) for i in range(self.COL_SIZE)]
        # Notice there are 3 RAM write signals/pixel row. There can be up to 124 pixels in one column,  ## TODO Docu: Put this at the beginning of the function: First the explanation. Second the code?
        # however only 62 with in pixel RAM. Since there are only 62 rows with pixel RAM, there are
        # 62x3=186 RAM write signals in total. The lowest 62 pixel never have RAM, the first row that
        # can have RAM is row62.
        # Example: RAM(0:6) means RAM(0:2)_row62, RAM(0:2)_row63, RAM(0)_row64
        self.PIXEL_CONF["INJ_ROW_EN"] = [bitarray.util.zeros(5) for i in range(self.COL_SIZE)]
        self.PIXEL_CONF["HB_COL_EN"] = [bitarray.bitarray("0") for i in range(self.COL_SIZE)]
        # Bit 22= enableB (active 0) hit bus for the column – hit bus is the OR function of receiver
        # outputs (after local enable bit) in all hit buffers. Hit bus is designed to be fast – delay 10 ns
        # with respect to hit should be possible.
        self.PIXEL_CONF["AMP_COL_EN"] = [bitarray.bitarray("0") for i in range(self.COL_SIZE)]
        self.PIXEL_CONF["INJ_COL_EN"] = [bitarray.bitarray("0") for i in range(self.COL_SIZE)]
        self.enable_column()

    # This enables the columns, one can specify the matrix a,b,c,d.
    # It also resets all the other values in the pixel
    def enable_column(self, sub_matrix=["a", "b", "c", "d"], enable_col="all"):
        """
        Enable all columns that are specified.

        All other pixel variables will be reset!

        Parameters
        ----------
        sub_matrix: list
            - valid values are: "a", "b", "c", "d"
        enable_col: string or list
            - string only "all" is a valid parameter
            - list:
                - This should contain columns that should be enabled e.g. [0,2,5]
        """
        column_ranges = {
            "a": [0, 8],
            "b": [8, 13],
            "c": [13, 24],
            "d": [24, 29],
        }

        for col in range(self.COL_SIZE - 1, -1, -1):
            # all values to 0 first for RAM
            self.PIXEL_CONF["HIT_BUFFER_RAM"][col] = bitarray.bitarray("0000")
            self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] = bitarray.bitarray("0")
            # set default values for all non RAM values
            self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_ROW"][col] = bitarray.util.zeros(5)
            self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_PIX_WR"][col] = bitarray.util.zeros(7)
            self.PIXEL_CONF["INJ_ROW_EN"][col] = bitarray.util.zeros(5)
            self.PIXEL_CONF["HB_COL_EN"][col] = bitarray.bitarray("1")
            self.PIXEL_CONF["AMP_COL_EN"][col] = bitarray.bitarray("0")
            self.PIXEL_CONF["INJ_COL_EN"][col] = bitarray.bitarray("0")
            if enable_col != "all":
                if col not in enable_col:
                    continue
            for isub in sub_matrix:
                if col >= column_ranges[isub][0] and col < column_ranges[isub][1]:
                    self.PIXEL_CONF["HIT_BUFFER_RAM"][col] = bitarray.bitarray("1111")
                    self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] = bitarray.bitarray("1")
                    # i dont see the loop condition for the part if enable_col = "all"

    # This will enable the Ampout
    # note it will not reset the configuration after writing!
    def enable_ampout(self, col=0):
        """Set the ampout for one column."""
        self.PIXEL_CONF["AMP_COL_EN"][col] = bitarray.bitarray("1")

    def enable_hitbus(self, col=0):
        """Set the hitbus for one column."""
        self.PIXEL_CONF["HB_COL_EN"][col] = bitarray.bitarray("0")

    # This will enable the injection
    # note it will not reset the configuration after writing!
    def enable_injection(self, col=0, row=0):
        """
        Set the injection mask.

        The main reason for this function is to translate into the python array coordinate system.
        """
        self.injection[row, col] = 1

    def enable_mask(self, col=0, row=0):
        """
        Set the disable mask.

        This will sent the mask for pixel col, row.
        This will disable this pixel (sorry for the name).
        It mainly tries to avoid x,y confusion with the numpy array.
        """
        self.mask[row, col] = 1

    def set_inj_en(self, pix="none"):
        """
        Create a mask based on the pixels given as parameter and writes it to the chip in order to enable INJECTION.

        Parameters
        ----------
        pix: string OR list OR tuple OR np.ndarray
            - valid strings as input: "all" (all 1s) or "none" (all 0s)
            - list, tuple or np.ndarray:
                - Single pixel: e.g. pix=[20,60]
                - Matrix of the same dimensions as the chip
                - A list of pixels: e.g. pix=[[20,60],[20,61],[20,62]]
        """
        mask = self._create_mask(pix)
        # the idea is that for each row we have to wite the enable for len(mask[:,y]) pixels
        for y in range(self.ROW_SIZE):
            # if this row in all col should be enabled we should write the config:
            if bool(np.any(mask[y, :])):
                for x in range(self.COL_SIZE):
                    if not bool(mask[y, x]):
                        self.enable_injection(col=x, row=y)
            self.write_current_config()

    def set_tdac(self, tdac, overwrite=False):
        # TODO: this is just a copy form monopix at the moment it just writes an unused array!!!!!!!!!!
        # TODO: Maybe better to make this function write all the bit masks only after all four have been changed, and not one by one.
        """
        Writes the 4-bit mask of trim values to the whole pixel matrix.

        Parameters
        ----------
        tdac: int or np.ndarray
            A single trim value to be written to the whole matrix, or a specific matrix with the same dimensions as the chip and every single value to be written.
        """
        self.logger.error("To implement this function!")
        mask = np.copy(self.PIXEL_CONF["Trim"])
        if isinstance(tdac, int):
            mask.fill(tdac)
            self.logger.info("Setting a single TDAC/TRIM value to all pixels: {0:s}".format(str(tdac)))
        elif np.shape(tdac) == np.shape(mask):
            self.logger.info("Setting a TDAC matrix of valid dimensions.")
            mask = np.array(tdac, dtype=np.uint8)
        else:
            self.logger.error("The input tdac parameter must be int or array of size [{0:d},{1:d}]".format(self.chip_props["COL_SIZE"], self.chip_props["ROW_SIZE"]))
            return

        # Unpack the mask as 8 different masks, where the first 4 masks correspond to the 4 Trim bits.
        trim_bits = np.unpackbits(mask)
        trim_bits_array = np.reshape(trim_bits, (self.chip_props["COL_SIZE"], self.chip_props["ROW_SIZE"], 8)).astype(np.bool)
        # Write the mask for every trim bit.
        for bit in range(4):
            trim_bits_sel_mask = trim_bits_array[:, :, 7 - bit]

    def write_global_conf(self):
        """
        Write the global configuration.

        Parameters:
            conf_type: lets you choose what type of configuration you are using
            this is to be removed!
        """
        self.logger.info("Before programming: %s" % (self.power_status()["Power [mW]"]))
        self.logger.info("Writing default configuration")
        self.send_reset()
        size_INJ_ROW_EN = len(self.PIXEL_CONF["INJ_ROW_EN"][0])
        size_HIT_BUFFER_RAM_WRITE_ROW = len(self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_ROW"][0])
        # note: if you enable multiple cols for injection, you always enable the same rows
        # this can be fixed, but I need to think more for that
        INJ_ROW_EN_ALL_COLS = bitarray.util.zeros(size_INJ_ROW_EN * self.COL_SIZE)
        HIT_BUFFER_RAM_WRITE_ROW_ALL_COLS = bitarray.util.zeros(size_HIT_BUFFER_RAM_WRITE_ROW * self.COL_SIZE)
        # self.logger.info(np.swapaxes(self.injection,0,1))
        for row in range(self.ROW_SIZE):
            if np.any(self.injection[row, :]):
                INJ_ROW_EN_ALL_COLS[row] = 1
            # only rows>=62 can have a trim
            # TODO implement Trims
        fname = mk_fname(ext="conf.log")
        f = open(fname, "w+")
        for icol in range(self.COL_SIZE - 1, -1, -1):
            # self.logger.info(f"icol : {icol}")
            for i_write_row in range(size_HIT_BUFFER_RAM_WRITE_ROW - 1, -1, -1):
                # self.logger.info(f"i_write_row : {i_write_row}")
                # this assumes that one writes the same values for each row
                # with self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] =1
                # for col in range(self.COL_SIZE):
                for col in range(self.COL_SIZE - 1, -1, -1):
                    # reset everything to default
                    self.PIXEL_CONF["INJ_COL_EN"][col] = bitarray.bitarray("0")
                    self.PIXEL_CONF["INJ_ROW_EN"][col] = bitarray.util.zeros(size_INJ_ROW_EN)
                    # reset everything to default
                    self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_ROW"][col] = bitarray.util.zeros(size_HIT_BUFFER_RAM_WRITE_ROW)
                    row = icol * size_HIT_BUFFER_RAM_WRITE_ROW + i_write_row
                    # print(icol, i_write_row, row, col,size_HIT_BUFFER_RAM_WRITE_ROW)
                    inj_row_to_write = INJ_ROW_EN_ALL_COLS[col * size_INJ_ROW_EN : (col + 1) * size_INJ_ROW_EN]
                    self.PIXEL_CONF["INJ_ROW_EN"][col] = inj_row_to_write
                    if np.any(self.injection[:, col]):
                        self.PIXEL_CONF["INJ_COL_EN"][col] = bitarray.bitarray("1")
                    if icol == col:
                        self.PIXEL_CONF["HIT_BUFFER_RAM_WRITE_ROW"][col][i_write_row] = 1
                        # self.PIXEL_CONF["HB_COL_EN"][col] = bitarray.bitarray("1")
                        # this is a bit backword we don't write the
                    if row < self.ROW_SIZE and self.mask[row, col]:
                        # if icol == col and row < self.ROW_SIZE and self.mask[row, icol]:
                        self.PIXEL_CONF["HIT_BUFFER_RAM"][col] = bitarray.bitarray("0000")
                        self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] = bitarray.bitarray("0")
                        # self.PIXEL_CONF["HB_COL_EN"][icol] = bitarray.bitarray("0")
                    else:
                        self.PIXEL_CONF["HIT_BUFFER_RAM"][col] = self.PIXEL_CONF["HIT_BUFFER_RAM"][col] & bitarray.bitarray("1111")
                        self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] = self.PIXEL_CONF["HIT_BUFFER_RAM_ALL"][col] & bitarray.bitarray("1")
                        # self.PIXEL_CONF["HB_COL_EN"][icol] = bitarray.bitarray("1")
                f.write(f"This is i_write_row {i_write_row} icol {icol}\n")
                # just for debug:
                debug_string = self._pixel_bitorder(debug=True)
                f.write(debug_string)
                f.write("\n")
                self.write_current_config()
        f.close()
        time.sleep(0.01)
        self.logger.info("After programming: %s" % (self.power_status()["Power [mW]"]))

    def write_current_config(self):
        """
        Set the Pixel part of the config with the correct bit oder and write the config.

        It will wait until the configuration is finished!
        """
        self["CONF_SR"].set_configuration({"Pixels": self._pixel_bitorder()})
        self["CONF_SR"].write()
        while not self["CONF_SR"].is_ready:
            time.sleep(0.001)

    # power
    def power_up(
        self,
        VSSA=1.2,
        VGate=2.1,
        # VGate=0,
        VDDD=1.8,
        VDDA=1.8,
        TH1=1.4,
        TH2=1.1,
        TH3=1.05,
        VDDR=1.6,
        BL=1.0,
        Vminus=1.0,  # not used
        VCasc2=0.0,  # not used
        NTC=5,
        # INJ_HI=126,
        # INJ_LO=126,
    ):
        """Power up everything with the default settings. Note that setting the power manually might not work, because there is some high currents in the GPAC_MightyPix startup."""
        # DACS #set only value not channel

        self.set_global_voltage(
            Vminus=0.8,  # vminus = VSRC0
            TH1=TH1,  # th1, thfine = VSRC1
            BL=BL,  # bl = VSRC2
            VDDR=VDDR,  # vddram = VSRC3
            TH2=TH2,  # th2 = VSRC4, thfast comp
            TH3=TH3,  #  th3 = VSRC5, thslow comp
        )

    def power_down(self):
        """Disables the power."""
        for pwr in [
            "Vminus",
            "TH1",
            "BL",
            "VDDR",
            "TH2",
            "TH3",
        ]:
            self[pwr].set_voltage(0, unit="V")
            self.SET_VALUE[pwr] = 0

    def power_cycle(self, wait=0.1):
        """Switch off and on."""
        self.power_down()
        self.power_up()

    def power_status(self):
        """Loop over all power units and displays the status."""
        status = {}
        full_power = 0.0
        analogue_power = 0.0
        digital_power = 0.0
        for pwr in [
            "Vminus",
            "TH1",
            "BL",
            "VDDR",
            "TH2",
            "TH3",
        ]:
            # If this a line is failing in your code you probably did not init() the chip!
            # Remember: this chip initializes the sw on __init__ and the hw on init!
            # status[pwr + "[V]"] = self[pwr].get_voltage(unit="V")
            status[pwr + "set"] = self.SET_VALUE[pwr]
            status["Power [mW]"] = 0
        return status

    # this function only returns the current power status of the parameterse in the configuration.yaml file. For more meta data, refer to power status
    def power_status_mini(self):
        """Loop over all power units and displays the status."""
        status = {}
        full_power = 0.0
        analogue_power = 0.0
        digital_power = 0.0
        for pwr in [
            "Vminus",
            "TH1",
            "BL",
            "VDDR",
            "TH2",
            "TH3",
        ]:
            # If this a line is failing in your code you probably did not init() the chip!
            # Remember: this chip initializes the sw on __init__ and the hw on init!
            status[pwr] = self.SET_VALUE[pwr]
        # for dac in ["INJ_LO", "INJ_HI"]:
        #     status[dac] = self.SET_VALUE[dac]
        return status

    def get_data(self, wait=0.01, no_inject=False):
        """
        Return data on the FIFO generated by injected pulses.

        Parameters
        ----------
        wait: float
            Period of time (in seconds) between the last injection and read-out of the last batch of data.

        Return
        ----------
        raw: numpy.ndarray
            Read-out data
        """
        count = 0
        while not self["data_rx"]["READY"]:
            time.sleep(0.01)
            count += 1
            if count > 10:
                break
        if count > 10:
            self.logger.warn("Could not get a synchronization lock")
            return np.empty(0, dtype="uint32")
        # self.logger.warn("FIFO Size before pulse: %d", self[self.which_fifo].FIFO_SIZE)
        if not no_inject:
            self.logger.debug("injecting: ")
            self["inj"].start()
        else:
            self.logger.debug("WTF??")
        i = 0
        raw = np.empty(0, dtype="uint32")
        time.sleep(0.01)
        while self["inj"].is_done() != 1:
            time.sleep(0.01)
            raw = np.append(raw, self[self.which_fifo].get_data())
            i = i + 1
            if i > 10:
                break
        time.sleep(wait)
        # self.logger.warn("FIFO Size after pulse: %d ", self[self.which_fifo].FIFO_SIZE)
        raw = np.append(raw, self[self.which_fifo].get_data())
        
        if i > 10000:
            self.logger.info("get_data: error timeout len={0:d}".format(len(raw)))
        lost_cnt = self["data_rx"]["LOST_DATA_COUNTER"]
        if self["data_rx"]["LOST_DATA_COUNTER"] != 0:
            self.logger.warn("get_data: error cnt={0:d}".format(lost_cnt))
        return raw

    def set_inj_high(self, inj_high):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        print("here_high", inj_high)
        if isinstance(self.inj_device, str) and self.inj_device == "GPAC_MightyPix":
            self["INJ_HI"].set_voltage(inj_high, unit="V")
        else:
            self.inj_device.set_voltage(inj_high, high=True)
            time.sleep(0.01)
        self.SET_VALUE["INJ_HI"] = inj_high

    def set_inj_low(self, inj_low):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        print("here_low", inj_low)
        if isinstance(self.inj_device, str) and self.inj_device == "GPAC_MightyPix":
            self["INJ_LO"].set_voltage(inj_low, unit="V")
        else:
            self.inj_device.set_voltage(inj_low, high=False)
            time.sleep(0.01)
        self.SET_VALUE["INJ_LO"] = inj_low

    def set_inj_all(self, inj_high=0.9, inj_low=0.7, inj_n=1, inj_width=500000, delay=700):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        self.set_inj_high(inj_high)
        self.set_inj_low(inj_low)
        self["inj"].reset()
        self["inj"]["REPEAT"] = inj_n
        self["inj"]["DELAY"] = delay
        self["inj"]["WIDTH"] = inj_width
        self["inj"]["EN"] = 1
        self.logger.info("inj:%.4f,%.4f inj_width:%d inj_n:%d delay:%d" % (inj_high, inj_low, inj_width, self["inj"]["REPEAT"], delay))

    def start_inj(self, inj_high=None, wait=False):
        """Start the injection."""
        if inj_high is not None:
            self.set_inj_high(inj_high)
        self["inj"].start()

        self.logger.info(
            "start_inj:%.4f,%.4f"
            % (
                self.SET_VALUE["INJ_HI"],
                self.SET_VALUE["INJ_LO"],
                # self["inj"].get_phase(),
            )
        )
        while self["inj"].is_done() != 1 and wait is True:
            time.sleep(0.0001)

    def get_rx_status(self):
        """Report the error status counters."""
        return_counters = {}

        for prop in [
            "LOST_DATA_COUNTER",
            "DECODER_ERROR_COUNTER",
            "FIFO_SIZE",
            "ENABLE",
            "REORDER",
            "READY",
            "LOAD_COLUMN2_COUNTER",
            "LOAD_PIXEL2_COUNTER",
        ]:
            return_counters[prop] = getattr(self["data_rx"], prop)
        if self.isMIO3:
            return_counters["FIFO"] = self["fifo"].get_FIFO_SIZE()
        else:
            return_counters["FIFO"] = self["FIFO"].get_FIFO_INT_SIZE()
        return return_counters

    def dac_status(self):
        """Return the current DAC stettings"""
        current_dac_settings = {}
        for dac in self["CONF_SR"]._fields:
            current_dac_settings[dac] = bitarray.util.ba2int(self["CONF_SR"][dac])
            # print("test", current_dac_settings[dac])
        return current_dac_settings

    # Above function only returns the "CONF_SR" settings, we need the "CONF" settings as well
    def dac_status_CONF(self):
        """Returns the current DAC CONF settings"""
        current_dac_CONF_settings = {}
        for dac in self["CONF"]._fields:
            current_dac_CONF_settings[dac] = bitarray.util.ba2int(self["CONF"][dac])
        return current_dac_CONF_settings

    def get_configuration(self):
        conf = {
            "mask": self.mask,
            "injection": self.injection,
            "trim": self.trim,
            "dacs": np.asarray(list(self.dac_status().values()), dtype=np.dtype("U")),
        }
        # this is the same as in the daq settings but maybe a bit more readable
        for i_conf, value in self.PIXEL_CONF.items():
            conf[i_conf] = np.asarray([i.tobytes() for i in value])
        return conf

    def get_firmware_configuration(self):
        # return_dict = {}
        # for dac in self["CONF"]._fields:
        #     return_dict[dac] = bitarray.util.ba2int(self["CONF"][dac])
        return super().get_configuration()

    def reset_read(self, wait=0.0001):
        """
        Reset the read-out of the chip.

        Parameters
        ----------
        wait: float
            Period of time (in seconds) that the function waits before changing back the state of the reset.
        """
        self["CONF"]["RESET"] = 1

        self["CONF"].write()

        time.sleep(wait)
        self["CONF"]["RESET"] = 0
        self["CONF"].write()

    def set_read(self):
        """
        Enable the read-out of the chip.

        Parameters
        ----------
        """
        # Reset read-out module of the.
        self["data_rx"].reset()
        # Reset read-out.
        self.reset_read(wait=0.001)
        # Set initial threshold values of all global thresholds.
        # Reset FIFO
        # self["FIFO"]["RESET"]  # If 'FIFO' type: sitcp_FIFO
        if self.isMIO3:
            self["fifo"]["RESET"]  # mio3
        else:
            self["FIFO"].reset()  # If 'FIFO' type: sram_fifo
        # Read-out trash data from the chip
        self["data_rx"].set_en(True)
        fifo_size = 0
        if self.isMIO3:
            fifo_size = self["fifo"].get_FIFO_SIZE()
        else:
            fifo_size = self["FIFO"].get_FIFO_SIZE()
        self.logger.debug(
            "Setting Mightypix Read-out: reset_fifo={}".format(
                fifo_size,
            )
        )
        # Reset FIFO
        # self["fifo"]["RESET"]  # If 'fifo' type: sitcp_fifo
        # self["FIFO"].reset()  # If 'fifo' type: sram_fifo
        if self.isMIO3:
            self["fifo"]["RESET"]  # mio3
        else:
            self["FIFO"].reset()  # If 'FIFO' type: sram_fifo

    def stop_read(self):
        """
        Stop the read-out of the chip.

        Returns
        ----------
        lost_cnt: int
            Number of hits not read-out when the read-out was stopped.
        """
        self["data_rx"].set_en(False)
        lost_cnt = self["data_rx"]["LOST_DATA_COUNTER"]
        if lost_cnt != 0:
            self.logger.warn("stop_read: error cnt={0:d}".format(lost_cnt))
        return lost_cnt

    def set_th(self, th_id=None, th_value=None):
        """
        Set a global threshold.

        Parameters
        ----------
        th_id: int or list of int
            Single integer or list of integers corresponding to valid Threshold IDs (i.e. 1, 2 or 3).
        th_value: float or list of floats
            Single float or list of floats corresponding to valid Threshold values (in Volts).
        """
        if isinstance(th_id, int):
            if th_id > 0 and th_id < 4:
                th_string = "TH" + str(th_id)
                th_dict = {th_string: th_value}
                self.set_global_voltage(**th_dict)
            else:
                self.logger.info("*{0}* is not a valid Threshold ID. (Only 1, 2 or 3 are valid)".format(th_id))
        elif isinstance(th_id, (list, tuple, np.ndarray)) and len(th_id) > 0:
            if len(th_id) == len(th_value):
                for th_pos, th_iter in enumerate(th_id):
                    if isinstance(th_iter, int) and th_iter > 0 and th_iter < 4:
                        th_string = "TH" + str(th_iter)
                        th_dict = {th_string: th_value[th_pos]}
                        self.set_global_voltage(**th_dict)
                    else:
                        self.logger.info("*{0}* is not a valid Threshold ID. (Only 1, 2 or 3 are valid)".format(th_iter))
            else:
                self.logger.info("The number of threshold values does not match the number of threshold IDs.")
        else:
            self.logger.info("The input was incorrect. It must be either: 1. An integer TH ID and a value, or 2. A list of TH IDs and list of values.")

    def set_global_voltage(self, **kwarg):
        """
        Set a global voltage to a specific value (in V).

        Parameters
        ----------
        kwarg:
            Any name on the list of global voltages as parameter name, followed by the value expected to be set as parameter value (in V).
            e.g. self.set_global_voltage(TH1=1.5)
        """
        for k in kwarg.keys():
            print(k)
            if k == "setting":
                if "value" not in kwarg:
                    raise ValueError("set_global_voltage needs a value if 'setting' is used!")

                # removed for simulation only put it back!!!!!!!!!!!!!!!!!
                self[kwarg[k]].set_voltage(kwarg["value"], unit="V")
                self.SET_VALUE[kwarg[k]] = kwarg["value"]
                self.logger.info(f"Setting {kwarg[k]} to {kwarg['value']}")
                break
            self.logger.info(f"Setting {k} to {kwarg[k]}")
            # removed for simulation only put it back!!!!!!!!!!!!!!!!!
            self[k].set_voltage(kwarg[k], unit="V")
            self.SET_VALUE[k] = kwarg[k]

    def set_global_current(self, **kwarg):
        """
        Set a global current to a specific value (in uA).

        Parameters
        ----------
        kwarg:
            Any name on the list of global currents as parameter name, followed by the value expected to be set as parameter value (in uA).
            e.g. self.set_global_current(Iref=-8)
        """
        # TODO
        pass

    def show(self, config_id="all", show_plot=False):
        """
        Log general information about the current status of the chip.

        Parameters
        ----------
        pix_mask_id: string
            - "power": Logs Power status
            - "dac": Logs DAC status
            Things that need to be added:
            - "Trim": Logs TDAC mask.
            - "EnHB": Logs Hit Buffer mask.
            - "EnInj": Logs Injection mask.
            - "EnAmpout": Logs Ampout mask.
        """
        if config_id == "all" or config_id == "power":
            ret = self.power_status()
            self.logger.info("Power" + ": " + str(ret))
        if config_id == "all" or config_id == "dac":
            ret = self.dac_status()
            self.logger.info("DACs" + ": " + str(ret))
        # if config_id=="all" or config_id=="Trim":
        #     pix_conf_list = np.array(self.PIXEL_CONF["Trim"],int)
        #     self.logger.info("Trim"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnPre":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnPre"],int)
        #     self.logger.info("EnPre"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnInj":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnInj"],int)
        #     self.logger.info("EnInj"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnMonitor":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnMonitor"],int)
        #     self.logger.info("EnMonitor"+": "+str(pix_conf_list.tolist()))

    def convert_physical_to_dig_pixel(self, col, row):
        """The conversion is mainly for the address that is returned, if you want to have physical pixel."""
        if row < 31:
            dig_row = row * 2
        else:
            dig_row = (row - 31) * 4 + 64
        self.logger.info(f"input {col},{row}")
        self.logger.info(f"output {col},{dig_row}")
        return (col, dig_row)

    def read_config_file(self, filename, config_type):
        """
        Read pixel configuratrion from a file.

        There are two types of files supported:
        .h5 file, where the pixel info has to be in the root directory by the name config_type
        .conf file, where the pixel info is saved as example_mask.conf

        The config type should be one of the matrices: mask,injection,trim.
        """
        # FIXME add more comments here
        if "h5" == filename.split()[-1]:
            with tb.open_file(filename, "r") as in_file:
                try:
                    setattr(self, config_type, getattr(in_file.root, config_type)[:])
                except:
                    self.logger.error(f"Could not get {config_type} from file {filename}.")
        elif "conf" == filename.split()[-1]:
            rows_inv = []
            try:
                with open(filename) as in_file:
                    for line in in_file:
                        if line[0] == "#":
                            continue
                        row_inv.append([int(i) for i in line])
                row_col_array = np.asarray(row_inv[::-1])
                setattr(self, config_type, row_col_array.T())
            except Exception as e:
                self.logger.error(f"Could not get {config_type} from file {filename}. \n {str(e)}")
        else:
            self.logger.error(f"Could not get {config_type} from file {filename}.")

    def close(self):
        """Make sure the power is out."""
        self.power_down()


if __name__ == "__main__":
    """This is just a simple main to test that the chip is connected and nothing huge is wrong."""
    # Create the Pixel object
    chip = MightyPix()
    chip.init()
    input()
    chip.reset_read()
    raw = chip.get_data()
    print(raw)
    input("here?")
    chip.dut["intf"].close()
