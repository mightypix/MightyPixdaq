#!/usr/bin/env python

from mightypix_daq.mightypix import MightyPix
import time


start_t = time.process_time()
start_tt = time.time()
chip = MightyPix(conf="mightypix_mio3.yaml")
chip.init()
chip.power_down()
chip.logger.info(chip.power_status())
chip.close()
