#!/usr/bin/env python

from mightypix_daq.mightypix_adapter import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

import mightypix_daq.analysis.interpreter as interpreter




start_t = time.process_time()
start_tt = time.time()
chip = MightyPix(conf="mightypix_adapter.yaml")
chip.init()
chip["CONF"]["DISABLE_CLK"] = 0 #if 0, than enable_clk in firmware ==1 
chip["CONF"].write()


chip.power_down()
chip.close()

