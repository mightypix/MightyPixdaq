# from mightypix_daq.mightypix_new_pixel import MightyPix
from mightypix_daq import mightypix_new_pixel
import numpy as np
import mightypix_daq.scan_base_new as scan_base
import pandas as pd
import yaml
import os, sys, time
import tables as tb
from mightypix_daq.analysis import plotting
import time
import bitarray
import mightypix_daq.analysis.interpreter_Klaas as interpreter

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

local_configuration = {
    "exp_time": 1.0,
    "injlist_param": [0.9,1.9,0.2], # in this scan: thr2 values
    "inj_n": 1000,   # Number of injection pulses per pixel and step
    "th1": 1.01,  # value for threshold 1 in V
    "t3": 1.01,  # value for threshold 1 in V
    "mask": [0,0], 
    "pix": [3,10], 
}

def reset_chip(chip):
    # chip.set_en_inj(cols=[1],rows=[ 0])
    # chip.set_en_ampout(1)
    chip.set_conf()

def is_pysical_cell(col, row):
    row_cond = False
    if row <= 60:
        row_cond = row % 2 == 0
    else:
        row_cond = (row - 60) % 4 == 0
    col_cond = (col > 1) and col < 30
    return row_cond and col_cond



def find_phys_cell(col, row):
    if not is_pysical_cell(col,row):
        col_new = col+1
        row_new = row+1 
        return col_new, row_new
    else:
        return col, row

class ScanNoise_opt(scan_base.ScanBase):
    scan_id = "noise_opt"
    
    def __init__(self, chip):
        super().__init__(chip)
        self.dac = None  # Initialize dac as None
        self.value = None  # Initialize value as None

    def scan(self, **kwargs):
        """
        description
        """

        ####################=
        # Load kwargs or default values.
        exp_time = kwargs.pop("exp_time", 1.0)
        n_inj = kwargs.pop("n_inj", 1)
        inj_high = kwargs.pop("inj_high", 1.25)
        inj_n =  kwargs.pop("inj_n",100)  # Number of injection pulses per pixel and step
        inj_low = kwargs.pop("inj_low", 0.5)
        # pix = kwargs.pop("pix", [20,10])
        scan_param_id = 0
        mask = self.chip._create_mask([0,0])
        pix = [20,10]

        ####################
        # enable readout
        self.chip.set_global_voltage(TH1=1.4)
        self.chip.set_global_voltage(TH2=1.15)
        self.chip.set_global_voltage(TH3=1.05)
        # self.chip.set_inj_all(inj_high=0.2, inj_low=0.1)
        # self.chip["data_rx"].ENABLE = 1
        # mask = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        # mask[:,:]=True
        # tdac= np.ones([self.chip.COL_SIZE, self.chip.ROW_SIZE-self.chip.lowest_tdac_row],dtype=np.uint8)
        # tdac[:,:]=tdac[:,:]*7
        # self.chip._write_conf(en_comp=mask,tdac=tdac)
        self.chip["CONF"].write()
        result_dict = {}
        self.chip.set_read()
        self.plot_flag = 1
        self.chip.stop_read()  
        self.chip.set_ts_inj(True)
        ts_n = self.chip['inj']['REPEAT']  
        init_dict = self.chip["CONF_SR"]._init 
        col , row = pix[0], pix[1]
        self.chip.set_en_comp(pix = [col,row])
        self.chip.set_en_ampout(col)
        self.chip.set_en_hitbus(col)
        self.chip.set_en_inj(col = col, row=row)
        # for dac in init_dict:
        dacs_to_loop = ["VNFoll", "VN"]
        
        for dac in dacs_to_loop:
        # for dac in chip["CONF_SR"]._fields:
            if dac == 'PixelConf':
                continue
            dac_str = str(dac)
            default_value = init_dict[dac_str]
            # default_value = bitarray.util.ba2int(chip["CONF_SR"][dac])
            print(f'default_value: {default_value}')
            self.chip.logger.info(f"Testing dac {dac}")
            self.chip.logger.info(self.chip.power_status()["Power [mW]"])
            # print(f'len(chip["CONF_SR"][dac]): {len(chip["CONF_SR"][dac])}')
            # start_value = max(default_value - 5, 0)
            start_value = max(default_value - len(self.chip["CONF_SR"][dac]), 0)
            # stop_value = default_value + 1 + len(chip["CONF_SR"][dac])
            bits = len(self.chip["CONF_SR"][dac])
            stop_value = min(default_value + 1 + bits, 2 ** bits - 1)
            # stop_value = default_value + 6
            for value in range(start_value, stop_value, 1):
                self.chip.set_inj_all(inj_high=inj_high, inj_low=inj_low ,  inj_n=inj_n, inj_width=1, delay=90)
                # self.chip["CONF_SR"][dac] = value
                print('-------------------------')
                self.chip["CONF_SR"]._init['VNFoll'] = 0 ######################################################## does not work?? ampout is still showing on scope
                self.chip["CONF_SR"].write()
                time.sleep(5)
                print('-------------------------')
                self.chip.set_read()
                time.sleep(5)
                with self.readout(scan_param_id=scan_param_id,  # TODO open/close self.readout for each scan step (make it faster)
                        fill_buffer=True, clear_buffer=True,
                        reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                        readout_interval=0.00001):
                        self.chip.start_inj()
                        dqdata = self.fifo_readout.data
                self.chip.stop_read()
                self.chip["CONF_SR"]._init[dac_str] = value
                # init_dict[dac_str] = value
                print('___________________________________________________________')
                print(self.chip["CONF_SR"]._init[dac_str])
                self.dac = dac
                time.sleep(2)
                self.value = value
                # chip.write_current_config()
                # chip.write_global_conf()
                # print(f"RX Enable:  {chip['data_rx'].READY}")
                self.chip["fifo"].RESET = 1
                self.chip["fifo"].RESET = 0
                self.chip["fifo"]["RESET"]
                # raw_data = chip.get_data()
                self.chip.logger.info(self.chip.get_rx_status())
                fast_interpreter = interpreter.Interpreter()
                #int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
                #print("data", int_data)
                
                print(f'dac, value: {dac}, {value}')
                # chip.stop_read()
                self.analyze()
                #self.plot(**local_configuration)
                


            self.chip.logger.info(f"lp2 counter after loop: {self.chip['data_rx'].LOAD_PIXEL2_COUNTER}")
            init_dict[dac_str] = default_value
            # self.chip["CONF_SR"].init[dac] = default_value
            print("___________________Check defaults")
            print(init_dict[dac_str])
            print("__________________________________________________-")
            reset_chip(self.chip)


    def plot(self,**local_configuration):
        #pix = kwargs.pop("pix", [25,5])
        #thr3 = kwargs.pop("t3", 1.01)
        if(self.plot_flag == 1):
            h5_filename = self.output_filename + ".h5"
            self.logger.info("Starting plotting...")
            with tb.open_file(h5_filename, "r") as h5_file:
                with plotting.Plotting(analyzed_data_file=h5_filename) as p:
                    #p.create_config_table()
                    #p.create_single_scurves(scan_parameter_name = "Threshold [V]", pix = pix, use_baseline_offset = thr3)
                    p.create_standard_plots(mask=None, dac=self.dac)
        else:
            print("No data to plot")

    # def plot(self, analyzed_data_file=None):
    #     print(f'dac, value: {self.dac}, {self.value}')
        
    #     if(self.plot_flag == 1):
    #         h5_filename = self.output_filename + ".h5"
    #         self.logger.info("Starting plotting...")
            
    #         with tb.open_file(h5_filename, "r") as h5_file:

    #             # Q: Maybe Plotting should not know about the file?
    #             # with plotting.Plotting(h5_filename, save_png=True, save_single_pdf=True, log=True) as p:
    #             with plotting.Plotting(analyzed_data_file=h5_filename) as p:
    #                 # p.create_config_table()
    #                 p.create_standard_plots(mask = None)
    #     else:
    #         print("No data to plot")


    # def analyze(self):
    #     # pass
    #     h5_filename = self.output_filename + ".h5"
        
    #     self.logger.info("Starting data analysis...")
    #     with tb.open_file(h5_filename, "r+") as h5_file:
    #         raw_data = h5_file.root.raw_data[:]
    #         print(f'raw_data: {raw_data}')
    #         meta_data = h5_file.root.meta_data[:]

    #         # TODO: TMP this should go to analysis function with chunking
    #         self.logger.info("Interpret raw data...")
    #         fast_interpreter = interpreter.Interpreter()
    #         int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
    #         print(f'len(int_data): {len(int_data)}')
    #         # print(int_data)
    #         if(len(int_data) == 0):
    #             self.plot_flag = 0

    #         if "interpreted" in h5_file.root:
    #             print("check")
    #             h5_file.remove_node(h5_file.root, "interpreted", recursive=True)

    #         h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
    #         hist_occ, _, _ = np.histogram2d(
    #             int_data["col"],
    #             int_data["row"],
    #             bins=(
    #                 np.arange(self.chip.COL_SIZE + 1),
    #                 np.arange(self.chip.ROW_SIZE + 1),
    #             ),
    #         )
    #         h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)

    #         h5_file.flush()
    #         h5_file.close()


    def analyze(self):
        # pass
        h5_filename = self.output_filename + ".h5"

        self.logger.info("Starting data analysis...")
        with tb.open_file(h5_filename, "r+") as h5_file:
            raw_data = h5_file.root.raw_data[:]
            meta_data = h5_file.root.meta_data[:]
            # print(f'raw_data_analyze: {raw_data[1:10]}')
            # TODO: TMP this should go to analysis function with chunking
            self.logger.info("Interpret raw data...")
            fast_interpreter = interpreter.Interpreter()
            int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
            # print(f'int_data: {int_data}')
            if(len(int_data) == 0):
                self.plot_flag = 0

            if "interpreted" in h5_file.root:
                # print("check")
                h5_file.remove_node(h5_file.root, "interpreted", recursive=True)

            h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
            hist_occ, _, _ = np.histogram2d(
                int_data["col"],
                int_data["row"],
                bins=(
                    np.arange(self.chip.COL_SIZE + 1),
                    np.arange(self.chip.ROW_SIZE + 1),
                ),
            )
            h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)
            h5_file.flush()

#chip = MightyPix(conf="mightypix_adapter.yaml")

# chip.setll(i_inj_anj_high=1.1, inj_low=0.8, inj_n=1, inj_width=10, delay=700)
# chip.set_global_voltage(TH1=1.4)
# chip.set_global_voltage(TH2=1.4)
# chip.set_global_voltage(TH3=1.05)

# reset_chip(chip)




# chip.logger.info(chip.power_status())

#chip.set_inj_all(inj_high=0.9, inj_low=0.7)

# chip.logger.info(chip.power_status()["Power [mW]"])
# reset_chip(chip)

# default_lp2_counter = chip["data_rx"].LOAD_PIXEL2_COUNTER


# chip.set_en_inj(20, 20)
# chip.set_en_ampout(20)
#chip.write_global_conf()

# chip.set_read()
# raw = chip.get_data()
# fast_interpreter = interpreter.Interpreter()
# int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
# print("data", int_data)



# chip.logger.info(f"lp2 counter after loop: {chip['data_rx'].LOAD_PIXEL2_COUNTER}")
# reset_chip(chip)


# start = 0
# #daq = ["IPFoll", "INFoll2", "VNFoll", "IPRegCasc"]


# chip.close()







if __name__ == "__main__":

    chip = mightypix_new_pixel.MightyPix(conf="mightypix_adapter_MUDAQ_new_V5.yaml")
    # chip = mightypix_new_pixel.MightyPix(conf="mightypix_new_pixel.yaml")
    chip.init()
    reset_chip(chip)
    scan = ScanNoise_opt(chip)
    scan.start(**local_configuration)