from mightypix_daq import mightypix_new_pixel
import numpy as np
import mightypix_daq.scan_base_new as scan_base
import pandas as pd
import yaml
import os, sys, time
import tables as tb
from mightypix_daq.analysis import plotting
import time
import bitarray
import mightypix_daq.analysis.interpreter_Klaas as interpreter
import mightypix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

local_configuration = {
    "exp_time": 1.0,
    "inj_n": 1000,   # Number of injection pulses per pixel and step
    "th1": 1.01,  # value for threshold 1 in V
    "t3": 1.01,  # value for threshold 1 in V
    "mask": [0,0], 
    "pix": [3,10], 
}

def reset_chip(chip):
    # chip.set_en_inj(cols=[1],rows=[ 0])
    # chip.set_en_ampout(1)
    chip.set_conf()

def is_pysical_cell(col, row):
    row_cond = False
    if row <= 60:
        row_cond = row % 2 == 0
    else:
        row_cond = (row - 60) % 4 == 0
    col_cond = (col > 1) and col < 30
    return row_cond and col_cond



def find_phys_cell(col, row):
    if not is_pysical_cell(col,row):
        col_new = col+1
        row_new = row+1 
        return col_new, row_new
    else:
        return col, row

class ScanNoise_opt(scan_base.ScanBase):
    scan_id = "noise_opt"
    
    def __init__(self, chip):
        super().__init__(chip)
        self.dac = None  # Initialize dac as None
        self.value = None  # Initialize value as None

    def scan(self, **kwargs):
        """
        description
        """

        ####################=
        # Load kwargs or default values.
        exp_time = kwargs.pop("exp_time", 1.0)
        n_inj = kwargs.pop("n_inj", 1)
        inj_high = kwargs.pop("inj_high", 0.9)
        inj_n =  kwargs.pop("inj_n",100)  # Number of injection pulses per pixel and step
        inj_low = kwargs.pop("inj_low", 0.31)
        # pix = kwargs.pop("pix", [20,10])
        scan_param_id = 0
        mask = self.chip._create_mask([0,0])
        pix = [20,10]

        ####################
        # enable readout
        self.chip.set_global_voltage(TH1=0.625) # for mudaq this is VCascade2
        self.chip.set_global_voltage(TH2=1.01)
        self.chip.set_global_voltage(TH3=1.01)
        
        self.chip.set_ts_inj(True)
        ts_n = self.chip['inj']['REPEAT']  
        init_dict = self.chip["CONF_SR"]._init 
        col , row = pix[0], pix[1]
        #self.chip.set_en_comp(pix = [col,row])
        self.chip.set_en_ampout(col)
        self.chip.set_en_hitbus(col)
        self.chip.set_en_inj(col = col, row=row)
        tdac = np.copy(self.chip.PixelConf['tdac0'])
        # for dac in init_dict:
        dacs_to_loop = ["VNFoll", "VN"]
        for dac in dacs_to_loop:
            if dac == 'PixelConf':
                continue
            dac_str = str(dac)
            default_value = init_dict[dac_str]
            print(f'default_value: {default_value}')
            self.chip.logger.info(f"Testing dac {dac}")
            # self.chip.logger.info(self.chip.power_status()["Power [mW]"])
            start_value = max(default_value - len(self.chip["CONF_SR"][dac]), 0)
            bits = len(self.chip["CONF_SR"][dac])
            stop_value = min(default_value + 1 + bits, 2 ** bits - 1)
            thr_list=list(np.arange(start_value,stop_value,1))
            for scan_param_id, value in enumerate(thr_list):
                self.chip["CONF_SR"]._init[dac_str] = int(value)
                print(f'value: {value}')
                self.chip._write_conf(en_comp=mask,tdac=tdac) 
                self.chip.set_conf()
                self.chip.set_inj_all(inj_high=inj_high, inj_low=inj_low ,  inj_n=inj_n, inj_width=10, delay=90)
                self.chip.set_read()
                
                with self.readout(scan_param_id=scan_param_id,  # TODO open/close self.readout for each scan step (make it faster)
                        fill_buffer=True, clear_buffer=True,
                        reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                        readout_interval=0.00001):
                        self.chip.start_inj()
                        dqdata = self.fifo_readout.data
                self.chip.stop_read()
                
                self.dac = dac
                self.value = value
                self.chip["fifo"].RESET = 1
                self.chip["fifo"].RESET = 0
                self.chip["fifo"]["RESET"]
                self.chip.logger.info(self.chip.get_rx_status())
                fast_interpreter = interpreter.Interpreter()
                self.plot_flag = 1 
                # self.analyze()
                # self.plot(**local_configuration)


            self.chip.logger.info(f"lp2 counter after loop: {self.chip['data_rx'].LOAD_PIXEL2_COUNTER}")
            self.chip["CONF_SR"]._init[dac_str] = default_value
            #self.chip._write_conf(en_comp=mask,tdac=tdac) 
            self.chip.set_conf()
            
            
            reset_chip(self.chip)


    # def plot(self,**local_configuration):
    #     #pix = kwargs.pop("pix", [25,5])
    #     #thr3 = kwargs.pop("t3", 1.01)
    #     if(self.plot_flag == 1):
    #         h5_filename = self.output_filename + ".h5"
    #         self.logger.info("Starting plotting...")
    #         with tb.open_file(h5_filename, "r") as h5_file:
    #             with plotting.Plotting(analyzed_data_file=h5_filename) as p:
    #                 #p.create_config_table()
    #                 #p.create_single_scurves(scan_parameter_name = "Threshold [V]", pix = pix, use_baseline_offset = thr3)
    #                 p.create_standard_plots(mask=None, dac=self.dac)
    #     else:
    #         print("No data to plot")





    # def plot(self, analyzed_data_file=None):
    #     print(f'dac, value: {self.dac}, {self.value}')
        
    #     if(self.plot_flag == 1):
    #         h5_filename = self.output_filename + ".h5"
    #         self.logger.info("Starting plotting...")
            
    #         with tb.open_file(h5_filename, "r") as h5_file:

    #             # Q: Maybe Plotting should not know about the file?
    #             # with plotting.Plotting(h5_filename, save_png=True, save_single_pdf=True, log=True) as p:
    #             with plotting.Plotting(analyzed_data_file=h5_filename) as p:
    #                 # p.create_config_table()
    #                 p.create_standard_plots(mask = None)
    #     else:
    #         print("No data to plot")


    # def analyze(self):
    #     # pass
    #     h5_filename = self.output_filename + ".h5"
        
    #     self.logger.info("Starting data analysis...")
    #     with tb.open_file(h5_filename, "r+") as h5_file:
    #         raw_data = h5_file.root.raw_data[:]
    #         print(f'raw_data: {raw_data}')
    #         meta_data = h5_file.root.meta_data[:]

    #         # TODO: TMP this should go to analysis function with chunking
    #         self.logger.info("Interpret raw data...")
    #         fast_interpreter = interpreter.Interpreter()
    #         int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
    #         print(f'len(int_data): {len(int_data)}')
    #         # print(int_data)
    #         if(len(int_data) == 0):
    #             self.plot_flag = 0

    #         if "interpreted" in h5_file.root:
    #             print("check")
    #             h5_file.remove_node(h5_file.root, "interpreted", recursive=True)

    #         h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
    #         hist_occ, _, _ = np.histogram2d(
    #             int_data["col"],
    #             int_data["row"],
    #             bins=(
    #                 np.arange(self.chip.COL_SIZE + 1),
    #                 np.arange(self.chip.ROW_SIZE + 1),
    #             ),
    #         )
    #         h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)

    #         h5_file.flush()
    #         h5_file.close()


    # def analyze(self):
    #     # pass
    #     h5_filename = self.output_filename + ".h5"

    #     self.logger.info("Starting data analysis...")
    #     with tb.open_file(h5_filename, "r+") as h5_file:
    #         raw_data = h5_file.root.raw_data[:]
    #         meta_data = h5_file.root.meta_data[:]
    #         # print(f'raw_data_analyze: {raw_data[1:10]}')
    #         # TODO: TMP this should go to analysis function with chunking
    #         self.logger.info("Interpret raw data...")
    #         fast_interpreter = interpreter.Interpreter()
    #         int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
    #         # print(f'int_data: {int_data}')
    #         if(len(int_data) == 0):
    #             self.plot_flag = 0

    #         if "interpreted" in h5_file.root:
    #             # print("check")
    #             h5_file.remove_node(h5_file.root, "interpreted", recursive=True)

    #         h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
    #         hist_occ, _, _ = np.histogram2d(
    #             int_data["col"],
    #             int_data["row"],
    #             bins=(
    #                 np.arange(self.chip.COL_SIZE + 1),
    #                 np.arange(self.chip.ROW_SIZE + 1),
    #             ),
    #         )
    #         h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)
    #         h5_file.flush()


    def analyze(self):
        
        h5_filename = self.output_filename + ".h5"
        print(h5_filename)
        print("_________________-")

        self.logger.info("Starting data analysis...")
        with tb.open_file(h5_filename, "r+") as h5_file:
            raw_data = h5_file.root.raw_data[:]
            meta_data = h5_file.root.meta_data[:]
            # run_config = h5_file.root.pixel_conf[:]

            self.logger.info("Interpret raw data...")
            fast_interpreter = interpreter.Interpreter()
            int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=meta_data)
            param_range = np.unique(meta_data['scan_param_id'])
            
            print(f'int_data: {int_data}')
            if(len(int_data) == 0):
                self.plot_flag = 0
            print(f'plot_flag: {self.plot_flag}')
            if "interpreted" in h5_file.root:
                h5_file.remove_node(h5_file.root, "interpreted", recursive=True)
            h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
            hist_occ, _, _ = np.histogram2d(
                int_data["col"],
                int_data["row"],
                bins=(
                    np.arange(self.chip.COL_SIZE + 1),
                    np.arange(self.chip.ROW_SIZE + 1),
                ),
            )
            h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)

            hist_scurve = au.scurve_hist3d(int_data, param_range)
  
            h5_file.create_carray(h5_file.root.interpreted,
                                    name="HistSCurve",
                                    title="Scurve Data",
                                    obj=hist_scurve,
                                    filters=tb.Filters(complib='blosc',
                                                        complevel=5,
                                                        fletcher32=False)
                                    )
            
        
            # h5_file.create_carray(h5_file.root.interpreted, name='ThresholdMap', title='Threshold Map', obj=self.threshold_map,
            #                            filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            h5_file.flush()
            print("analyze", h5_filename)
            # h5_file.close()





    def plot(self,**kwargs): 

        thr3 = kwargs.pop("t3", 1.01)
        row_start = kwargs.pop("row_start", 0)
        row_end = kwargs.pop("row_end",30)
        row_step = kwargs.pop("row_step", 2)
        col_start = kwargs.pop("col_start", 0)
        col_end = kwargs.pop("col_end", 29)       
        col_step = kwargs.pop("col_step", 1)       

        print(self.output_filename + ".h5")
        if(self.plot_flag == 1):
            h5_filename = self.output_filename + ".h5"
            print("plot", h5_filename)
            self.logger.info("Starting plotting...")
            with tb.open_file(h5_filename, "r") as h5_file:
                with plotting.Plotting(analyzed_data_file=h5_filename) as p:
                    p.create_config_table()
                    p.create_standard_plots(mask=None)
                    for row in range(row_start,row_end,row_step):
                        for col in range(col_start, col_end, col_step):
                            if kwargs.pop("pix", ''):
                                pix =  kwargs.pop("pix")
                            else:
                                pix = [col, row]

                            p.create_single_scurves(scan_parameter_name = "TH2 [V]", pix = pix) #use_baseline_offset = thr3)

        else:
            print("No data to plot")



if __name__ == "__main__":

    chip = mightypix_new_pixel.MightyPix(conf="mightypix_adapter_MUDAQ_new_V5.yaml")
    # chip = mightypix_new_pixel.MightyPix(conf="mightypix_new_pixel.yaml")
    # chip.init()
    # reset_chip(chip)
    scan = ScanNoise_opt(chip)
    scan.start(**local_configuration)
    scan.analyze()
    scan.plot(**local_configuration)