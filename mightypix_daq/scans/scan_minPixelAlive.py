#!/bin/env python
import os, sys, time
import numpy as np
import bitarray
import tables as tb
import yaml

import pickle

from mightypix_daq import mightypix_adapter

import matplotlib.pyplot as plt

# from mightypix_daq import mk_fname

import mightypix_daq.scan_base as scan_base
import mightypix_daq.analysis.interpreter as interpreter
from mightypix_daq.analysis import plotting

# import mightypix_daq.analysis as analysis

import mightypix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
    This scan tries to find the lowest achievable global thresholds at certain initial TRIM settings for every enabled CSA. 
"""

local_configuration = {
    "exp_time": 1.0,
    "n_inj": 1,  # Number of injections
    "th1": 1.01,  # value for threshold 1 in V
}
mask=None


def is_pysical_cell(col, row):
    row_cond = False
    if row <= 60:
        row_cond = row % 2 == 0
    else:
        row_cond = (row - 60) % 4 == 0
    col_cond = (col > 1) and col < 30
    return row_cond and col_cond


class ScanPixelAlive(scan_base.ScanBase):
    scan_id = "scan_PixelAlive"
    

    def scan(self, **kwargs):
        """
        This scan just injects a signal and checks the return
        """

        # Load kwargs or default values.
        exp_time = kwargs.pop("exp_time", 1.0)
        n_inj = kwargs.pop("n_inj", 1)
        inj_high = kwargs.pop("inj_high", 1.05)
        inj_low = kwargs.pop("inj_low", 0.5)
        scan_param_id = 0
        mask = self.chip._create_mask([0,0])


        self.chip.set_global_voltage(TH1=1.4)
        self.chip.set_global_voltage(TH2=1.05)
        self.chip.set_global_voltage(TH3=1.05)
        self.chip.set_inj_all(inj_high=1., inj_low=0.5)
        self.chip["data_rx"].ENABLE = 1
        self.chip["data_rx"].REORDER = 1
        result_dict = {}
        self.chip.set_read()

        self.plot_flag = 1

        # TODO put this into a config!
        if mask is not None:
            self.chip.mask=mask

        # rfile_name = mightypix.mk_fname("alife.root")
        # tfile = ROOT.TFile(rfile_name, "RECREATE")
        # hists = []
        # hists.append(
        #     ROOT.TH2D(
        #         f"hits_all",
        #         f"hits_all;col;row",
        #         self.mightypix.COL_SIZE + 5,
        #         0,
        #         self.mightypix.COL_SIZE + 5,
        #         self.mightypix.ROW_SIZE + 5,
        #         0,
        #         self.mightypix.ROW_SIZE + 5,
        #     )
        # )
        self.chip.stop_read()
        for row in range(7,8):
            for col in range(11,12,2):
                # for row in range(10):
                #     for col in range(2):
                # hists.append(
                #     ROOT.TH2D(
                #         f"hits_{col}_{row}",
                #         f"hits_{col}_{row};col;row",
                #         self.mightypix.COL_SIZE + 5,
                #         0,
                #         self.mightypix.COL_SIZE + 5,
                #         self.mightypix.ROW_SIZE + 5,
                #         0,
                #         self.mightypix.ROW_SIZE + 5,
                #     )
                # )
                self.chip.enable_column()
                # self.chip.injection = self.chip._create_mask("none")
                # self.chip.injection = self.chip._create_mask([0,0])
                self.chip.enable_injection(col, row)
                # self.chip.enable_ampout(col)
                # how_config = None
                self.chip.logger.info(f"============= This is col, row: {col}, {row} =======================")
                # print("before config",self.chip.injection)
                # self.chip.logger.info("RX status before configure:%s" % (self.chip.get_rx_status()))
                # self.chip.write_global_conf()
                self.chip.write_current_config()
                # self.chip.logger.info("RX status after configure:%s" % (self.chip.get_rx_status()))
                # for icol,_col in enumerate(self.chip.PIXEL_CONF["INJ_COL_EN"]):
                #     self.chip.logger.info(f"{icol} : {_col}",end=" ")
                # print()
                # print()
                # get_random_config(self.chip)
                # self.chip.write_current_config()
                time.sleep(0.1)
                self.chip.logger.info(self.chip.power_status())

                # data = self.chip.get_data(no_inject=True)
                # data = self.chip.get_data()

                # self.chip.logger.info("RX status:\n%s" % (self.chip.get_rx_status()))
                # for _ in range(1):
                #     self.chip["FIFO"].reset()
                #     time.sleep(0.002)
                # self.chip.logger.info("RX status:\n%s" % (self.chip.get_rx_status()))
                # print(self.chip["FIFO"]["FIFO_SIZE"])
                # Inject pixels in the mask with the current injection value.
                # with self.readout(scan_param_id=scan_param_id,fill_buffer=False,clear_buffer=True, readout_interval=0.001, timeout=0):
                self.chip.set_read()
                with self.readout(
                    scan_param_id=scan_param_id,
                    fill_buffer=False,
                    clear_buffer=True,
                    readout_interval=0.01,
                    timeout=0.2,
                ):
                    self.chip.start_inj()
                self.chip.stop_read()
                # raw = self.chip.get_data()
                # fast_interpreter = interpreter.Interpreter()
                # int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
                #     time.sleep(0.05)
                #     self.chip.logger.info("RX status in readout loop:\n%s" % (self.chip.get_rx_status()))
                #     # time.sleep(1)
                #     # pre_cnt=cnt
                # # Increase scan parameter ID counter.
                # scan_param_id = scan_param_id + 1
                # self.chip.logger.info("RX status after readoutloop:\n%s" % (self.chip.get_rx_status()))
                # if len(data)==0:
                #     self.chip.stop_read()
                #     time.sleep(0.5)
                #     self.chip.power_down()
                #     time.sleep(5)
                #     self.chip.power_up()
                #     time.sleep(5)
                #     self.chip._write_global_conf()
                #     time.sleep(0.5)
                #     self.chip.set_read()
                #     time.sleep(0.5)
                # print("\033[91m",data,"\033[0m")
                # #

                # show the data directly in the loop.
                # self.chip.set_read()
                # # time.sleep(0.1)
                # self.chip.logger.info("RX status before readout:\n%s" % (self.chip.get_rx_status()))
                # data = self.chip.get_data()
                # self.chip.logger.info("RX status after readout:\n%s" % (self.chip.get_rx_status()))
                # self.chip.stop_read()

                # # # Interpret data, count pixels with hits and identify if they have reached the limit occupancy.
                # fast_interpreter = interpreter.Interpreter()
                # int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=data, meta_data=None)
                # print(col, row)
                # for name in int_data.dtype.names:
                #     print(f"{name:>12}", end="")
                # print()
                # for ihit, hit in enumerate(int_data):
                #     hists[0].Fill(int_data["det_col"][ihit], int_data["det_row"][ihit])  # this is for all
                #     hists[-1].Fill(int_data["det_col"][ihit], int_data["det_row"][ihit])  # this is for this injection
                #     for name in int_data.dtype.names:
                #         value = int_data[name][ihit]
                #         if "raw" in name:
                #             print(f"{value:>12X}", end="")
                #         else:
                #             print(f"{value:>12}", end="")
                #     print()
                # # print(int_data.dtype.names)
                # # print(int_data)
                # # print(int_data.size)
                # # for ii,i in enumerate(debug_out):
                # #     print("%s, %X"%(i[0],i[1]))
                # #     # if ii>300:
                # #     #     break

                # int_cleardata = int_data[
                #     np.bitwise_and(int_data["cnt"] < 2, int_data["col"] < 257)
                # ]
                # plot2d = np.histogram2d(
                #     int_cleardata["col"],
                #     int_cleardata["row"],
                #     bins=[
                #         np.arange(0, self.chip.COL_SIZE + 1, 1),
                #         np.arange(0, self.chip.ROW_SIZE + 1, 1),
                #     ],
                # )[0]
                # result_dict[(col, row)] = plot2d
                # input("lala")
        # test_h2.Draw("colz")
        # input()
        # test_h2.SaveAs("test.pdf")
        # test_h2.SaveAs("test.png")
        # test_h2.SaveAs(local_configuration["output"])

        # fig = plt.figure()
        # for col, row in result_dict:
        #     print(col, row)
        #     im2 = plt.imshow(result_dict[(col, row)])
        #     plt.show()
        # c = ROOT.TCanvas()
        # hists[0].Draw("colz")
        # c.SaveAs(rfile_name.replace(".root", ".png"))
        # for h in hists:
        #     h.Write()
        # tfile.Close()
        self.chip.power_down()

    def analyze(self):
        # pass
        h5_filename = self.output_filename + ".h5"

        self.logger.info("Starting data analysis...")
        # hists = []
        # for ih in range(5):
        #     hists.append(
        #         ROOT.TH2D(
        #             f"hits{ih}",
        #             f"hits{ih};col;row",
        #             self.chip.COL_SIZE + 5,
        #             0,
        #             self.chip.COL_SIZE + 5,
        #             self.chip.ROW_SIZE + 5,
        #             0,
        #             self.chip.ROW_SIZE + 5,
        #         )
        #     )
        with tb.open_file(h5_filename, "r+") as h5_file:
            raw_data = h5_file.root.raw_data[:]
            # print(len(raw_data))  
            # for iraw in raw_data[:100]:
            #     print(hex(iraw))
            meta_data = h5_file.root.meta_data[:]
            # run_config = h5_file.root.configuration.run_config[:]

            # TODO: TMP this should go to analysis function with chunking
            # print('haeder1\t header2\t y\t x\t Hits\t Counter')
            self.logger.info("Interpret raw data...")
            fast_interpreter = interpreter.Interpreter()
            int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
            print("____________________________________")
            for int_d in int_data[:20]:
                print(int_d)
                print(int_d["det_col"])
            print("_________________________________________________________________________")
            print("interpreted data length", len(int_data))
            if(len(int_data) == 0):
                self.plot_flag = 0
            print('--------------------------------------------')
            print(len(int_data))
            print('--------------------------------------------')
            #occ = au.occ_hist2d(int_data)

            if "interpreted" in h5_file.root:
                print("check")
                h5_file.remove_node(h5_file.root, "interpreted", recursive=True)
            h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")

            # if int_data["det_col"] != 0:
            hist_occ, _, _ = np.histogram2d(
                int_data["det_col"],
                int_data["det_row"],
                bins=(
                    np.arange(self.chip.COL_SIZE + 1),
                    np.arange(self.chip.ROW_SIZE + 1),
                ),
            )
            # print(hist_occ)
            # print(int_data["det_col"])
            h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)
            h5_file.flush()

            # for name in int_data.dtype.names:
            #     print(f"{name:>12}", end="")
            # print()
            # for ihit, hit in enumerate(int_data):
            #     # hists[0].Fill(int_data["col"][ihit], int_data["row"][ihit])
            #     for name in int_data.dtype.names:
            #         value = int_data[name][ihit]
            #         if "raw" in name:
            #             print(f"{value:>12X}", end="")
            #         else:
            #             print(f"{value:>12}", end="")
            #     print()

        # tfile = ROOT.TFile("test_{}.root".format(time.strftime("%Y%m%d_%H%M%S")), "RECREATE")
        # for h in hists:
        #     h.Write()
        # tfile.Close()

    def plot(self, analyzed_data_file=None):
        if(self.plot_flag == 1):
            h5_filename = self.output_filename + ".h5"

            self.logger.info("Starting plotting...")
            mask_file = self.chip._create_mask([0,0])
            with tb.open_file(h5_filename, "r") as h5_file:

                # Q: Maybe Plotting should not know about the file?
                # with plotting.Plotting(h5_filename, save_png=True, save_single_pdf=True, log=True) as p:
                with plotting.Plotting(analyzed_data_file=h5_filename) as p:
                    p.create_config_table()
                    p.create_standard_plots(mask = mask_file)
        else:
            print("No data to plot")


if __name__ == "__main__":
    from mightypix_daq import mightypix_adapter
    import argparse

    parser = argparse.ArgumentParser(
        usage="python scan_source.py -t1 1.4 -t2 1.1 -t3 1.05 -f 0:44 -p -time 50",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("-conf", "--config_file", type=str, default=None)
    parser.add_argument("-t1", "--TH1", type=float, default=1.4)
    parser.add_argument("-t2", "--TH2", type=float, default=1.1)
    parser.add_argument("-t3", "--TH3", type=float, default=1.05)
    parser.add_argument("-f", "--flavor", type=str, default=None)
    parser.add_argument("-o", "--output", type=str, default=None)
    parser.add_argument("-a", "--analyze", type=str, default=None)
    parser.add_argument("-p", "--power_reset", action="store_const", const=1, default=0)  # Default = True: Skip power reset.
    parser.add_argument("-time", "--scan_time", type=int, default=None, help="Scan time in seconds.")
    parser.add_argument("-dout", "--output_dir", type=str, default=None)
    parser.add_argument("-m", "--mask", type=str, default=None)


    args = parser.parse_args()

    # m = mightypix.MightyPix(conf="mightypix_mio3.yaml",no_power_reset=not bool(args.power_reset))
    m = mightypix_adapter.MightyPix(conf="mightypix_adapter.yaml",no_power_reset=not bool(args.power_reset))
    # m = mightypix.MightyPix(conf="mightypix_right.yaml",no_power_reset=not bool(args.power_reset))
    if args.analyze is None:
        if not m.isInitialized:
            m.init()
        m.set_inj_en(pix="none")
    if args.config_file is not None:
        m.load_config(args.config_file)

    # if args.th1 is not None:
    #     m.set_th(1, args.th1)
    # if args.th2 is not None:
    #     m.set_th(2, args.th2)
    # if args.th3 is not None:
    #     m.set_th(3, args.th3)
    local_configuration|= vars(args)

    if args.flavor is not None:
        if args.flavor == "all":
            collist = range(0, m.COL_SIZE)
            m.logger.info("Enabled: Full matrix")
        else:
            tmp = args.flavor.split(":")
            collist = range(int(tmp[0]), int(tmp[1]))
            m.logger.info("Enabled: Columns {0:s} to {1:s}".format(tmp[0], tmp[1]))

        pix = []
        for i in collist:
            for j in range(0, m.ROW_SIZE):
                pix.append([i, j])
    else:
        pix = []

        # m.set_tdac(m.PIXEL_CONF["Trim"])

        # for i in range(0,m.COL_SIZE):
        #    for j in range(0,m.ROW_SIZE):
        #        if m.PIXEL_CONF["EnPre"][i,j]!=0:
        #            pix.append([i,j])
        #        else:
        #            pass
    if args.mask is not None:
        h5_mask_file= tb.open_file(args.mask, "r")
        mask=np.transpose(h5_mask_file.root.mask)

    if len(pix) > 0:
        local_configuration["pix"] = pix
    else:
        pass

    if args.scan_time is not None:
        local_configuration["scan_time"] = args.scan_time
    # fout=None
    # if args.output is not None:
    #     fout
    if args.output is not None:
        scan = ScanPixelAlive(m, fout=args.output, online_monitor_addr="tcp://127.0.0.1:6500")
    else:
        scan = ScanPixelAlive(m, online_monitor_addr="tcp://127.0.0.1:6500")

    if args.analyze is None:
        scan.start(**local_configuration)
    scan.analyze()
    scan.plot()