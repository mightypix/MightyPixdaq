#!/bin/env python
import os, sys, time
import numpy as np
import bitarray
import tables as tb
import yaml

import pickle

from mightypix_daq import mightypix_new_pixel

import matplotlib.pyplot as plt

# from mightypix_daq import mk_fname

import mightypix_daq.scan_base_new as scan_base
import mightypix_daq.analysis.interpreter_Klaas as interpreter
from mightypix_daq.analysis import plotting
import mightypix_daq.analysis.analysis_utils as au
import numpy as np


import mightypix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
    This scan tries to find the lowest achievable global thresholds at certain initial TRIM settings for every enabled CSA. 
"""

local_configuration = {
    "exp_time": 1.0,
    "injlist_param": [0.8,2.01,0.2], #in this scan: injHigh values
    "inj_n": 100,   # Number of injection pulses per pixel and step
    "th1": 1.01,  # value for threshold 1 in V
    "t2": 1.15,  # value for threshold 2 in V
    "mask": [0,0], 
      # "pix": [3,10], 
    "row_start": 8, 
    "row_end": 12, 
    "row_step": 2, 
    "col_start": 5, 
    "col_end": 10, 
    "col_step": 2,  
}


## ATTENTION: The matrix is always defined by [col,row] !!

def is_physical_cell(col, row):
    row_cond = False
    if row <= 60:
        row_cond = row % 2 == 0
    else:
        row_cond = (row - 60) % 4 == 0
    col_cond = (col > 1) and col < 30
    return row_cond and col_cond



def find_phys_cell(col, row):
    if not is_physical_cell(col,row):
        col_new = col+1
        row_new = row+1 
        return col_new, row_new
    else:
        return col, row


class ScanPixel(scan_base.ScanBase):
    scan_id = "threshold_scan"
    

    def scan(self, **kwargs):
        """
        This scan just injects a signal and checks the return
        """

        ####################=
        # Load kwargs or default values.
        exp_time = kwargs.pop("exp_time", 1.0)
        injlist_param =  kwargs.pop("injlist_param",[0.2,0.5,0.0]) 
        inj_n =  kwargs.pop("inj_n",100) 
        inj_low = kwargs.pop("inj_low", 0.52 )
        scan_param_id = 0
        thr2 = kwargs.pop("t2", 1.2)        # pix = kwargs.pop("pix", [25,5])
        row_start = kwargs.pop("row_start", 0)
        row_end = kwargs.pop("row_end",30)
        row_step = kwargs.pop("row_step", 6)
        col_start = kwargs.pop("col_start", 0)
        col_end = kwargs.pop("col_end", 29)       
        col_step = kwargs.pop("col_step", 1) 
        ####################
   
        ####################

        ####################
        # enable readout     
        mask = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        mask[:,:]=True  
        self.chip.set_global_voltage(TH1=1.4)
        self.chip.set_global_voltage(TH2=thr2)
        self.chip.set_global_voltage(TH3=1.05)
        self.chip["data_rx"].ENABLE = 1
        self.chip.set_ts_inj(True)
        # ts_n = self.chip['inj']['REPEAT']  
        # en_comp = np.copy(self.chip.PixelConf['en_comp0'])
        tdac = np.copy(self.chip.PixelConf['tdac0'])
        self.chip["CONF"].write()
        # self.chip.mask=mask
        self.chip._write_conf(en_comp=mask,tdac=tdac)
        self.plot_flag = 1
        ####################
        
        ####################
        # main scan loop

        # Create the list of values to be injected. If no values were given, it will inject to the default INJ_HI in chip.
        if injlist_param is None:
            injlist=[self.chip.SET_VALUE["INJ_HI"]]
        elif len(injlist_param)==3:
            injlist=list(np.arange(injlist_param[0],injlist_param[1],injlist_param[2]))
        else:
            injlist=[self.chip.SET_VALUE["INJ_HI"]]
            
        
        ####################
        # Define row & column. Be aware that not all rows are connected
        for row_i in range(row_start,row_end,row_step):
            for col_i in range(col_start, col_end, col_step):
                if kwargs.pop("pix", ''):
                    pix =  kwargs.pop("pix")
                    print("found pixel", pix)
                else:
                    print(f"using col {col_i} and row {row_i}")
                    col, row = find_phys_cell(col_i, row_i)

                self.chip.set_en_comp(pix = [col,row])
                self.chip.set_en_ampout([col])
                self.chip.set_en_hitbus([col])
                self.chip.set_en_inj(col = col, row=row)
                    
                ####################
                # enable readout 
                mask[col,row]=False

                time.sleep(0.1)
                self.chip.logger.info(self.chip.power_status())
                self.logger.info('Starting scan...')

                for scan_param_id, vcal in enumerate(injlist):
                    # print(inj_n)
                    self.chip.set_inj_all(inj_high=vcal, inj_low=inj_low ,  inj_n=inj_n, inj_width=1, delay=90)
                    self.chip.set_read()


                    with self.readout(scan_param_id=scan_param_id,  # TODO open/close self.readout for each scan step (make it faster)
                        fill_buffer=True, clear_buffer=True,
                        reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                        readout_interval=0.001):
                        self.chip.start_inj()
                        dqdata = self.fifo_readout.data


                    self.chip.stop_read()
            
     
        self.logger.info('Scan finished')

        # self.chip.power_down()


    def analyze(self):
        # pass
        h5_filename = self.output_filename + ".h5"

        self.logger.info("Starting data analysis...")
        with tb.open_file(h5_filename, "r+") as h5_file:
            raw_data = h5_file.root.raw_data[:]
            meta_data = h5_file.root.meta_data[:]
            # run_config = h5_file.root.pixel_conf[:]

            self.logger.info("Interpret raw data...")
            fast_interpreter = interpreter.Interpreter()
            int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=meta_data)
            # print(meta_data['scan_param_id'])
            # print(int_data["scan_param_id"])
            param_range = np.unique(meta_data['scan_param_id'])

            if(len(int_data) == 0):
                self.plot_flag = 0
            if "interpreted" in h5_file.root:
                h5_file.remove_node(h5_file.root, "interpreted", recursive=True)
            h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
            hist_occ, _, _ = np.histogram2d(
                int_data["col"],
                int_data["row"],
                bins=(
                    np.arange(self.chip.COL_SIZE + 1),
                    np.arange(self.chip.ROW_SIZE + 1),
                ),
            )
            h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)
            # print("para", param_range)
            hist_scurve = au.scurve_hist3d(int_data, param_range)
            h5_file.create_carray(h5_file.root.interpreted,
                                    name="HistSCurve",
                                    title="Scurve Data",
                                    obj=hist_scurve,
                                    filters=tb.Filters(complib='blosc',
                                                        complevel=5,
                                                        fletcher32=False))
            

            h5_file.flush()


    def plot(self,**kwargs):
        # pix = kwargs.pop("pix", [25,5])
        row_start = kwargs.pop("row_start", 0)
        row_end = kwargs.pop("row_end",30)
        row_step = kwargs.pop("row_step", 2)
        col_start = kwargs.pop("col_start", 0)
        col_end = kwargs.pop("col_end", 29)       
        col_step = kwargs.pop("col_step", 1)    



        if(self.plot_flag == 1):
                h5_filename = self.output_filename + ".h5"
                self.logger.info("Starting plotting...")
                with tb.open_file(h5_filename, "r") as h5_file:
                    with plotting.Plotting(analyzed_data_file=h5_filename) as p:
                        p.create_config_table()
                        p.create_standard_plots(mask=None)
                        for row in range(row_start,row_end,row_step):
                            for col in range(col_start, col_end, col_step):
                                if kwargs.pop("pix", ''):
                                    pix =  kwargs.pop("pix")
                                else:
                                    pix = [col, row]

                            p.create_single_scurves(scan_parameter_name = "Threshold [V]", pix = pix, use_baseline_offset = thr3)
                            p.create_scurves_plot()
  
        else:
            print("No data to plot")


if __name__ == "__main__":
    from mightypix_daq import mightypix_new_pixel
    import argparse

    parser = argparse.ArgumentParser(
        usage="python scan_scurve.py -t1 1.4 -t2 1.1 -t3 1.05 -f 0:44 -p -time 50",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("-conf", "--config_file", type=str, default=None)
    parser.add_argument("-t1", "--TH1", type=float, default=1.4)
    parser.add_argument("-t2", "--TH2", type=float, default=1.5)
    parser.add_argument("-t3", "--TH3", type=float, default=1.05)
    parser.add_argument("-f", "--flavor", type=str, default=None)
    parser.add_argument("-o", "--output", type=str, default=None)
    parser.add_argument("-a", "--analyze", type=str, default=None)
    parser.add_argument("-p", "--power_reset", action="store_const", const=1, default=0)  # Default = True: Skip power reset.
    parser.add_argument("-time", "--scan_time", type=int, default=None, help="Scan time in seconds.")
    parser.add_argument("-dout", "--output_dir", type=str, default=None)
    parser.add_argument("-m", "--mask", type=str, default=None)
    parser.add_argument("-row_start", "--Row_start", type=int, default=0)
    parser.add_argument("-row_end", "--Row_end", type=int, default=30)
    parser.add_argument("-row_step", "--Row_step", type=int, default=2)
    parser.add_argument("-col_start", "--Col_start", type=int, default=0)
    parser.add_argument("-col_end", "--Col_end", type=int, default=29)
    parser.add_argument("-col_step", "--Col_step", type=int, default=1)
    parser.add_argument("-pix", "--pixel", type=list, default=None)
    args = parser.parse_args()


    # load config
    m = mightypix_new_pixel.MightyPix(conf="mightypix_adapter_MUDAQ_new_V5.yaml",no_power_reset=not bool(args.power_reset))
    # m = mightypix_new_pixel.MightyPix(conf="mightypix_new_pixel.yaml",no_power_reset=not bool(args.power_reset))

    if args.analyze is None:
        if not m.isInitialized:
            m.init()
        m.set_inj_all()
    if args.config_file is not None:
        m.load_config(args.config_file)

    local_configuration|= vars(args)

    if args.flavor is not None:
        if args.flavor == "all":
            collist = range(0, m.COL_SIZE)
            m.logger.info("Enabled: Full matrix")
        else:
            tmp = args.flavor.split(":")
            collist = range(int(tmp[0]), int(tmp[1]))
            m.logger.info("Enabled: Columns {0:s} to {1:s}".format(tmp[0], tmp[1]))

        pix = []
        for i in collist:
            for j in range(0, m.ROW_SIZE):
                pix.append([i, j])
    else:
        pix = []

    if args.mask is not None:
        h5_mask_file= tb.open_file(args.mask, "r")
        mask=np.transpose(h5_mask_file.root.mask)

    if len(pix) > 0:
        local_configuration["pix"] = pix
    else:
        pass

    if args.scan_time is not None:
        local_configuration["scan_time"] = args.scan_time


    if args.output is not None:
        scan = ScanPixel(m, fout=args.output, online_monitor_addr="tcp://127.0.0.1:6500")
    else:
        scan = ScanPixel(m, online_monitor_addr="tcp://127.0.0.1:6500")
    
    if args.analyze is None:
        # scan.start(inj=np.arange(0.02, 0.2, 0.002)*10, cols=None, rows=None)
        scan.start(**local_configuration)

    scan.analyze()
    scan.plot(**local_configuration)