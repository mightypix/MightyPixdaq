#!/usr/bin/env python

from mightypix_daq.mightypix import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle
import streamlit as st
import scan_base
import pandas as pd
import yaml
import pickle
import mightypix_daq.analysis.interpreter as interpreter
st.set_page_config(layout="wide")

with open("scans/style.css") as f:
    st.markdown(f"<style>{f.read()}</style>", unsafe_allow_html=True)


def reset_chip(chip):
    chip.power_cycle()
    chip.enable_column()
    chip.write_global_conf()
    
    # # chip.set_th(th_id=1, th_value=1.05)
    # chip.set_inj_all(inj_high=0.5, inj_low=0.1)
    # chip.enable_column()
    # # chip.enable_injection(0, 0)
    # # chip.enable_injection(1, 0)
    # chip.enable_injection(20, 20)
    # chip.enable_ampout(20)
    # # chip.enable_injection(5, 0)
    # # chip.enable_injection(0, 1)
    # # chip.enable_injection(1, 1)
    # # chip.enable_injection(2, 1)
    # # chip.enable_injection(3, 1)
    # chip.write_global_conf()

class SystematicScan(scan_base.ScanBase):
    scan_id = "systematic_scan"
    
    def scan(self, **kwargs):
        '''
        changes_df: a dataframe of parameteres and their values
        injection: True readout done with or without injection
        '''
        self.epoche += 1
        scan_param_id = 0
        changes_df = kwargs.pop("changes_df", None)
        injection = kwargs.pop("injection", True)

        if isinstance(changes_df, pd.DataFrame):
            if len(changes_df)!=0:
                print("Non default configuration received for scanning")

                for i in range(len(changes_df)):
                    
                    key = changes_df.iloc[i,0]
                    val = changes_df.iloc[i,1]
                    #INJ_LO and INJ_HI parameters are returned from power_Status dict but can only be adjusted as dac parameters
                    
                    if ((key == "INJ_LO") | (key == "INJ_HI")):
                        self.chip[key].set_voltage(val, unit="V")
                        self.chip.SET_VALUE[key] = val

                    elif (key in self.chip.power_status_mini().keys()):
                        print(key,val,"global_voltage")
                        self.chip.set_global_voltage(setting=key, value = val) 
                    
                    elif (key in self.chip.dac_status().keys()):
                        print(key,val,"dac_SR")
                        self.chip["CONF_SR"][key] = int(val)
                    
                    elif (key in self.chip.dac_status_CONF().keys()):
                        print(key,val,"dac")
                        self.chip["CONF"][key] = int(val)
                    
                    else:
                        print("Error: key not found in list of adjustable parameters")
                    
                    time.sleep(0.1)
                
                print(self.chip.power_status())
            
        self.chip.logger.info(chip.power_status())
        #fast_interpreter = interpreter.Interpreter()
        self.chip.logger.info(chip.power_status()["Power [mW]"])
        
        #TODO:this changes INJ_LO and INJ_HIGH parameters to their default values. Change it later
        #reset_chip(self.chip)
        inj_high = self.chip.SET_VALUE["INJ_HI"]
        inj_low = self.chip.SET_VALUE["INJ_LO"]
        self.chip.set_inj_all(inj_high=inj_high, inj_low=inj_low)
        self.chip.enable_column()
        #default_lp2_counter = self.chip["data_rx"].LOAD_PIXEL2_COUNTER
        
        self.chip.enable_injection(20, 20)
        self.chip.enable_ampout(20)

        self.chip.write_global_conf()
        self.chip.set_read()
        
        #It first goes into the self.readout functiona and runs the code until "yield" and then does self.chip.start_inj(), after this it resumes the readout() function after yield
        with self.readout(
            scan_param_id=scan_param_id,
            fill_buffer=False,
            clear_buffer=True,
            readout_interval=0.01,
            timeout=0.2,
        ):
            if injection:
                self.chip.start_inj()
            else:
                pass
        self.chip.stop_read()
        
        # print('\n\n############################################################################')
        # print(f"{len(self.n_readouts)}")
        # print('############################################################################\n\n')
        reset_chip(chip)
        self.chip.power_cycle()
        #self.chip.set_global_voltage(setting=key, value = def_value)  

def edit_input(var):
    st.session_state[var+"input"] = st.session_state[var]
def edit_slider(var):
    st.session_state[var] = st.session_state[var+"input"]

def filter_df(main_df, df_var):
    main_df = DF_def_changes(main_df, df_var["value"])
    temp_df = main_df.drop(["type", "subtype", "min_value", "max_value"],axis=1)
    if len(temp_df.columns)>=4:
        temp_df_updated = temp_df.iloc[np.where(temp_df.iloc[:,2]!=temp_df.iloc[:,3])[0],[0,1,2,3]]
    else:
        temp_df_updated = temp_df.iloc[np.where(temp_df.iloc[:,2]!=temp_df.iloc[:,1])[0],[0,1,2]]
    return temp_df_updated


def button_scan(temp_df_updated, sys_scan, chip, injection = True):
    
    
    Run_num = temp_df_updated.iloc[:,[0,2]].columns[1]
    chip.logger.info("###################################################################")
    chip.logger.info(f"{Run_num}")
    if injection:
        chip.logger.info("Running with injection")
    else:
        chip.logger.info("Running without injection")
    chip.logger.info("Updated parameters")
    chip.logger.info("{0:^15}|{1:^10}|{2:^10}".format("Variable","Value","Old value"))
    for i in range(len(temp_df_updated)):
        var = temp_df_updated.iloc[i,0]
        val = float(temp_df_updated.iloc[i,2])
        if (len(temp_df_updated.columns)>=4):
            old_val = float(temp_df_updated.iloc[i,3])
        else:
            old_val = float(temp_df_updated.iloc[i,1])
        chip.logger.info("{0:<15}|{1:>10}|{2:>10}".format(var,val,old_val))
    
    chip.logger.info("###################################################################")   
    
    placeholder =st.empty()
    
    if(len(temp_df_updated)==0):
        with placeholder:
            st.write("Scanning with no change in default parameters")
        sys_scan.start(injection = injection)
        chip.power_cycle()
    else:
        with placeholder:
            st.write("Running the scan")    
        sys_scan.start(changes_df = temp_df_updated.iloc[:,[0,2]], injection = injection)
        chip.power_cycle()
    
    with placeholder:
        st.write("Scan finished")
            
    st.header("Updated parameters")
    placeholder2 = st.empty()
    with placeholder2:
        st.write("No changes yet")
    if(len(temp_df_updated)!=0):
        with placeholder2:
            st.write(temp_df_updated)  
    

@st.experimental_singleton
def get_class():
    chip = MightyPix(conf= "mightypix.yaml")
    print("Configuring for the first time")
    chip.init()
    sys_scan = SystematicScan(chip, online_monitor_addr="tcp://127.0.0.1:5500")
    return chip,sys_scan




@st.cache(allow_output_mutation=True)
def return_main_df(conf = "mightypix.yaml"):
    with open(conf, "r") as stream:
        try:
            content = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    
    all_params_df = pd.DataFrame(columns = ["type", "subtype", "name", "def_value", "min_value", "max_value"])
    for temp_dict in content["registers"]:
        type_key = temp_dict["type"]
        if "fields" in temp_dict.keys():
            sub_type = temp_dict["name"]
            for sub_dict in temp_dict["fields"]:
                name = sub_dict["name"]
                all_params_df.loc[len(all_params_df)] = [type_key, sub_type, name, 0, 0, int(2**sub_dict["size"])]
        else:
            sub_type = "NoSubtype"
            name = temp_dict["name"]
            all_params_df.loc[len(all_params_df)] = [type_key, sub_type, name, 0, 0, 2.5]
        #dropping the pixel column because it wont be adjusted
    all_params_df = all_params_df.drop(np.where(all_params_df["max_value"]>10000)[0][0])
    return all_params_df  

def set_defaults_df_var(df):
   for var in df["name"]:
        def_val = all_params_df.loc[all_params_df["name"] == var, "def_value"]
        st.session_state[var] = float(def_val)
        st.session_state[var+"input"] = st.session_state[var]


@st.cache(allow_output_mutation=True)
def DF_def_changes(df, run_column):
    #there are 6 columns in total in the main dataframe. Now any additional run values column would correspond to the run number
    run_number = df.shape[1] - 6 + 1
    df.insert(6,f"run{run_number}",run_column,False)
    return df

@st.cache(allow_output_mutation=True)
def store_var_changes(df_var, key, value):
    df_var.loc[df_var["name"] == key, "value"] = value
    return df_var

if __name__ == "__main__":
    
    
    #initialize the chip 
    chip,sys_scan = get_class()
    chip.power_up()
    all_params_df = return_main_df( "mightypix.yaml")
    df_var = pd.DataFrame()
    
    # #get all defaults. The returns are expected to be dictionaries
    # #TODO: if a paramter in streamlit GUI doesnt have a default given here, report an error that there is no default
    power_defaults = chip.power_status_mini()
    CONF_defaults = chip.dac_status_CONF()
    CONF_SR_defaults = chip.dac_status()

    # power_defaults = pickle.load(open("dict_power","rb"))
    # CONF_defaults = pickle.load(open("dict_CONF","rb"))
    # CONF_SR_defaults = pickle.load(open("dict_CONF_SR","rb"))

    #save the current default values 
    for key, val in power_defaults.items():
        all_params_df.loc[all_params_df["name"] == key, "def_value"] = float(val)

    for key, val in {**CONF_defaults, **CONF_SR_defaults}.items():
        all_params_df.loc[all_params_df["name"] == key, "def_value"] = int(val)

    # #now we combine all the defaults in one file
    
    # defaults = {}
    # for dic in [power_defaults, CONF_defaults, CONF_SR_defaults]:
    #     defaults = {**defaults, **dic}

    # #save the current default values 
    # for key, val in defaults.items():
    #     #print(key, val, "\n")
    #     all_params_df.loc[all_params_df["name"] == key, "def_value"] = val
    df_var["name"] = all_params_df["name"]
    df_var["value"] = all_params_df["def_value"]
  
    #we want a separate dataframe for each unique combination of type and subtype to keep account of return values
    DFs_dict = {}
    
    unique_type_df = all_params_df[["type","subtype"]].value_counts().reset_index()
    for ele in range(len(unique_type_df)):
        type_df = unique_type_df["type"].loc[ele]
        subtype_df = unique_type_df["subtype"].loc[ele]
        temp_df = all_params_df[["name","def_value"]].loc[(all_params_df["subtype"] == subtype_df) & (all_params_df["type"] == type_df)].reset_index()
        DFs_dict[type_df+"_"+subtype_df] = temp_df[["name","def_value"]]
    
    #implementing the streamlit script here
    st.header("Online Parameter Control")
    
    #each "type" in the parameters will have its own tab
    tab_names = [x[0] for x in all_params_df[["type"]].value_counts().axes[0]]
    tab_names.append("Scan")
    tab_list = st.tabs(tab_names)
    for i, tab in enumerate(tab_list[:-1]):
        with tab:
            if (unique_type_df["subtype"].loc[unique_type_df["type"] == tab_names[i]] != "NoSubtype").any():
                pass
                col_names = list(unique_type_df["subtype"].loc[unique_type_df["type"] == tab_names[i]])
                #proportions division for streamlit columns
                col_divison = []
                cols = {}
                for k in range(len(col_names)):
                    col_divison.extend([1,3,0.2])
                    cols[col_names[k]] = len(DFs_dict[tab_names[i]+"_"+col_names[k]])
                    
                col_list = st.columns(len(col_names))
                for j, col in enumerate(col_list):
                    with col:
                        st.button("Set Defaults", key=tab_names[i]+"_"+col_names[j], on_click=set_defaults_df_var, args=(DFs_dict[tab_names[i]+"_"+col_names[j]], ))
                    
                for r in range(max(cols.values())):
                    col_list = st.columns(col_divison)
                
                    for j, col in enumerate(col_list[1::3]):
                        with col:
                            try:
                                var = DFs_dict[tab_names[i]+"_"+col_names[j]]["name"].iloc[r]
                                temp_df = all_params_df.loc[(all_params_df["type"]==tab_names[i]) & (all_params_df["subtype"]==col_names[j])]
                                #print(var, r, j, temp_df["min_value"].loc[temp_df["name"]==var].values[0], temp_df["max_value"].loc[temp_df["name"]==var].values[0])
            
                                var_value = st.slider(
                                "",
                                min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                                max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                                step = 1.0,
                                #value= float(df_var["value"].loc[df_var["name"]==var]),
                                key=var, on_change = edit_input, args=(var,))
                                df_var = store_var_changes(df_var, var, st.session_state[var])
                            except IndexError:
                                st.write("")

                    for j, col in enumerate(col_list[0::3]):
                        with col:
                            try:
                                var = DFs_dict[tab_names[i]+"_"+col_names[j]]["name"].iloc[r]
                                temp_df = all_params_df.loc[(all_params_df["type"]==tab_names[i]) & (all_params_df["subtype"]==col_names[j])]
                                
            
                                var_value = st.number_input(
                                var,
                                min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                                max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                                #value= float(df_var["value"].loc[df_var["name"]==var]),
                                step = 1.0,
                                key=var+"input", on_change = edit_slider, args=(var,))
                                df_var = store_var_changes(df_var, var, st.session_state[var])
                            except IndexError:
                                st.write("")
                    

                # col_list = st.columns(len(col_names))
                # for j, col in enumerate(col_list):
                #     with col:
                        
                        
                #         temp_df = all_params_df.loc[(all_params_df["type"]==tab_names[i]) & (all_params_df["subtype"]==col_names[j])]
                        
                #         st.button("Set Defaults", key=tab_names[i]+"_"+col_names[j], on_click=set_defaults_df_var, args=(DFs_dict[tab_names[i]+"_"+col_names[j]], ))
                            
                #         for var in DFs_dict[tab_names[i]+"_"+col_names[j]]["name"]:
                #             var_value = st.slider(
                #                 var,
                #                 min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                #                 max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                #                 #value= float(df_var["value"].loc[df_var["name"]==var]),
                #                 key=var)
                #             df_var = store_var_changes(df_var, var, var_value)
                                

            else:
                temp_df = all_params_df.loc[(all_params_df["type"]==tab_names[i]) & (all_params_df["subtype"]=="NoSubtype")]
                st.button("Set Defaults", key=tab_names[i]+"_"+"NoSubtype", on_click=set_defaults_df_var, args=(DFs_dict[tab_names[i]+"_"+"NoSubtype"], ))
                for var in DFs_dict[tab_names[i]+"_"+"NoSubtype"]["name"]:
                    col_list = st.columns([1,3,3,3])
                    with col_list[1]:
                        var_value = st.slider(
                            "",
                            min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                            max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                            #value= float(df_var["value"].loc[df_var["name"]==var])
                            key=var, on_change = edit_input, args=(var,))
                        #df_var = store_var_changes(df_var, var, var_value)
                    with col_list[0]:
                        var_value1 = st.number_input(
                            var,
                            min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                            max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                            #value= float(df_var["value"].loc[df_var["name"]==var])
                            key=var+"input", on_change = edit_slider, args=(var,))
                    df_var = store_var_changes(df_var, var, st.session_state[var])
   


                
                
                # col_list = st.columns([5,1])
                # with col_list[0]:
                #     #temp_dict = {}
                
                #     for var in DFs_dict[tab_names[i]+"_"+"NoSubtype"]["name"]:
                #         var_value = st.slider(
                #             var,
                #             min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                #             max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]),
                #             #value= float(df_var["value"].loc[df_var["name"]==var])
                #             key=var)
                #         df_var = store_var_changes(df_var, var, var_value)
                
                # with col_list[1]:

                #     for var in DFs_dict[tab_names[i]+"_"+"NoSubtype"]["name"]:
                #         var_value = st.number_input(
                #             var,
                #             min_value= float(temp_df["min_value"].loc[temp_df["name"]==var]),
                #             max_value= float(temp_df["max_value"].loc[temp_df["name"]==var]))
                #             #value= float(df_var["value"].loc[df_var["name"]==var])
                #             #key=var)
                #         #df_var = store_var_changes(df_var, var, var_value)   
               
                   
    #sys_scan = get_obj_sysscan()
    
    with tab_list[-1]:
        
        temp_df_updated = pd.DataFrame()
        
        col1, col2 = st.columns(2)

        with col1:
            if st.button("Scan with injection"):
                
                temp_df_updated = filter_df(all_params_df, df_var)
                button_scan(temp_df_updated, sys_scan, chip, injection = True)

        with col2:
            if st.button("Scan without injection"):
                temp_df_updated = filter_df(all_params_df, df_var)
                button_scan(temp_df_updated, sys_scan, chip, injection = False)

        
        st.header("All parameters")
        st.write(all_params_df.drop(["type", "subtype", "min_value", "max_value"],axis=1))
        
        if st.button("Close Chip"):
            chip.close()
            print("Now closing the chip")
        
        "This is the current session state"
        st.session_state
        