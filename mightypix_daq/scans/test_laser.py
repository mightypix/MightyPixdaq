#!/bin/env python
import os, sys, time
import numpy as np
import bitarray
import tables as tb
import yaml

import pickle

from mightypix_daq import mightypix_new_pixel

import matplotlib.pyplot as plt

# from mightypix_daq import mk_fname

import mightypix_daq.scan_base_new as scan_base
import mightypix_daq.analysis.interpreter_Klaas as interpreter
from mightypix_daq.analysis import plotting


import mightypix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
    This scan tries to find the lowest achievable global thresholds at certain initial TRIM settings for every enabled CSA. 
"""

local_configuration = {
    "exp_time": 1.0,
    "n_inj": 1,  # Number of injections
    "th1": 1.01,  # value for threshold 1 in V
    "mask": [0,0], 
}
# mask=None

## ATTENTION: The matrix is always defined by [col,row] !!

def is_pysical_cell(col, row):
    row_cond = False
    if row <= 60:
        row_cond = row % 2 == 0
    else:
        row_cond = (row - 60) % 4 == 0
    col_cond = (col > 1) and col < 30
    return row_cond and col_cond


class ScanPixelAlive(scan_base.ScanBase):
    scan_id = "scan_PixelAlive"
    

    def scan(self, **kwargs):
        """
        This scan just injects a signal and checks the return
        """

        ####################=
        # Load kwargs or default values.
        exp_time = kwargs.pop("exp_time", 1.0)
        scan_param_id = 0
        mask = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        mask[:,:]=False
        # mask[0:16,:]=1
        # mask[17:30,:]=1
        tdac= np.ones([self.chip.COL_SIZE, self.chip.ROW_SIZE-self.chip.lowest_tdac_row],dtype=np.uint8)
        tdac[:,:]=tdac[:,:]*7
        # tdac[0:16,:]=0
        print(tdac)
        # input() 
        self.chip.mask=mask
        # mask = self.chip._create_mask()
        # print(mask)
        ####################

        ####################
        # enable readout
        self.chip.set_global_voltage(TH1=1.4)
        self.chip.set_global_voltage(TH2=1.15)
        self.chip.set_global_voltage(TH3=1.05)
        # self.chip.set_inj_all(inj_high=0.2, inj_low=0.1)
        self.chip._write_conf(en_comp=mask,tdac=tdac)
        self.chip["CONF"].write()
        # self.chip["data_rx"].ENABLE = 1
        self.chip.set_ts_inj(True)
        # self.chip.mask_rx(True)
        
        # self.chip.set_tdac(7)
        
        self.plot_flag = 1


        # for col in range(0,29,1):
        self.chip.set_read()
        while True:
            try:
                with self.readout(scan_param_id=0,                       # TODO open/close self.readout for each scan step (make it faster)
                            fill_buffer=True, clear_buffer=True,
                            reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                            readout_interval=0.001):
                    dqdata = self.fifo_readout.data
                    
                    self.chip.logger.info(self.chip.power_status())
            except KeyboardInterrupt:
                print("done bye bye")
                break
        self.chip.stop_read()
        self.chip.power_down()




    def analyze(self):
        # pass
        h5_filename = self.output_filename + ".h5"

        self.logger.info("Starting data analysis...")
        with tb.open_file(h5_filename, "r+") as h5_file:
            raw_data = h5_file.root.raw_data[:]
            meta_data = h5_file.root.meta_data[:]

            # TODO: TMP this should go to analysis function with chunking
            self.logger.info("Interpret raw data...")
            fast_interpreter = interpreter.Interpreter()
            int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw_data, meta_data=None)
            # print(int_data)
            if(len(int_data) == 0):
                self.plot_flag = 0

            if "interpreted" in h5_file.root:
                print("check")
                h5_file.remove_node(h5_file.root, "interpreted", recursive=True)

            h5_file.create_group(h5_file.root, "interpreted", "Interpreted Data")
            hist_occ, _, _ = np.histogram2d(
                int_data["col"],
                int_data["row"],
                bins=(
                    np.arange(self.chip.COL_SIZE + 1),
                    np.arange(self.chip.ROW_SIZE + 1),
                ),
            )
            h5_file.create_carray(h5_file.root.interpreted, name="HistOcc", obj=hist_occ)
            h5_file.flush()


    def plot(self, analyzed_data_file=None):
        if(self.plot_flag == 1):
            h5_filename = self.output_filename + ".h5"
            self.logger.info("Starting plotting...")
            with tb.open_file(h5_filename, "r") as h5_file:

                # Q: Maybe Plotting should not know about the file?
                # todo put the log files in the same file?
                # with plotting.Plotting(h5_filename, save_png=True, save_single_pdf=True, log=True) as p:
                pdf_output=""
                with plotting.Plotting(analyzed_data_file=h5_filename) as p:
                    p.create_config_table()
                    p.create_standard_plots(mask=None)
                    pdf_output=p.filename
                with plotting.Plotting(analyzed_data_file=h5_filename,pdf_file=pdf_output.replace(".pdf","_log.pdf"), log=True) as p:
                    p.create_config_table()
                    p.create_standard_plots(mask=None)
        else:
            print("No data to plot")


if __name__ == "__main__":
    from mightypix_daq import mightypix_new_pixel
    import argparse

    parser = argparse.ArgumentParser(
        usage="python scan_source.py -t1 1.4 -t2 1.1 -t3 1.05 -f 0:44 -p -time 50",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("-conf", "--config_file", type=str, default=None)
    parser.add_argument("-t1", "--TH1", type=float, default=1.4)
    parser.add_argument("-t2", "--TH2", type=float, default=1.1)
    parser.add_argument("-t3", "--TH3", type=float, default=1.05)
    parser.add_argument("-f", "--flavor", type=str, default=None)
    parser.add_argument("-o", "--output", type=str, default=None)
    parser.add_argument("-a", "--analyze", type=str, default=None)
    parser.add_argument("-p", "--power_reset", action="store_const", const=1, default=0)  # Default = True: Skip power reset.
    parser.add_argument("-time", "--scan_time", type=int, default=None, help="Scan time in seconds.")
    parser.add_argument("-dout", "--output_dir", type=str, default=None)
    parser.add_argument("-m", "--mask", type=str, default=None)
    args = parser.parse_args()


    # load config
    m = mightypix_new_pixel.MightyPix(conf="mightypix_adapter_MUDAQ_new_V5.yaml",no_power_reset=not bool(args.power_reset))
    # m = mightypix_new_pixel.MightyPix(conf="mightypix_new_pixel.yaml",no_power_reset=not bool(args.power_reset))

    if args.analyze is None:
        if not m.isInitialized:
            m.init()
        m.set_inj_all()
    if args.config_file is not None:
        m.load_config(args.config_file)

    local_configuration|= vars(args)

    if args.flavor is not None:
        if args.flavor == "all":
            collist = range(0, m.COL_SIZE)
            m.logger.info("Enabled: Full matrix")
        else:
            tmp = args.flavor.split(":")
            collist = range(int(tmp[0]), int(tmp[1]))
            m.logger.info("Enabled: Columns {0:s} to {1:s}".format(tmp[0], tmp[1]))

        pix = []
        for i in collist:
            for j in range(0, m.ROW_SIZE):
                pix.append([i, j])
    else:
        pix = []

    if args.mask is not None:
        h5_mask_file= tb.open_file(args.mask, "r")
        mask=np.transpose(h5_mask_file.root.mask)

    if len(pix) > 0:
        local_configuration["pix"] = pix
    else:
        pass

    if args.scan_time is not None:
        local_configuration["scan_time"] = args.scan_time


    if args.output is not None:
        scan = ScanPixelAlive(m, fout=args.output, online_monitor_addr="tcp://127.0.0.1:6500")
    else:
        scan = ScanPixelAlive(m, online_monitor_addr="tcp://127.0.0.1:6500")
    
    if args.analyze is None:
        scan.start(**local_configuration)
    scan.analyze()
    scan.plot()