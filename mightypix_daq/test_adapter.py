#!/usr/bin/env python

from mightypix_daq.mightypix_adapter import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

import mightypix_daq.analysis.interpreter as interpreter


def reset_chip(chip):
    chip.power_cycle()
    # chip.set_th(th_id=1, th_value=1.05)
    # chip.set_inj_all(inj_high=0, inj_low=4095) #you set inj_high and low in dacs and they are converted to V! inj_high = is equal to 1.4V!
    chip.enable_column()
    # chip.enable_injection(0, 0)
    # chip.enable_injection(1, 0)
    # chip.enable_injection(20, 20)
    # chip.enable_ampout(20)
    # chip.enable_injection(5, 0)
    # chip.enable_injection(0, 1)
    # chip.enable_injection(1, 1)
    # chip.enable_injection(2, 1)
    # chip.enable_injection(3, 1)
    chip.write_global_conf()
    # chip["CONF"]["SYNC"] = 1
    # chip["CONF"].write()
    # chip["CONF"]["SYNC"] = 0
    chip["CONF"].write()


start_t = time.process_time()
start_tt = time.time()
# chip = MightyPix(conf="mightypix_mio3_v2.yaml")
# chip = MightyPix(conf="mightypix_mio3.yaml")
chip = MightyPix(conf="mightypix_adapter.yaml")
# chip = MightyPix(conf="mightypix_v2.yaml")
chip.init()
chip.logger.info(chip.power_status())
# chip.set_th(th_id=1, th_value=1.4)
# chip.set_th(th_id=2, th_value=1.1)
# chip.set_th(th_id=3, th_value=1.4)

chip.set_inj_all(inj_high=0.9, inj_low=0.7)
input()

# while True:
#     chip.start_inj()
#     time.sleep(0.5)
# input()
time.sleep(0.1)
#

# chip.set_global_current(Vminus=-0.0001)
# time.sleep(0.1)
# chip.set_global_current(Vminus=-200)
# time.sleep(0.1)
# chip.set_global_current(Vminus=-1)
# invert = False
# chip.enable_injection(1, 1)

# chip.write_current_config()
#
# # # invert = True
# th_value=1.0
# chip.set_th(th_id=2, th_value=1.1)
# while True:
#     # th_value+=0.01
#     # chip.set_th(th_id=2, th_value=th_value)
#     if invert:
#         chip.enable_mask(14, 69)
#     else:
#         chip.mask = chip._create_mask("none")
#     invert=not invert
#     chip.write_global_conf()
#     time.sleep(0.01)
#     chip.set_read()
#     raw = chip.get_data()
#     print(len(raw))
#     # print(th_value,chip["TH2"].get_voltage(unit="V"),len(raw))
#     # int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
#     # print(int_data)
#     # for name in int_data.dtype.names:
#     #     print(f"{name:>12}", end="")
#     # print()
#     # for ihit, hit in enumerate(int_data):
#     #     for name in int_data.dtype.names:
#     #         value = int_data[name][ihit]
#     #         if "raw" in name:
#     #             print(f"{value:>12X}", end="")
#     #         else:
#     #             print(f"{value:>12}", end="")
#     #     print()

#     # print(len(int_data))
#     chip.stop_read()


# fast_interpreter = interpreter.Interpreter()
chip.logger.info(chip.power_status()["Power [mW]"])
reset_chip(chip)

default_lp2_counter = chip["data_rx"].LOAD_PIXEL2_COUNTER
# print(default_lp2_counter, type(default_lp2_counter))

chip.enable_injection(20, 20)
chip.enable_ampout(20)
chip.write_global_conf()

# self["CONF"]["RESET"] = 0
# self["CONF"].write()
# chip.enable_injection(5, 0)
# chip.enable_injection(0, 1)
# chip.enable_injection(1, 1)
# chip.enable_injection(2, 1)
# chip.enable_injection(3, 1)
chip.set_read()
raw = chip.get_data()
fast_interpreter = interpreter.Interpreter()
int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
print("data", int_data)


input()
get_data

chip.logger.info(f"lp2 counter after loop: {chip['data_rx'].LOAD_PIXEL2_COUNTER}")
# chip["CONF_SR"][dac] = default_value
reset_chip(chip)


start = 0
daq = ["IPFoll", "INFoll2", "VNFoll", "IPRegCasc"]

for dac in chip["CONF_SR"]._fields:
    default_value = bitarray.util.ba2int(chip["CONF_SR"][dac])
    chip.logger.info(f"Testing dac {dac}")
    chip.logger.info(chip.power_status()["Power [mW]"])
#     # chip["fifo"].RESET = 1
#     # chip["fifo"].RESET = 0
#     # chip["fifo"]["RESET"]

    #  quick FIFO test 
    # for i in range(10):
    #     time.sleep(1)

    #     fifo_data = chip[chip.which_fifo].get_data()
    #     data_size = len(fifo_data)
    #     data_gen = np.linspace(start, data_size - 1 + start, data_size, dtype=np.int32)

    #     comp = (fifo_data == data_gen)
    #     chip.logger.info(str((i, " OK?:", comp.all(), float(32 * data_size) / pow(10, 6), "Mbit")))
    #     start += data_size
    #     chip.logger.warn("FIFO Size after pulse: %d ", chip[chip.which_fifo].FIFO_SIZE)
    #     chip.logger.warn("FIFO internal Size after pulse: %d", chip["data_rx"].get_fifo_size())
    #     print(chip[chip.which_fifo].get_data())

    if len(chip["CONF_SR"][dac]) > 2:
        continue
    for value in range(2 ** len(chip["CONF_SR"][dac])):
        chip["CONF_SR"][dac] = value
        chip.write_current_config()
        # chip.write_global_conf()
        # time.sleep(0.1)
        # print(f"RX Enable:  {chip['data_rx'].READY}")
        chip.set_read()
        chip["fifo"].RESET = 1
        chip["fifo"].RESET = 0
        chip["fifo"]["RESET"]
        data = chip.get_data()
        chip.logger.info(chip.get_rx_status())
        int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
        print("data", int_data)
        chip.stop_read()
    #     if chip["data_rx"]["LOST_DATA_COUNTER"] == 0:
    #         chip.stop_read()cle()
    # chip.set_th(th_id=1, th_value=1.05)
    # chip.set_inj_all(inj_high=0, inj_low=4095) #you set inj_high and low in dacs and they are converted to V! inj_high = is equal to 1.4V!
    # chip.enable_column()
    # # chip.enable_injection(0, 0)
    # # chip.enable_injection(1, 0)
    # chip.enable_injection(20, 20)
    # chip.enable_ampout(20)
    # chip.enable_injection(5, 0)
    # chip.enable_injection(0, 1)
    # chip.enable_injection(1, 1)
    # chip.enable_injection(2, 1)
    # chip.enable_injection(3, 1)
    #         chip.set_read()

    #         # chip.start_inj()
    #         data = chip.get_data()
    #         if len(data) > 0:
    #             chip.logger.info(f"We have data!! For dac {dac} = {value}")
    #     power_status = chip.power_status()
    #     chip.logger.info(f"dac {dac} value {value} Full Power: {power_status['Power [mW]']} Analogue Power: {power_status['Analogue Power [mW]']} Digital Power: {power_status['Digital Power [mW]']}")
    # count = 0
    # for raw in data:
    #     count += 1
    #     # print(type(raw))
    #     chip.logger.info("%s  %s" % (hex(raw), bin(raw)))
    #     if count > 100:
    #         break
    # else:
    #     print("No data!!")

    # if dac not in daq:
    #     continue
    # for value in range(0, 2 ** len(chip["CONF_SR"][dac]), 10):
    #     chip["CONF_SR"][dac] = value
    #     chip.write_global_conf()
    #     chip.set_read()
    #     raw = chip.get_data()
    #     chip.stop_read()

    #     int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
    #     print(dac, value, len(raw), len(int_data))
        # print(int_data)
        # for name in int_data.dtype.names:
        #     print(f"{name:>12}", end="")
        # print()
        # for ihit, hit in enumerate(int_data):
        #     if ihit>10:
        #         break
        #     for name in int_data.dtype.names:
        #         value = int_data[name][ihit]
        #         if "raw" in name:
        #             print(f"{value:>12X}", end="")
        #         else:
        #             print(f"{value:>12}", end="")
        #     print()

    chip.logger.info(f"lp2 counter after loop: {chip['data_rx'].LOAD_PIXEL2_COUNTER}")
    chip["CONF_SR"][dac] = default_value
    reset_chip(chip)

    # print(type(default_value))
    # print(dac)

chip.close()
