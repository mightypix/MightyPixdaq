#!/usr/bin/env python

from mightypix_daq.mightypix_adapter import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

import mightypix_daq.analysis.interpreter as interpreter


def reset_chip(chip):
    chip.power_cycle()
    chip.enable_column()
    # chip.enable_injection(0, 0)
    chip.enable_injection(1, 0)
    # chip.enable_injection(20, 20)
    chip.enable_ampout(1)
    # chip["data_rx"].REORDER = 1
    chip.write_global_conf()
    # chip["CONF"]["SYNC"] = 1
    # chip["CONF"].write()
    # chip["CONF"]["SYNC"] = 0
    # chip["CONF"].write()

# input()
start_t = time.process_time()
start_tt = time.time()
chip = MightyPix(conf="mightypix_adapter.yaml")
# chip = MightyPix(conf="mightypix_adapter_MUDAQ.yaml")
chip.init()
chip.set_inj_all(inj_high=0.9, inj_low=0.5)
chip.set_global_voltage(TH1=1.4)
chip.set_global_voltage(TH2=1.05)
chip.set_global_voltage(TH3=1.05)
# chip.set_mgt_ref("ext")
# input()
reset_chip(chip)
chip.write_global_conf()
# input()
while True:

    chip.write_global_conf()    
    # chip["data_rx"].reset()
    chip["data_rx"].REORDER = 0
    chip["data_rx"].NOT_FILTER_COLUMN2 = 0 #not implemented at the moment in receiver
    chip["data_rx"].ENABLE = 1
    # chip["CONF"]["DISABLE_CLK"] = 1
    # chip["CONF"]["DISABLE_REF_CLK"] = 1
    # chip["CONF"]["DISABLE_EXT_CLK"] = 1
    # chip["CONF"].write()
    # time.sleep(2)
    # chip["CONF"]["DISABLE_CLK"] = 0
    chip["CONF"].write()
    # time.sleep(0.5)
    # chip.enable_injection(8, 8)
    # chip.enable_ampout(8)
    # chip.enable_hitbus(8)
    # chip.enable_injection(6, 6)
    # chip.enable_ampout(6)
    # chip.enable_hitbus(6)
    # chip.enable_injection(4, 4)
    # chip.enable_ampout(4)
    # chip.enable_hitbus(4)
    chip.set_inj_all(inj_high=1, inj_low=0.5, inj_n=1, inj_width=50000, delay=700)
    chip.enable_injection(8, 8)
    chip.enable_ampout(8) 
    chip.enable_hitbus(8)
    chip.write_current_config()
    chip.start_inj()
    # chip["CONF_SR"]["sendcounter"] =1
    # time.sleep(0.5)
    # input()
    chip.set_read()
    chip.logger.info(chip.get_rx_status())
    print(f"RX READY:  {chip['data_rx'].READY}")
    print(f"rx Reset:  {chip['data_rx'].RX_RESET}")
    print(f" Reset:  {chip['data_rx'].RESET}")
    print(f"Load col2 cnt:  {chip['data_rx'].LOAD_COLUMN2_COUNTER}")
    print(f"FIFO SIZE :  {chip['data_rx'].FIFO_SIZE}")
    chip.set_read()
    raw = chip.get_data()
    fast_interpreter = interpreter.Interpreter()
    int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
    print("raw", raw)
    for iraw in raw[:1000]:
        # if hex(iraw) == "0xbcbc":
        print(hex(iraw))
    # # print("debug output",debug_out)
    print("data", int_data)
chip.close()
