#!/usr/bin/env python

from mightypix_daq.mightypix_new_pixel import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

# import mightypix_daq.analysis.interpreter as interpreter
import mightypix_daq.analysis.interpreter_Klaas as interpreter

col = 15
row = 0

start_t = time.process_time()
start_tt = time.time()

chip = MightyPix(conf="mightypix_adapter_MUDAQ_new.yaml")
# chip = MightyPix(conf="mightypix_new_pixel.yaml")
chip.init()
print("done with init")
comp_mask = np.copy(chip.PixelConf['en_comp0'])
comp_mask[:,:] = 0
comp_mask[col, row] = 1
chip.set_pix(comp_mask, comp="both")
print("done with enabling")
chip.set_inj_all(inj_high=0.7, inj_low=0.15, inj_width=2**6, delay=2**8)
chip.set_global_voltage(TH1=1.4)
chip.set_global_voltage(TH2=1.2)
chip.set_global_voltage(TH3=1.2)



while True:
    chip.logger.info(chip.get_rx_status())

    chip.set_en_ampout(cols = col)
    chip.set_en_hitbus(cols=[col])
    chip.set_en_inj(cols = [col], rows =[row])
    # tdac = np.copy(chip.PixelConf['tdac0'])
    # chip.set_tdac(tdac = 7, comp="both")

    chip.start_inj()

    chip["data_rx"].ENABLE = 1
    chip.set_read()
    chip.logger.info(chip.get_rx_status())
    raw = chip.get_data()
    fast_interpreter = interpreter.Interpreter()
    int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
    print("raw", raw)
    for iraw in raw[:100]:
        print(hex(iraw))
    print("data", int_data)
    print("idle pattern: b00_1111_1010, b11_0000_0101")
    print(bin(chip['data_rx']["RAW_DATA0"]))
    print(bin(chip['data_rx']["RAW_DATA1"]))
    chip.stop_read()
    # break
chip.close()
