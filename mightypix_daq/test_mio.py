#!/usr/bin/env python

from mightypix_daq.mightypix import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

import mightypix_daq.analysis.interpreter as interpreter


def reset_chip(chip):
    chip.power_cycle()
    chip.enable_column()
    chip.enable_injection(0, 0)
    chip.enable_injection(1, 0)
    chip.enable_injection(20, 20)
    chip.enable_ampout(20)
    chip["data_rx"].INVERT = 1
    chip.write_global_conf()
    # chip["CONF"]["SYNC"] = 1
    # chip["CONF"].write()
    # chip["CONF"]["SYNC"] = 0
    # chip["CONF"].write()


start_t = time.process_time()
start_tt = time.time()
chip = MightyPix(conf="mightypix.yaml")
chip.init()
chip.set_inj_all(inj_high=0.9, inj_low=0.7)

reset_chip(chip)
input()
while True:
    chip.write_global_conf()
    time.sleep(0.5)
    chip.start_inj()
    time.sleep(0.5)
    # chip["data_rx"].reset(1)
    chip.logger.info(chip.get_rx_status())
    print(f"RX Enable:  {chip['data_rx'].READY}")
    print(f"rx Reset:  {chip['data_rx'].RX_RESET}")
    print(f" Reset:  {chip['data_rx'].RESET}")
    print(f"Load col2 cnt:  {chip['data_rx'].LOAD_COLUMN2_COUNTER}")
    print(f"FIFO SIZE :  {chip['data_rx'].FIFO_SIZE}")
    chip.set_read()
    raw = chip.get_data()
    fast_interpreter = interpreter.Interpreter()
    int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
    print("raw", raw)
    print("data", int_data)
chip.close()

