#!/usr/bin/env python

from mightypix_daq.Toko_daq.panda2 import Panda2
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle

import mightypix_daq.analysis.interpreter as interpreter

dut = Panda2("/home/lab/daq/MightyPixdaq_fast_receiver2/mightypix_daq/mightypix_panda.yaml")
dut.init()
# en_comp = np.copy(dut['CONF']['PixelConf']['en_comp'])
# tdac = np.copy(dut['CONF']['PixelConf']['tdac'])


## configure chip and enable rx
col, row = 15, 0
# en_comp[:,:] = 0
# en_comp[col, row] = 1
# dut.set_en_comp(en_comp, comp='both')
dut.set_en_comp('all')

dut.set_en_hitbus([col])
#dut.set_en_hitbus('all')
#dut.set_en_hitbus('none')

dut.set_en_ampout([col])
#dut.set_en_ampout('none')

dut.set_en_inj([col, row])
dut.set_rx(False)
dut.set_tdac(7, comp='both')
# tdac = np.copy(dut['CONF']['PixelConf']['tdac'])
dut.set_inj_amp()
dut.set_inj(inj_n=100, inj_width=1024*8, inj_delay=1024*8)
dut.power_up()
dut.set_conf()  ## set lowest (largest in number) threshold
dut["data_rx"].ENABLE = 1
# dut.set_sync(auto_sync=True)

while True:
    # dut.set_ts_inj(True)
    # dut.set_rx(True)
    print("RX status:\n%s" % (dut.get_rx_status()))
    # dut.mask_rx(True)
    # dut.inject()
    raw = dut.get_data()
    print("RX status:\n%s" % (dut.get_rx_status()))
    dut.set_rx(False)
    print("RX status:\n%s" % (dut.get_rx_status()))
    # fast_interpreter = interpreter.Interpreter()
    # int_data, _, debug_out = fast_interpreter.interpret_data(raw_data=raw, meta_data=None)
    print("raw", raw)
    for iraw in raw[:1000]:
        # if hex(iraw) == "0xbcbc":
        print(hex(iraw))
    # # print("debug output",debug_out)
    # print("data", int_data)
    
