#!/usr/bin/env python
'''script to check different parameters for any given parameter for a range of values with online monitor'''
from mightypix_daq.mightypix_new_pixel import MightyPix
import time
import bitarray
import bitarray.util
import numpy as np
import copy
import pickle
import scan_base
import argparse
import mightypix_daq.analysis.interpreter as interpreter


def reset_chip(chip):
    chip.set_conf()


class SystematicScan(scan_base.ScanBase):
    scan_id = "systematic_scan"
    def scan(self, **kwargs):
        
        scan_param_id = 0
        key = kwargs.pop("key", None)
        val = kwargs.pop("val", True)
        injection = kwargs.pop("injection", True)

        if (key == None) | (val == None):
            print("Guess i m running the script with default parameter")
            
        else:
                    
            #INJ_LO and INJ_HI parameters are returned from power_Status dict but can only be adjusted as dac parameters
            
            if ((key == "INJ_LO") | (key == "INJ_HI")):
                self.chip[key].set_voltage(val, unit="V")
                self.chip.SET_VALUE[key] = val

            elif (key in self.chip.power_status_mini().keys()):
                print(key,val,"global_voltage")
                self.chip.set_global_voltage(setting=key, value = val) 
            
            elif (key in self.chip.dac_status().keys()):
                print(key,val,"dac_SR")
                self.chip["CONF_SR"][key] = int(val)
            
            elif (key in self.chip.dac_status_CONF().keys()):
                print(key,val,"dac")
                self.chip["CONF"][key] = int(val)
            
            else:
                print("Error: key not found in list of adjustable parameters")
            
            time.sleep(0.1)
        print(key,val)
        print(self.chip.power_status())
            
        self.chip.logger.info(chip.power_status())
        #fast_interpreter = interpreter.Interpreter()
        self.chip.logger.info(chip.power_status()["Power [mW]"])
        
        #TODO:this changes INJ_LO and INJ_HIGH parameters to their default values. Change it later
        #reset_chip(self.chip)
        inj_high = self.chip.SET_VALUE["INJ_HI"]
        inj_low = self.chip.SET_VALUE["INJ_LO"]
        self.chip.set_inj_all(inj_high=inj_high, inj_low=inj_low)
        # self.chip.enable_column()
        #default_lp2_counter = self.chip["data_rx"].LOAD_PIXEL2_COUNTER
        
        # self.chip.enable_injection(20, 20)
        # self.chip.enable_ampout(20)

        # self.chip.write_global_conf()
        self.chip.set_read()
        
        #It first goes into the self.readout functiona and runs the code until "yield" and then does self.chip.start_inj(), after this it resumes the readout() function after yield
        with self.readout(
            scan_param_id=scan_param_id,
            fill_buffer=False,
            clear_buffer=True,
            readout_interval=0.01,
            timeout=0.2,
        ):
            if injection:
                self.chip.start_inj()
            else:
                pass
        self.chip.stop_read()
        
        # print('\n\n############################################################################')
        # print(f"{len(self.n_readouts)}")
        # print('############################################################################\n\n')
        reset_chip(chip)
        self.chip.power_cycle()
        #self.chip.set_global_voltage(setting=key, value = def_value)  


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(
        usage="python test_param.py -p VSSA -lb 0 -ub 2.5 -s 0.1",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument("-p", "--parameter", type=str, default=None)
    parser.add_argument("-lb", "--lower_bound", type=float, default=None)
    parser.add_argument("-ub", "--upper_bound", type=float, default=None)
    parser.add_argument("-s", "--step", type=float, default=None)

    
    start_t = time.process_time()
    start_tt = time.time()
    chip = MightyPix(conf = "mightypix_adapter_MUDAQ_new_V5.yaml")
    
    chip.init()
    
    args = parser.parse_args()
    if args.parameter:
        key = args.parameter
    else: 
        print("Error: No parameter provided")
        exit(0)

    if args.lower_bound:
        _min = args.lower_bound
    else: 
        print("Error: No lower bound provided")
        exit(0)
    if args.upper_bound:
        _max = args.upper_bound
    else: 
        print("Error: No upper bound provided")
        exit(0)
    if args.step:
        step = args.step
    else: 
        print("Error: No ste size provided")
        exit(0)
    

    power_defaults = chip.power_status_mini()
    CONF_defaults = chip.dac_status_CONF()
    CONF_SR_defaults = chip.dac_status()

    def_value = None
    for k,v in {**power_defaults, **CONF_defaults, **CONF_SR_defaults}.items():
        if k == key:
            def_value = v
            break
    if def_value == None:
        print("Parameter not found")
        exit(0)
    
    loop_list = np.arange(_min, _max, step).tolist()
    loop_list.append(def_value)
    

    for val in loop_list:
        sys_scan = SystematicScan(chip, online_monitor_addr="tcp://127.0.0.1:5500")
        sys_scan.current_param = (key, val)
        
        sys_scan.start(key = key, val = val, injection = True)
        #int_data = scan.analyze()
        #chip.logger.info(f"{key}_out_test: {val}, {len(int_data)}")
        # print('\n\n############################################################################')
        # print(f"{key}_out_test: {val}, {len(int_data)}")
        # print('############################################################################\n\n')

        reset_chip(chip)
    
        chip.power_cycle()

    #chip.set_global_voltage(setting=key, value = def_value)  



chip.close()
