/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ps / 1ps


`include "mightypix_daq/firmware/src/mightypix_mio3.v"
// `include "encode.v"
`include "encode_8b10b.v"

module tb (
    input wire FCLK_IN,

    //full speed
    inout wire [7:0] BUS_DATA,
    input wire [15:0] ADD,
    input wire RD_B,
    input wire WR_B,

    //high speed
    inout wire [7:0] FD,
    input wire FREAD,
    input wire FSTROBE,
    input wire FMODE
);

wire [19:0] SRAM_A;
wire [15:0] SRAM_IO;
wire SRAM_BHE_B;
wire SRAM_BLE_B;
wire SRAM_CE1_B;
wire SRAM_OE_B;
wire SRAM_WE_B;

wire DATA_LVDS;

mightypix_mio fpga (
    .FCLK_IN(FCLK_IN),

    .BUS_DATA(BUS_DATA),
    .ADD(ADD),
    .RD_B(RD_B),
    .WR_B(WR_B),
    .FDATA(FD),
    .FREAD(FREAD),
    .FSTROBE(FSTROBE),
    .FMODE(FMODE),

    .SRAM_A(SRAM_A),
    .SRAM_IO(SRAM_IO),
    .SRAM_BHE_B(SRAM_BHE_B),
    .SRAM_BLE_B(SRAM_BLE_B),
    .SRAM_CE1_B(SRAM_CE1_B),
    .SRAM_OE_B(SRAM_OE_B),
    .SRAM_WE_B(SRAM_WE_B),

    .DATA_LVDS(DATA_LVDS)

);

//SRAM Model
reg [15:0] sram [1048576-1:0];
assign SRAM_IO = !SRAM_OE_B ? sram[SRAM_A] : 16'hzzzz;
always@(negedge SRAM_WE_B)
    sram[SRAM_A] <= SRAM_IO;

wire CLK_SER;
assign CLK_SER = fpga.CLK80;

reg [3:0] cnt10 = 0;
always@(posedge CLK_SER)
    if (cnt10 == 9)
        cnt10 <= 0;
    else
        cnt10 <= cnt10 + 1;

wire load;
assign load = (cnt10 == 9);

logic [PSIZE-1:0][8:0] pattern;
localparam PSIZE = 100;

initial begin
    // pattern[0] = {1'b1, 8'hCB}; //K28_5
    pattern[0] = {1'b1, 8'h0}; //K28_5
    for (int i=1; i < 40; i = i + 1 )
    begin
        //  pattern[i] = {1'b1, i};
        // pattern[i] = {1'b1, 8'hCB}; //K28_5
        pattern[i] = {1'b1, 8'hCB}; //K28_5
        // pattern[i] = {1'b1, 8'hbc}; //K28_5
    end
    for (int i=72; i < 100; i = i + 1 )
    begin
        pattern[i] = {1'b1, 8'hCB}; //K28_5
    end

    // Send counter 1
    // DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0};
    // CommaOut <= 4'b1_0_0_0;

    // pattern[50] = {1'b0, 8'h3c};
    // pattern[51] = {1'b0, 8'ha0};
    // pattern[52] = {1'b0, 8'h05};
    // pattern[53] = {1'b1, 8'h0};

    // pattern[54] = {1'b0, 8'hC0};
    // pattern[55] = {1'b0, 8'hC0};
    // pattern[56] = {1'b0, 8'hC0};
    // pattern[57] = {1'b0, 8'hAA};

    // pattern[58] = {1'b0, 8'hC0};
    // pattern[59] = {1'b0, 8'hC0};
    // pattern[60] = {1'b0, 8'hC0};
    // pattern[61] = {1'b0, 8'hAA};

    // pattern[62] = {1'b0, 8'hC0};
    // pattern[63] = {1'b1, 8'h00};

    //lets try some real data:

    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3c72b | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3c72b | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3c72b | 1 | 0x1aa1c
    // last 32: 0x1caac054 | 0x8 0b1000 | 0xc3c72b | 1 | 0x54c0
    // last 32: 0xc054fb86 | 0x0 0b0 | 0xc3c72b | 1 | 0x86fb    -> lp2
    // last 32: 0xc054fb86 | 0x0 0b0 | 0xc3ecfb | 1 | 0xc3ecfb  ->ts_fpga
    // last 32: 0xfb86c109 | 0x0 0b0 | 0xc3ecfb | 1 | 0x9c1     
    // last 32: 0xc109f5ff | 0x0 0b0 | 0xc3ecfb | 1 | 0xfff5   -> rc2
    // last 32: 0xf5ff0c93 | 0x0 0b0 | 0xc3ecfb | 1 | 0x930c
    // last 32: 0xc9320ff | 0x0 0b0 | 0xc3ecfb | 1 | 0xff20   ->rc4
    // last 32: 0x20ff1caa | 0x2 0b10 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3ecfb | 1 | 0x1aa1c
    // last 32: 0x1caac055 | 0x8 0b1000 | 0xc3ecfb | 1 | 0x55c0
    // last 32: 0xc055a774 | 0x0 0b0 | 0xc3ecfb | 1 | 0x74a7  -> lp2
    // last 32: 0xc055a774 | 0x0 0b0 | 0xc3fa6b | 1 | 0xc3fa6b ->ts_fpga
    // last 32: 0xa774c109 | 0x0 0b0 | 0xc3fa6b | 1 | 0x9c1
    // last 32: 0xc109f5ff | 0x0 0b0 | 0xc3fa6b | 1 | 0xfff5  -> rc2
    // last 32: 0xf5ff1e37 | 0x0 0b0 | 0xc3fa6b | 1 | 0x371e
    // last 32: 0x1e3789ad | 0x0 0b0 | 0xc3fa6b | 1 | 0xad89 ->rc4
    // last 32: 0x89ad1caa | 0x2 0b10 | 0xc3fa6b | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3fa6b | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3fa6b | 1 | 0x1aa1c
    // last 32: 0x1caa1caa | 0xa 0b1010 | 0xc3fa6b | 1 | 0x1aa1c
    // last 32: 0x1caac056 | 0x8 0b1000 | 0xc3fa6b | 1 | 0x56c0
    // last 32: 0xc0561f10 | 0x0 0b0 | 0xc3fa6b | 1 | 0x101f
    // last 32: 0xc0561f10 | 0x0 0b0 | 0xc403cb | 1 | 0xc403cb
    // last 32: 0x1f10c109 | 0x0 0b0 | 0xc403cb | 1 | 0x9c1
    // last 32: 0xc109f5ff | 0x0 0b0 | 0xc403cb | 1 | 0xfff5
    // last 32: 0xf5ff1735 | 0x0 0b0 | 0xc403cb | 1 | 0x3517
    // last 32: 0x1735c96f | 0x0 0b0 | 0xc403cb | 1 | 0x6fc9
    // last 32: 0xc96f1caa | 0x2 0b10 | 0xc403cb | 1 | 0x1aa1c
     

     pattern[40] = {1'b1, 8'h1c};
     pattern[41] = {1'b0, 8'haa};
     pattern[42] = {1'b1, 8'h1c};
     pattern[43] = {1'b0, 8'haa};
     
     pattern[44] = {1'b1, 8'hbc};
     pattern[45] = {1'b1, 8'hbc};
     pattern[46] = {1'b1, 8'hbc};
     pattern[47] = {1'b1, 8'hbc};

     pattern[48] = {1'b1, 8'h1c};
     pattern[49] = {1'b0, 8'haa};
     pattern[50] = {1'b1, 8'h1c};
     pattern[51] = {1'b0, 8'haa};
     //0xc055a774
     pattern[52] = {1'b0, 8'hc0};
     pattern[53] = {1'b0, 8'h55};
     pattern[54] = {1'b0, 8'ha7};
     pattern[55] = {1'b0, 8'h74};
     
    //  0xc109f5ff
     pattern[56] = {1'b0, 8'hc1};
     pattern[57] = {1'b0, 8'h09};
     pattern[58] = {1'b0, 8'hf5};
     pattern[59] = {1'b0, 8'hff};

    //  0x1e3789ad
     pattern[60] = {1'b0, 8'h1e};
     pattern[61] = {1'b0, 8'h37};
     pattern[62] = {1'b0, 8'h89};
     pattern[63] = {1'b0, 8'had};

     pattern[64] = {1'b1, 8'h1c};
     pattern[65] = {1'b0, 8'haa};
     pattern[66] = {1'b1, 8'h1c};
     pattern[67] = {1'b0, 8'haa};

     pattern[68] = {1'b1, 8'h1c};
     pattern[69] = {1'b0, 8'haa};
     pattern[70] = {1'b1, 8'h1c};
     pattern[71] = {1'b0, 8'haa};


// '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80d783', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80df53', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80e723', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa'

    
end

logic dispout, dispin = 0; 
always@(posedge CLK_SER)
    if (load)
        dispin <= dispout;

integer wcounter = 0;
always@(posedge CLK_SER)
    if (load)
        if (wcounter < PSIZE-1)
            wcounter <= wcounter +1;
        else
            wcounter <= 0;

integer sync_time = 0;
always@(posedge CLK_SER)
    if (load)
        sync_time <= sync_time + 1;


logic [8:0] datain;
assign datain = sync_time > 10 ? pattern[wcounter] : {1'b1, 8'hbc};

logic [9:0] dataout_enc;
encode_8b10b enc(.datain(datain[7:0]), .k(datain[8]), .dispin(dispin), .dataout(dataout_enc), .dispout(dispout)) ;

logic [9:0] ser_out;
always@(posedge CLK_SER)
    if (load)
        ser_out <= dataout_enc;
    else
        ser_out[9:0] <= {1'bx, ser_out[9:1]};

assign DATA_LVDS = ser_out[0];

initial begin
    $dumpfile("mightypix.vcd");
    $dumpvars(0,pattern);
    $dumpvars(0);
end

endmodule
