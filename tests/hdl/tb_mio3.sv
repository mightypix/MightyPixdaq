/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University 
 * ------------------------------------------------------------
 */

`timescale 1ps / 1ps

`define COCOTB_SIM 1
`include "mightypix_daq/firmware/src/mightypix_mio3.v"

// // fpga
// `include "mightypix_daq/firmware/src/mightypix_core.v"
// `include "clk_gen.v"
`include "utils/clock_multiplier.v"
`include "utils/DCM_sim.v"
`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"
`include "utils/OBUFDS_sim.v"
// `include "../mightypix_daq/firmware/IP/fast_reciever//fast_data_input/fast_data_input_sim_netlist.v"


// // `include "timestamp640/timestamp640.v"
// // `include "timestamp640/timestamp640_core.v"
// `include "mightypix_daq/firmware/src/lib/mightypix_data_rx.v"
// `include "mightypix_daq/firmware/src/lib/mightypix_data_rx_core.v"
// `include "mightypix_daq/firmware/src/lib/receiver_logic.v"
// `include "mightypix_daq/firmware/src/lib/rec_sync_scratch.v"
// `include "tlu_slave/tlu_slave.v"
// `include "tlu_slave/tlu_slave_core.v"
// `include "tlu_slave/tlu_slave_fsm.v"

// // basil modules
// `include "utils/3_stage_synchronizer.v"
// `include "utils/flag_domain_crossing.v"
// `include "utils/bus_to_ip.v"
// `include "utils/fx2_to_bus.v"
// `include "utils/reset_gen.v"
// `include "utils/generic_fifo.v"
// `include "sram_fifo/sram_fifo_core.v"
// `include "sram_fifo/sram_fifo.v"
// `include "utils/ODDR_sim.v"
// `include "utils/BUFG_sim.v"

// // basil modules in mightypix_core

// `include "rrp_arbiter/rrp_arbiter.v"
// `include "gpio/gpio.v"
// `include "gpio/gpio_core.v"
// `include "spi/spi_core.v"
// `include "spi/spi.v"
// `include "spi/blk_mem_gen_8_to_1_2k.v"
// `include "pulse_gen/pulse_gen_core.v"
// `include "pulse_gen/pulse_gen.v"
// `include "utils/CG_MOD_pos.v"
// `include "utils/cdc_pulse_sync.v"
// `include "utils/cdc_syncfifo.v"
// `include "utils/cdc_reset_sync.v"
// `include "utils/ddr_des.v"
// `include "utils/clock_divider.v"

// `include "utils/IDDR_sim.v"
// `include "tests/hdl/RAMB16_S1_S9_sim2.v" 

`include "encode_8b10b.v"

module tb (
    input wire FCLK_IN,

    //full speed
    output wire BUS_CLK,
    output wire BUS_CLK_test,
    input wire  BUS_RST,
    inout wire [31:0] BUS_DATA_IN,
    output wire [31:0] BUS_DATA_OUT,
    // output wire [31:0] BUS_DATA,
    input wire [31:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    output wire BUS_BYTE_ACCESS,

    //high speed
    inout wire [7:0] FD,
    input wire FREAD,
    input wire FSTROBE,
    input wire FMODE
);


wire [19:0] SRAM_A;
wire [15:0] SRAM_IO;
wire SRAM_BHE_B;
wire SRAM_BLE_B;
wire SRAM_CE1_B;
wire SRAM_OE_B;
wire SRAM_WE_B;

wire DATA_LVDS_N;
wire DATA_LVDS_P;


// Connect tb internal bus to external split bus
wire [31:0] BUS_DATA;
assign BUS_DATA = BUS_WR ? BUS_DATA_IN : 32'bz;
assign BUS_DATA_OUT = BUS_DATA;
assign BUS_BYTE_ACCESS = BUS_ADD < 32'h8000_0000 ? 1'b1 : 1'b0;


wire CLK_RX_OUT_BUF;
wire CLK_RX_BUF;
wire CLK_IN_BUF;
wire CLK_RX_x0p5_BUF;
wire CLK_RX_x0p1_BUF;
wire CLK_REF_IN_BUF;
wire conf_clk;
wire conf_clk_x5;
wire CLK_USR_CLK;
wire CLK_USR_CLK2;


wire PLL_VCO, conf_clk_div,conf_clk_x5_div;

    // clock_divider #(.DIVISOR(40)) clock_divider (
    clock_divider #(.DIVISOR(80)) clock_divider (
                    .CLK(CLK_RX_x0p1_BUF),
                    .RESET(),
                    .CE(), // for sequential logic driven by CLK
                    .CLOCK(conf_clk_div) // only for combinatorial logic, does not waste bufg
                );

    // clock_divider #(.DIVISOR(8)) clock_divider_x5 (
    clock_divider #(.DIVISOR(16)) clock_divider_x5 (
                    .CLK(CLK_RX_x0p1_BUF),
                    .RESET(),
                    .CE(), // for sequential logic driven by CLK
                    .CLOCK(conf_clk_x5_div) // only for combinatorial logic, does not waste bufg
                );


// ----- Clock (mimics a PLL) -----
// 1.25GHz
localparam PLL_MUL                = 10;
localparam PLL_DIV_MAIN           = 1;
localparam PLL_DIV_CLK_IN         = 10;
localparam PLL_DIV_CLK_RX         = 10; //used as WCLK in receiver
localparam PLL_DIV_CLK_RX2         = 5; //used as WCLK in receiver
localparam PLL_DIV_CLK_RX_OUT      = 1;
localparam PLL_DIV_CLK_RX_x0p1    = 100;
localparam PLL_DIV_CLK_RX_x0p5    = 40;
localparam PLL_DIV_CLK_REF_IN     = 10;

clock_multiplier #(
    .MULTIPLIER(PLL_MUL)
) i_clock_multiplier(
    .CLK(BUS_CLK),
    .CLOCK(PLL_VCO)
);

clock_divider #(
    .DIVISOR(7)
) i_clock_divisor_0 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(BUS_CLK_test)
);
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_IN)
) i_clock_divisor_1 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_IN_BUF)
);
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX)
) i_clock_divisor_2 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_RX_BUF)
);
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX_OUT)
) i_clock_divisor_3 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_RX_OUT_BUF)
);
assign CLK_RX_OUT_BUF =PLL_VCO;
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX_x0p1)
) i_clock_divisor_4 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_RX_x0p1_BUF)
);
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX_x0p5)
) i_clock_divisor_5 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_RX_x0p5_BUF)
);
clock_divider #(
    .DIVISOR(PLL_DIV_CLK_REF_IN)
) i_clock_divisor_6 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_REF_IN_BUF)
);

clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX)
) i_clock_divisor_7 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_USR_CLK)
);

clock_divider #(
    .DIVISOR(PLL_DIV_CLK_RX2)
) i_clock_divisor_8 (
    .CLK(PLL_VCO),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK_USR_CLK2)
);
wire PLL_VCO_x2;
clock_multiplier #(
    .MULTIPLIER(2)
) i_clock_multiplier_x2(
    .CLK(PLL_VCO),
    .CLOCK(PLL_VCO_x2)
);


// localparam FIFO_BASEADDR = 64'h200000000;
// localparam FIFO_HIGHADDR = 64'h300000000-2;

// localparam FIFO_BASEADDR_DATA = 64'h200000000_0000;
// localparam FIFO_HIGHADDR_DATA = 64'h300000000_0000;

localparam FIFO_BASEADDR = 32'h8000;
localparam FIFO_HIGHADDR = 32'h9000-1;
localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

// -------  USER MODULES  ------- //
//DUMMY wires for simulation
wire ARB_READY_OUT,ARB_WRITE_OUT;
wire [31:0]ARB_DATA_OUT;
// assign ARB_READY_OUT = !FIFO_FULL;
wire SCK, CSB, MOSI, MISO, RST_Ctl_RB, Ctl_Clk_1, Ctl_Clk_2, Ctl_Load, Ctl_SIN, Ctl_SOut;
wire INJECTION, Sync_res, Clk_ref_OUT, Clk_ext_LVDS, RX_READY, clk_enable;
wire FIFO_FULL, FIFO_READ, FIFO_NOT_EMPTY, FIFO_WRITE, FIFO_NEAR_FULL;
wire [31:0] FIFO_DATA;
wire I2C_SDA, I2C_SCL;

bram_fifo #(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR),
    .BASEADDR_DATA(FIFO_BASEADDR_DATA),
    .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
    .ABUSWIDTH(32)
) i_out_fifo (
    .BUS_CLK(BUS_CLK_test),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ_NEXT_OUT(ARB_READY_OUT),
    .FIFO_EMPTY_IN(!ARB_READY_OUT),
    .FIFO_DATA(ARB_DATA_OUT),

    .FIFO_NOT_EMPTY(),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(FIFO_NEAR_FULL),
    .FIFO_READ_ERROR()
);

//fast data input wrapper 
  wire [35:0] recieved_data;
//   wire [8:0] recieved_data;
  wire        soft_reset_rx;
  wire [ 3:0] rxdisperr_i;
  wire [ 3:0] rxnotintable_i;
  wire        rxusrclk_i;
  wire [ 3:0] rxcharisk_i;
  wire        resetdone;
  wire cpllreset_i;
  wire gt0_gtrxreset_i;
  wire track_data_out;
  wire cplllocked;

//   fast_data_input fast_input_wraper_inst (
//       .Q0_CLK1_GTREFCLK_PAD_P_IN(LVDS_IN_CLK_P),
//       .Q0_CLK1_GTREFCLK_PAD_N_IN(LVDS_IN_CLK_N),
//       .DRPCLK_IN(FCLK_IN_BUFF),
//       .RXN_IN(DATA_LVDS_N),
//       .RXP_IN(DATA_LVDS_P),
//       .reset(soft_reset_rx),
//       .gt0_cplllock_i(cplllocked),
//       .gt0_rxdisperr_i(rxdisperr_i),
//       .gt0_rxnotintable_i(rxnotintable_i),
//       .cdc_data_out(recieved_data),
//       .TRACK_DATA_OUT(track_data_out),
//       .gt0_rxcharisk_i(rxcharisk_i),
//       .gt0_rxfsmresetdone_i(resetdone),
//     //   .gt0_rxusrclk_i(rxusrclk_i),
//       .output_clk(CLK_RX_OUT_BUF)
//   );
//   assign rxusrclk_i=CLK_RX_OUT_BUF;


mightypix_core fpga
                 (
                   //local bus
                   .BUS_CLK(BUS_CLK_test),
                   .BUS_DATA(BUS_DATA[7:0]),
                   .BUS_ADD(BUS_ADD),
                   .BUS_RD(BUS_RD),
                   .BUS_WR(BUS_WR),
                   .BUS_RST(BUS_RST),



                   .ARB_READY_OUT(ARB_READY_OUT),
                   .ARB_WRITE_OUT(ARB_WRITE_OUT),
                   .ARB_DATA_OUT(ARB_DATA_OUT),
                   .FIFO_FULL(FIFO_FULL),
                   .FIFO_NEAR_FULL(FIFO_NEAR_FULL),

                   //clocks
                   .conf_clk(conf_clk),
                   .conf_clk_x8(conf_clk_x5),
                //    .CLK4(CLK4),
                   .CLK_RX_x0p1(CLK_RX_x0p1_BUF),
                   .CLK_RX_x0p5(CLK_RX_x0p5_BUF),
                   .CLK_IN(CLK_IN_BUF),
                   .CLK_RX(CLK_RX_BUF),
                   .CLK_RX_x2(CLK_RX_OUT_BUF),
                   .CLK_REF_IN(CLK_REF_IN_BUF),
                    
                    //RX INFO
                    .soft_reset_rx(soft_reset_rx),
                    .rxdisperr(4'b0000),
                    .rxnotintable(4'b0000),
                    .rxusrclk(CLK_USR_CLK),
                    .rxusrclk2(CLK_USR_CLK),
                    .rxdata(recieved_data[31:0]),
                    // .rxdata(recieved_data[7:0]),
                    .rxcharisk(recieved_data[36:32]),
                    .cplllocked(1'b1),
                    .resetdone(1'b1),
                    .SDA(I2C_SDA),
                    .SCL(I2C_SCL),

                   .SCK(SCK),    //DIN6
                   .CSB(CSB),    //DIN5
                   .MOSI(MOSI),   //DIN4
                   .MISO(MISO),    //DOUT1
                   .RST_Ctl_RB(RST_Ctl_RB),   //DIN7
                   .Ctl_Clk_1(Ctl_Clk_1),    //DIN3
                   .Ctl_Clk_2(Ctl_Clk_2),    //DIN2
                   .Ctl_Load(Ctl_Load),     //DIN1
                   .Ctl_SIN(Ctl_SIN),     //DIN0
                   .Ctl_SOut(Ctl_SOut),      //DOUT0

                   .INJECTION(INJECTION),
                   .DATA_LVDS_P(),     //LVDA_OUT0
                   .DATA_LVDS_N(),     //LVDA_OUT0
                   .Sync_res(Sync_res),     //LVDA_IN2
                   .Clk_ref(Clk_ref_OUT),
                //    .Clk_ext_LVDS(Clk_ext_LVDS),
                   .RX_READY(RX_READY),
                   .CLK_ENABLE(clk_enable)
                 );

wire CLK_SER;
wire CLK_SER_x2;
assign CLK_SER = CLK_RX_BUF;
// assign CLK_SER = CLK_RX_BUF2;
// assign CLK_SER = PLL_VCO;
assign CLK_SER_x2 = PLL_VCO_x2;



logic [PSIZE-1:0][35:0] pattern;

localparam PSIZE = 100;

initial begin
    for (int i=0; i < 40; i = i + 1 )
    begin
        // pattern[i] = {1'b1, 8'hbc}; //K28_5
        pattern[i] = {4'b1111, 32'hbcbcbcbc}; //K28_5
    end
    for (int i=72; i < 100; i = i + 1 )
    begin
        // pattern[i] = {1'b1, 8'hbc}; //K28_5
        pattern[i] = {4'b1111, 32'hbcbcbcbc}; //K28_5
    end

    //  pattern[40] = {1'b1, 8'h1c};
    //  pattern[41] = {1'b0, 8'haa};
    //  pattern[42] = {1'b1, 8'h1c};
    //  pattern[43] = {1'b0, 8'haa};
     
    //  pattern[44] = {1'b1, 8'hbc};
    //  pattern[45] = {1'b1, 8'hbc};
    //  pattern[46] = {1'b1, 8'hbc};
    //  pattern[47] = {1'b1, 8'hbc};

    //  pattern[48] = {1'b1, 8'h1c};
    //  pattern[49] = {1'b0, 8'haa};
    //  pattern[50] = {1'b1, 8'h1c};
    //  pattern[51] = {1'b0, 8'haa};
    //  //0xc055a774
    //  pattern[52] = {1'b0, 8'hc0};
    //  pattern[53] = {1'b0, 8'h55};
    //  pattern[54] = {1'b0, 8'ha7};
    //  pattern[55] = {1'b0, 8'h74};
     
    // //  0xc109f5ff
    //  pattern[56] = {1'b0, 8'hc1};
    //  pattern[57] = {1'b0, 8'h09};
    //  pattern[58] = {1'b0, 8'hf5};
    //  pattern[59] = {1'b0, 8'hff};

    // //  0x1e3789ad
    //  pattern[60] = {1'b0, 8'h1e};
    //  pattern[61] = {1'b0, 8'h37};
    //  pattern[62] = {1'b0, 8'h89};
    //  pattern[63] = {1'b0, 8'had};

    //  pattern[64] = {1'b1, 8'h1c};
    //  pattern[65] = {1'b0, 8'haa};
    //  pattern[66] = {1'b1, 8'h1c};
    //  pattern[67] = {1'b0, 8'haa};

    //  pattern[68] = {1'b1, 8'h1c};
    //  pattern[69] = {1'b0, 8'haa};
    //  pattern[70] = {1'b1, 8'h1c};
    //  pattern[71] = {1'b0, 8'haa};
     
     pattern[41] = {4'b1010, 32'h1c551c55};
     pattern[42] = {4'b0000, 32'haaaaaaaa};
     pattern[43] = {4'b1010, 32'h1c551c55};
     pattern[44] = {4'b0000, 32'ha7a7a7a7};
     pattern[45] = {4'b0101, 32'h551c551c};
     pattern[46] = {4'b0000, 32'haaaaaaaa};
     pattern[47] = {4'b0101, 32'h551c551c};
     
     pattern[48] = {4'b1111, 32'hbcbcbcbc};
     pattern[49] = {4'b1111, 32'hbcbcbcbc};
     pattern[50] = {4'b1111, 32'hbcbcbcbc};
     pattern[51] = {4'b1111, 32'hbcbcbcbc};

     pattern[52] = {4'b0000, 32'haaaaaaaa};
     pattern[53] = {4'b0101, 32'h551c551c};
    //  pattern[52] = {4'b0000, 32'haaaaaaaa};
     pattern[54] = {4'b1111, 32'h1c1c1c1c};
     pattern[55] = {4'b0000, 32'haaaaaaaa};
    //  //0xc055a774
     pattern[56] = {4'b0000, 32'hc0c0c0c0};
     pattern[57] = {4'b0000, 32'h55555555};
     pattern[58] = {4'b0000, 32'ha7a7a7a7};
     pattern[59] = {4'b0000, 32'h74747474};
     
    // //  0xc109f5ff
    //  pattern[56] = {2'b00, 16'hc1c1};
    //  pattern[57] = {2'b00, 16'h0909};
    //  pattern[58] = {2'b00, 16'hf5f5};
     pattern[60] = {4'b0000, 32'hffffffff};

    //  0x1e3789ad
    //  pattern[60] = {4'b0000, 32'h1e1e1e1e};
    //  pattern[61] = {4'b0000, 32'h37373737};
    //  pattern[62] = {4'b0000, 32'h89898989};
    //  pattern[63] = {4'b0000, 32'hadadadad};

    //  pattern[64] = {4'b1111, 32'h1c1c1c1c};
    //  pattern[65] = {4'b0000, 32'haaaaaaaa};
    //  pattern[66] = {4'b1111, 32'h1c1c1c1c};
    //  pattern[67] = {4'b0000, 32'haaaaaaaa};

    //  pattern[68] = {4'b1111, 32'h1c1c1c1c};
    //  pattern[69] = {4'b0000, 32'haaaaaaa};
    //  pattern[70] = {4'b1111, 32'h1c1c1c1c};
    //  pattern[71] = {4'b0000, 32'haaaaaaa};   
end

logic dispout, dispin = 0; 
always@(posedge CLK_SER)
    dispin <= dispout;

integer wcounter = 0;
always@(posedge CLK_SER)
    if (wcounter < PSIZE-1) begin
        wcounter <= wcounter +1;
    end
    else begin
        wcounter <= 0;
    end

integer sync_time = 0;
always@(posedge CLK_SER)
    sync_time <= sync_time + 1;


// // logic [8:0] datain;
// logic [35:0] datain;
// // assign datain = sync_time > 10 ? pattern[wcounter] : {1'b1, 8'hbc};
// assign datain = sync_time > 10 ? pattern[wcounter] : {4'b1111, 32'hbcbcbcbc};

// logic [9:0] dataout_enc;
// logic [9:0] dataout_enc;
// encode_8b10b enc(.datain(datain[7:0]), .k(datain[8]), .dispin(dispin), .dataout(dataout_enc), .dispout(dispout)) ;


logic [35:0] ser_out;
// logic [9:0] ser_out;
always@(posedge CLK_SER) begin
    if (sync_time > 10)
        ser_out <= {pattern[wcounter]};
        // ser_out <= {dataout_enc};
    else 
        ser_out <=  {4'b1111, 32'hbcbcbcbc};
        // ser_out <=  {1'b1, 8'hbc};
end

assign recieved_data = ser_out;

initial begin
    $dumpfile("mightypix.vcd");
    $dumpvars(0,pattern);
    $dumpvars(0);
end

endmodule
