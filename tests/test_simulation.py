#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import yaml
import time
from bitarray import bitarray

from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean
from mightypix_daq.mightypix import MightyPix
import basil.dut


class TestSimulation(unittest.TestCase):
    def setUp(self):
        extra_defines = []  # Simulate only one double column

        root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        try:
            cocotb_compile_clean()
        except:
            pass
        cocotb_compile_and_run(
            sim_files=[root_dir + "/tests/hdl/tb.sv"],
            extra_defines=extra_defines,
            sim_bus="basil.utils.sim.SiLibUsbBusDriver",
            include_dirs=(
                root_dir,
                root_dir + "/mightypix_daq/firmware/src",
                # root_dir + "/mightypix_daq/firmware/src/lib",
                root_dir + "/tests/hdl",
            ),
            # extra='EXTRA_ARGS += -g2012'
        )

        with open(root_dir + "/mightypix_daq/mightypix.yaml", "r") as f:
            cnfg = yaml.safe_load(f)

        cnfg["transfer_layer"][0]["type"] = "SiSim"
        # cnfg['hw_drivers'][0]['init']['no_calibration'] = True

        # dut = basil.dut.Dut(conf=cnfg)
        self.dut = MightyPix(conf=cnfg)

        self.dut.init()
        # self.dut["data_rx"].NOT_FILTER_COLUMN2 = 1

    def test_configuration(self):
        # self.assertTrue(self.dut.dut["data_rx"].READY)

        self.dut.dut["data_rx"].ENABLE = 1
        # self.dut["data_rx"].NOT_FILTER_COLUMN2 = 1
        self.dut.set_read()
        print(self.dut.get_rx_status())
        for _ in range(30):
            self.dut.dut["data_rx"].READY

        data = self.dut.dut["fifo"].get_data()
        print([hex(i) for i in data.tolist()])
        print(len(data))
        # self.assertEqual(len(data), 20)  # 2x2 words

        # self.assertEqual(data.tolist(), [0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA])
        a = [0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xAAC0, 0xC084, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA]

        # ['0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80f6eb', '0xc0c0', '0xc0aa', '0x80f73b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80febb', '0xc0c0', '0xc0aa', '0x80ff0b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x81068b', '0xc0c0', '0xc0aa', '0x8106db', '0xc000']
        # ['0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80f6eb', '0xc0c0', '0xc0aa', '0x80f73b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80febb', '0xc0c0', '0xc0aa', '0x80ff0b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x81068b', '0xc0c0', '0xc0aa', '0x8106db', '0xc000']

        # 3ca0,
        # 0500,
        # C0C0,
        # C0AA,
        # C0C0,
        # C0AA,
        # C000,

        self.dut.enable_column()
        self.dut.enable_injection(0, 0)

    #
    # def test_read(self):
    #     self.dut.enable_injection(0, 0)
    #     self.dut.enable_column()

    #     print(self.dut.power_status())
    #     print("kmllmklkmmmk")
    #     self.dut.reset_read()
    #     print("kmllmklkmmmk")
    #     raw = self.dut.get_data()
    #     print(raw)

    def tearDown(self):
        self.dut.close()
        time.sleep(5)
        cocotb_compile_clean()


if __name__ == "__main__":
    unittest.main()
